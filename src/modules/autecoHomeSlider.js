import { tns } from "tiny-slider/src/tiny-slider";

const sliderHome = () => {
    let slider = tns({
        container: '.slider-home-new',
        items: 1,
        autoplayTimeout: 6000,
        responsive: {
            "350": {
                controls: true,
                lazyload: true
            },
            "980": {
                controls: true,
                lazyload: true,
                nav: true
            }
        },
        slideBy: "page",
        autoplay: true,
        loop: true,
        controls: true,
        navPosition: "bottom",
        mouseDrag: true,

        onInit: function () { }
    });
}

const autecoHomeSlider = () => {
    sliderHome();
    
}

export default autecoHomeSlider;
