/* eslint-disable no-undef */
import '../styles/vitrinas.scss'

const nameCategoryH1 = () => {
  // primera carga titulo
  $('.title-category-h1').text($('.titulo-sessao').first().text())
  $('.titulo-sessao').remove()
}

document.addEventListener('DOMContentLoaded', (e) => {
  nameCategoryH1()
})
