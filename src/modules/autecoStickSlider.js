$(document).on('ready', function () {
    if ($('.slider')) {
        $('.slider').slick({
            centerMode: true,
            infinite: true,
            slidesToShow: 5,
            speed: 400,
            focusOnSelect: true,
            lazyLoad: 'ondemand',
            centerPadding: "0px"
        });
        //home slider
        if($('.home')){
            $('#slider-home-new .cont__img img').fadeIn(200);            
            $('.cont__extra .slick-list').fadeIn();
            setTimeout(function(){
                $('.content-categories').show();
            }, 300);
        }
    }
});

const perspectiveCarrousel = () => {
    let slCenter = document.querySelector('.slick-current');

    let nextDiv = slCenter.nextSibling
    let prevDiv = slCenter.previousSibling

    if (slCenter.classList.contains('slick-center')) {
        let slScale = document.querySelectorAll('.sl-scale')
        slScale.forEach(element => {
            element.classList.remove('sl-scale')
        });
        nextDiv.classList.add('sl-scale');
        prevDiv.classList.add('sl-scale');
    }

}

const postSlider = () => {
    let slCenter = document.querySelector('.slick-track');
    let btnNext = document.querySelector('.slick-next');
    let btnPrev = document.querySelector('.slick-prev');

    perspectiveCarrousel()

    eventsSlider(btnNext)
    eventsSlider(btnPrev)
    eventsSlider(slCenter)


}

const eventsSlider = (entry) => {
    entry.addEventListener('click', () => {
        perspectiveCarrousel();
    })
}

const autecoStickSlider = () => {
    perspectiveCarrousel();
    postSlider();
}

export default autecoStickSlider



