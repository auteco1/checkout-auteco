import '../styles/products.scss'
// import Carrito from './componentes/carrito'

const modalCrossSelling = () => {
  if (document.querySelectorAll('.crosSellingElement .crossSelling ul li:not(.helperComplement)').length >= 1) {
    document.addEventListener('click', (e) => {
      if (e.target.classList.contains('buy-button')) {
        e.preventDefault()
        e.stopPropagation()
        const href = e.target.href
        const data = href.split('?sku=')

        const stringData = JSON.stringify(data)
        const result = stringData.includes('https://www.autecomobility.com/')
        if (result) {
          const skuId = data[1].split('&qty')
          const item = {
            id: skuId[0],
            quantity: 1,
            seller: '1'
          }
          vtexjs.checkout.addToCart([item], null, 1).done(function (orderForm) {
            document.querySelector('body').classList.add('modal-open')
            document.querySelector('.blackbox').classList.add('in')
            document.querySelector('.crosSellingElement').classList.add('show')
          })
        } else {
          alert('Por favor, seleccione el modelo deseado.')
        }
      }
    })
  }
}

const activeSlide = () => {
  $('li.helperComplement').remove()
  $('.crossSelling ul').slick({
    slidesToShow: 2,
    slidesToScroll: 1,
    thumbs: false,
    autoplay: true,
    speed: 1500,
    responsive: [{
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }]
  })
}

const bannerRepuestos = () => {
  const banner = $('.value-field.Banner-repuestos-originales').html()
  if (banner) {
    $('.content-image-respuestos').html(banner)
  }
}

const closeModal = () => {
  document.addEventListener('click', (e) => {
    if (e.target.classList.contains('close')) {
      document.querySelector('body').classList.remove('modal-open')
      document.querySelector('.blackbox').classList.remove('in')
      document.querySelector('.crosSellingElement').classList.remove('show')
    }
  })
}

const verifiedIfHasHash = () => {
  const hashInPage = window.location.hash
  setTimeout(() => {
    if (hashInPage === '#reserva') {
      $('.call-me-modal').modal('show')
    }
  }, 2000)
}

const addFromVitrine = () => {
  document.addEventListener('click', (e) => {
    if (e.target.classList.contains('addVitrineProduct')) {
      e.preventDefault()
      // validate if have selection Sku
      if (e.target.closest('.shelf-qd-v2').querySelector('.shelf-qd-skuSelection .sizeSelection .selected') != null) {
        const skuId = e.target.closest('.shelf-qd-v2').querySelector('.shelf-qd-skuSelection .sizeSelection .selected').getAttribute('data-sku')
        const item = {
          id: skuId,
          quantity: 1,
          seller: '1'
        }
        vtexjs.checkout.addToCart([item], null, 1).done(function (orderForm) {
          e.target.closest('li').classList.add('added')
        })
      } else {
        e.target.closest('.shelf-qd-v2').querySelector('.shelf-qd-skuSelection').insertAdjacentHTML('beforeend', '<span class="errorMessage">Selecciona una talla para poder agregar el producto</span>')
        setTimeout(() => {
          e.target.closest('.shelf-qd-v2').querySelector('.errorMessage').innerHTML = ''
        }, 5000)
      }
    }
  })
}

const skuShelfPrint = (data, element) => {
  element.querySelector('.shelf-qd-skuSelection').insertAdjacentHTML('beforeend', '<div class="sizeSelection selection"></div>')
  data.skus.map((key, value) => {
    if (key.available === true) {
      if (key.sku) {
        element.querySelector('.sizeSelection').insertAdjacentHTML('beforeend', '<label class="label-sku-selection Talla" data-sku="' + key.sku + '"> <input type="checkbox" class="sku-selection-vitrine itemSelection"/> ' + key.dimensions.TALLA + '</label>')
      }
    }
  })
  /* setTimeout(() => {
    activeSlide()
  }, 100) */
}

const prodsInShelf = () => {
  document.querySelectorAll('.crossSelling ul li:not(.helperComplement)').forEach((elm) => {
    const prodId = elm.querySelector('.qd_productId').value
    dataProd(prodId).then((res) => {
      skuShelfPrint(res, elm)
    })
  })
}

const dataProd = (prodId) => {
  return fetch(`/api/catalog_system/pub/products/variations/${prodId}`).then(res => res.json())
}

const validateCheckSku = () => {
  document.addEventListener('click', (e) => {
    if (e.target.classList.contains('label-sku-selection') && e.target.nodeName === 'LABEL') {
      e.target.closest('.selection').querySelectorAll('.label-sku-selection').forEach((element, value) => {
        element.classList.remove('selected')
      })
      e.target.classList.add('selected')
    }
  })
}

const openPopupQuestion = () => {
  document.addEventListener('click', (e) => {
    if ($(document.body).is('.product-brand')) {
      if (e.target.classList.contains('click-question')) {
        $('.call-me-modal').modal('show')
      }
    }
  })
}

const messageOutStock = () => {
  const numberProduct = $('#___rc-p-id').val()
  dataProd(numberProduct).then((res) => {
    const unavailableStock = $('body').hasClass('qd-product-unavailable')
    if (res.available === false) {
      if ($('.content-aviso-outstock').length < 2) {
        $('.qd-price-formated').attr('style', 'display: block;')
        $('.product-qd-v1-act-tab').attr('style', 'display: block !important')
        $('.product-qd-v1-price').append('<div class="content-aviso-outstock"><label>No disponible online. </label><a href="/puntos-de-atencion">Valida el punto de venta más cercano</a></div>')
      }
    }

    if (unavailableStock === true) {
      setTimeout(() => {
        $('.product-qd-v1-price span').attr('style', 'display: block !important')
      }, 150)
      if ($(document.body).is('.product-brand') && $('.product-qd-v1-test-drive').is(':visible') !== true) {
        $('.product-qd-v1-contact').attr('style', 'width : 100%!important; display : inline')
        // $('.product-qd-v1-price span').attr('style', 'display: block!important')
      }
    }
  })
}

const listenChangueSKU = () => {
  $('body').on('change', function () {
    if ($(this).hasClass('qd-product-unavailable')) {
      $('.content-aviso-outstock').remove()
      $('.qd-price-formated').attr('style', 'display: block;')
      $('.qd-price-formated').attr('style', 'font-size: 37px;')
      $('.qd-price-formated').attr('style', 'font-weight: 600;')
      $('.qd-price-formated').attr('style', 'color: #000;')
      $('.qd-price-formated').attr('style', 'width: 60%;')
      $('.product-qd-v1-price span').attr('style', 'display: block !important')
      $('.product-qd-v1-price').append('<div class="content-aviso-outstock"><label><sttrong>No disponible online.</sttrong> </label><a href="/puntos-de-atencion">Valida el punto de venta más cercano</a></div>')
      setTimeout(() => {
        $('.product-qd-v1-price span').attr('style', 'display: block !important')
        $('.product-brand .product-qd-v1-act-tab').attr('style', 'display: block !important')
        $('.price-title').attr('style', 'display: block !important')

        if ($('.addi').is(':visible') === true) {
          $('.addi').attr('style', 'display: none !important')
        }
      }, 150)
    } else {
      const unavailableStock = $('body').hasClass('qd-product-unavailable')
      if (unavailableStock === true) {
        $('.product-qd-v1-price span').attr('style', 'display: block !important')
        if ($(document.body).is('.product-brand') && $('.product-qd-v1-test-drive').is(':visible') !== true) {
          $('.product-qd-v1-contact').attr('style', 'width : 100%!important; display : inline')
          // $('.product-qd-v1-price span').attr('style', 'display: block!important')
        }
      }
      // $('.product-qd-v1-contact').attr('style', 'width : 100%!important; display : inline')
      $('.qd-product-unavailable .que-hacer').attr('style', 'display: block !important')
      $('.content-aviso-outstock').remove()
      // $('.qd-price-formated').attr('style', 'display: none !important');
      if (!$('.addi').is(':visible') === true) {
        $('.addi').attr('style', 'display: block !important')
      }
    }
  })
}

const nameProductH1 = () => {
  // primera carga titulo
  $('.name-product').text($('.productName').first().text())
  // cambio en titulo
  $('body').on('change', function () {
    $('.name-product').text($('.productName').first().text())
  })
}

const hideTopbar = () => {
  if ($('.rep-2-c').length >= 1) {
    window.onscroll = function () { myFunction() }
  }

  const header = document.getElementById('header-display-scroll')
  const sticky = header.offsetTop

  function myFunction () {
    if (window.pageYOffset >= sticky - 20) {
      header.classList.add('sticky')
      $('.header-qd-v1').addClass('no-sticky-menu-header')
    } else {
      header.classList.remove('sticky')
      $('.rep-2-c header-qd-v1').removeClass('no-sticky-menu-header')
    }
  };
/* end sticky */
}

document.addEventListener('DOMContentLoaded', (e) => {
  nameProductH1()
  hideTopbar()
  messageOutStock()
  listenChangueSKU()
  modalCrossSelling()
  verifiedIfHasHash()
  closeModal()
  addFromVitrine()
  prodsInShelf()
  validateCheckSku()
  openPopupQuestion()
  bannerRepuestos()
  // activeSlide()
})
