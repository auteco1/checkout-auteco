import '../styles/category.scss'
import SmartFilter from "../modules/componentes/utils/smartfilter";
import mediaQ from "../modules/componentes/utils/mediaqueries";
import remoCentsAll from "../modules/componentes/utils/remoCentsAll";
import Ordercart from "../modules/componentes/utils/ordercart";
import discountPercent from "../modules/componentes/utils/discountPercent";
import autecoStickSlider from "./autecoStickSlider"
import fixedButtons from './componentes/fixedButtons'
// import { showcaseInstallment } from "../modules/componentes/utils/showcaseInstallment";
// import { gsap } from "gsap";

const sideFilter = () => {
  let selectAppendDiv, selectsFilters;
  if (mediaQ("(min-width: 1024px)")) {
    selectAppendDiv = "#orderDesktop";
    selectsFilters = "#FilterSelect__Deskt";
  } else {
    selectAppendDiv = "#orderDesktop";
    selectsFilters = "#FilterSelect__Deskt";
  }

  // //console.log("filter container", selectsFilters);

  let orderCart = new Ordercart();

  let filter = new SmartFilter({
    pageLimit: null,
    scrollInfinito: false,
    loadMoreWithButton: false,
    loadMorePages: true,
    loadContent: ".showcases__grid[id^=ResultItems]>.showcases__grid",
    classGrid: ".showcases__grid",
    linksMenu: ".search-single-navigator",
    insertMenuAfter: ".search-multiple-navigator h3:first",
    selectOrderCustom: true,
    selectORderDivAppend: selectAppendDiv,
    departamentBodyclass: "page-departament",
    departamentListProduct: true,
    categoryBodyClass: "categoria-redisign",
    buscaBodyClass: "page-busca",
    compare: false,
    divMultiSelect: false,
    divMultiSelectDiv: selectsFilters,
    callbackGeneral: function () { },
    callbackShowcases: function (container) {
      discountPercent(container);
      remoCentsAll(container);
    },
  });

  let listingProductPage = document.querySelector(
    ".listingProductPage__sidebar__filter__title"
  );
  listingProductPage.addEventListener("click", (e) => {
    const filterFixed = document.querySelector(
      ".listingProductPage__sidebar__filter"
    );
    filterFixed.classList.remove("open");
  });
  setTimeout(function () {
    //console.log("ingreso a category");
    document
      .querySelector(".listingProductPage__sidebar__filter__btn")
      .addEventListener("click", (e) => {
        const filterFixed = document.querySelector(
          ".listingProductPage__sidebar__filter"
        );
        filterFixed.classList.remove("open");
      });
  }, 500);
};

const filtrosClose = () => {
  /* Ocultar los filtros de Catogoria multiple seleccotr */
  let filtrosFields = document.querySelectorAll(
    ".search-multiple-navigator fieldset h5"
  ),
    contenerdorFields = document.querySelectorAll(
      ".search-multiple-navigator fieldset div"
    );

  filtrosFields.forEach((element) => {
    element.classList.add("active__panel");
    contenerdorFields.forEach((ele) => {
      ele.classList.add("close__div");
      ele.addEventListener("click", () => {
        element.classList.add("active__panel");
        ele.classList.add("close__div");
      });
    });
  });
};

/* const dataResultsNumber = () => {
  let numberResult = document.querySelector('.resultado-busca-numero .value').innerText
  document.querySelector('.dataResults-number').innerText = numberResult
} */

const messages = () => {
  let contenerdorFields = document.querySelectorAll(
    ".search-multiple-navigator fieldset div"
  );
  const filters = [];

  contenerdorFields.forEach((element) => {
    element.addEventListener("change", () => {
      const item = document.querySelectorAll(".button")
        ? document.querySelectorAll(".button")
        : [];
      item.forEach((item) => {
        //console.log(element, item);
        if (!filters.includes(item.textContent)) {
          filters.push(item.textContent);
          putFilter(filters);
        }
        clearFilterArr();
      });
    });
  });
};

const clearFilterArr = () => {
  const containerFilter = document.querySelectorAll(".itemFilter");
  const closeFilter = document.getElementById("cleanFilters");

  closeFilter.onclick = () => {
    if (containerFilter.length > 0) {
      containerFilter.forEach((item) => {
        item.style.display = "none";
      });
    }
  };
};

const putFilter = (arrayFiltros) => {
  const containerFilter = document.querySelector(
    ".listingProductPage__sidebar__menuMobile"
  );
  let content = "";
  arrayFiltros
    .filter((value, index, self) => self.indexOf(value) === index)
    .forEach((item) => {
      content = `<span class="itemFilter"> ${item} /</span>`;
    });
  containerFilter.insertAdjacentHTML("beforeend", content);
};

const extractColorFilter = () => {
  let contentColorFilter = document.querySelectorAll(
    ".search-multiple-navigator fieldset"
  );
  contentColorFilter.forEach((ele) => {
    let idFiel=ele.querySelector("h5").textContent.replace(/\s/g, '-').replace('ñ','n')
    ele.classList.add(idFiel)
    ele.querySelector("h5").id = idFiel
    if (ele.querySelector("h5").textContent === "Color") {
      let colorText = ele.querySelectorAll("label");
      ele.classList.add("selectColors");
      colorText.forEach((color, i) => {
        let fillColor = color.getAttribute("data-value").split("_");
        let newWrap = document.createElement("div");
        newWrap.classList.add("wrapColor");
        newWrap.innerHTML = `<span class="color" style="background-color: ${fillColor[1]};"></span> <p class="colorText">${fillColor[0]}</p>`;
        colorText[i].insertBefore(newWrap, color[i]);
      });
    }
  });
};

// start cambio de direccion flecha del filtro
if (document.querySelector(".search-single-navigator .hombre a")) {
  document
    .querySelector(".search-single-navigator .hombre a")
    .addEventListener("click", () => {
      const directionLogo = document.querySelector(".search-single-navigator");
      const classeActivadaHombre = directionLogo.childNodes[2].classList;
      if (classeActivadaHombre.length < 2) {
        $(
          "<style>.page-busca .menu-departamento .search-single-navigator .hombre a:after{transform: rotate(90deg);}</style>"
        ).appendTo("head");
      } else if (classeActivadaHombre.length > 1) {
        $(
          "<style>.page-busca .menu-departamento .search-single-navigator .hombre a:after{transform: rotate(0deg);}</style>"
        ).appendTo("head");
      }
    });
}
if (document.querySelector(".search-single-navigator .mujer a")) {
  document
    .querySelector(".search-single-navigator .mujer a")
    .addEventListener("click", () => {
      const directionLogo = document.querySelector(".search-single-navigator");
      const classeActivadaMujer = directionLogo.childNodes[5].classList;
      if (classeActivadaMujer.length < 2) {
        $(
          "<style>.page-busca .menu-departamento .search-single-navigator .mujer a:after{transform: rotate(90deg);}</style>"
        ).appendTo("head");
      } else if (classeActivadaMujer.length > 1) {
        $(
          "<style>.page-busca .menu-departamento .search-single-navigator .mujer a:after{transform: rotate(0deg);}</style>"
        ).appendTo("head");
      }
    });
}
// fin cambio de direccion flecha del filtro

const singleFilter = () => {
  let titles = document.querySelectorAll(".search-single-navigator h3");
  if (titles.length>0) {
    titles.forEach((ele) => {
      ele.addEventListener("click", (e) => {
        e.preventDefault();
        ele.nextElementSibling.classList.toggle("active");
      });
    });
  }
};

const saveFilter = () => {
  let contenerdorFields = document.querySelectorAll(
    ".search-multiple-navigator fieldset div"
  );
  let filters = [],
  filtersObjet={}
  contenerdorFields.forEach((element) => {
    element.addEventListener("change", () => {
      const item = document.querySelectorAll(".button")
      let id = element.parentElement.querySelector('h5').id
      //console.log('ID: ',id)
        ? document.querySelectorAll(".button")
        : [];
      item.forEach((item) => {
        if (!filters.includes(item.textContent)) {
          filtersObjet[id] = item.textContent;
          filters=[]
          Object.keys(filtersObjet).forEach(element => {
            //console.log('ID: ',element,'VALUE: ',filtersObjet[element])
            filters.push(filtersObjet[element])
          });
          filters.push(item.textContent);
          putFilter(filters);
        }
        clearFilterArr();
      });
      localStorage.setItem("filterData", filters);
      //console.log('filtro ingresado',filtersObjet)
      setTimeout(()=>{
        hideProductsoffStop()
        textContentBrand()
        printPriceProductsOut()
      },500)
    
    });
  });
};

window.addEventListener('DOMContentLoaded', function () {
  category()
})
const category = () => {
  let category = document.querySelector("body.categoria-redisign");
  let search = document.querySelector("body.page-busca");
  let brand = document.querySelector("body.brand");
  if (category || search) {
    //dataResultsNumber()
    fixedButtons()
    sideFilter();
    saveFilter();
    // //console.log("hola tita ");
    setTimeout(() => {
      extractColorFilter();
    }, 500);
    filtrosClose();
    textContentBrand()
    printPriceProductsOut()
    hideProductsoffStop()
    changeOrderFilter()
    autecoStickSlider()
    toggleMenuFilters()
    listernerSelect()
    //setTimeout(()=>{
      document.querySelector('#orderbyFilterSelect').value="OrderByPriceASC"
        setTimeout(()=>{
          textContentBrand()
          printPriceProductsOut()
        },500)
    //},1000)
  }
  if (category || search || brand) {
    singleFilter();
  }
  if (screen.width < 500) {
    messages();
    openMenuMobile()
  }
  //setTimeout(()=>{
    document.querySelector('.listingProductPage__cont__showcases').style.display="block";
  //},1500)
};


// if (document.querySelector(".page-number")) {
//   window.addEventListener("DOMContentLoaded", (event) => {
//     document.querySelector(".page-number").addEventListener("click", () => {
//       //console.log("este es un click en el paginador Ç/////////");
//     });
//   });
// }

if (document.querySelector("#filterMobileBtn")) {
  window.addEventListener("DOMContentLoaded", (event) => {
    document.querySelector("#filterMobileBtn").addEventListener("click", () => {
      if(document.querySelector(".listingProductPage__sidebar__filter")) {
        document.querySelector(".listingProductPage__sidebar__filter").classList.add('open')
      }
    });
  });
}

if(document.querySelector("#smartfilterPaginationHeader > div.pagesElement.top > ul")) {
  setTimeout(function () {
    var maximoDePaginas = document.querySelector("#smartfilterPaginationHeader > div.pagesElement.top > ul").childNodes.length
    //console.log(maximoDePaginas, "maximoDePaginas");
    let contadorDelPaginador = 1
    function sumar() {
      contadorDelPaginador++;
    }
    function restar() {
      contadorDelPaginador--;
    }
  
    function llamar() {
      //console.log(contadorDelPaginador, "contadorDelPaginador");
    }
  
    document.querySelector(".primero").addEventListener('click', () => {
      if(contadorDelPaginador > 1) {
        restar();
      }
      llamar();
      document.querySelector(`div.pagesElement.top > ul > li:nth-child(${contadorDelPaginador})`).click();
    })
    document.querySelector(".ultimo").addEventListener('click', () => {
      if(contadorDelPaginador < maximoDePaginas) {
        sumar();
      }
      llamar();
      document.querySelector(`div.pagesElement.top > ul > li:nth-child(${contadorDelPaginador})`).click();
    })
    document.querySelector("body > div.main > div.listingProductPage > div > div > section > div.listingProductPage__cont__showcases > div > div:nth-child(4) > p.ultimo").addEventListener('click', () => {
      if(contadorDelPaginador < maximoDePaginas) {
        sumar();
      }
      llamar();
      document.querySelector(`div.pagesElement.top > ul > li:nth-child(${contadorDelPaginador})`).click();
    })
    document.querySelector("body > div.main > div.listingProductPage > div > div > section > div.listingProductPage__cont__showcases > div > div:nth-child(4) > p.primero").addEventListener('click', () => {
      if(contadorDelPaginador > 1) {
        restar();
      }
      llamar();
      document.querySelector(`div.pagesElement.top > ul > li:nth-child(${contadorDelPaginador})`).click();
    })
  }, 1500);
}

const textContentBrand = () => {
  let brands = document.querySelectorAll('.item__brand p')
  brands.forEach(brand => {
    let arrayText=  brand.textContent.split('-')
    if(arrayText.length>2){
      if(brand.textContent.split('-')[1]==="y"){
        brand.textContent = brand.textContent.split('-')[3]
      }else{
        brand.textContent = brand.textContent.split('-')[1]+' '+brand.textContent.split('-')[2]
      }     
    }else{
      if(arrayText.length>1){
        brand.textContent = brand.textContent.split('-')[1]
      }
      brand.textContent = brand.textContent.split('-')[0]
    }
      brand.style.textTransform = 'uppercase';
  });

  let titleItems = document.querySelectorAll('.product-name')
  titleItems.forEach(title => {
      // //console.log('titles: ', title);
      title.textContent = title.textContent.replace('MOTO', '')
      title.textContent = title.textContent.replace('VICTORY', '')
      title.textContent = title.textContent.replace('ELÉCTRICA', '')
      title.textContent = title.textContent.replace('STÄRKER', '')
      title.textContent = title.textContent.replace('STARKER', '')
      title.textContent = title.textContent.replace('SUPERSOCO', '')
      title.textContent = title.textContent.replace('KAWASAKI', '')
      title.textContent = title.textContent.replace('VICTORBENELLI', '')
      title.textContent = title.textContent.replace('KYMCO', '')
      title.textContent = title.textContent.replace('RESERVA -', '')
      title.textContent = title.textContent.replace('ELÉCTRICA STARKER ', '')
      title.textContent = title.textContent.replace('ELÉCTRICA SUPER SOCO ', '')
      title.style.textTransform = 'uppercase';
  });
  let pricesProducts = document.querySelectorAll('.item__showcase__price.price span')
    pricesProducts.forEach(async (priceProduct) => {
      // console.log('precio inicial :',priceProduct.textContent.split('$')[1] )
      priceProduct.textContent=priceProduct.textContent.split(',')[0];
    })
      
}

const printPriceProductsOut=()=>{
  let pricesProducts = document.querySelectorAll('.item__showcase__outofstock')
    pricesProducts.forEach(async (priceProduct) => {
      // https://service.autecomobility.com/sold-out-product-price/get-by-id/2246
      let id =priceProduct.parentElement.parentElement.getAttribute('id')
      // console.log('CONTENEDOR PRODUCTO: ',containerProduct);
      let infoProduct = await getPriceProductOut(id)
      let Name = infoProduct[0].productName;
      let link = infoProduct[0].link
      let price = new Intl.NumberFormat().format(infoProduct[0].items[0].sellers[0].commertialOffer.ListPrice).replace(/,/g, '.')
      let priceBest =new Intl.NumberFormat().format(infoProduct[0].items[0].sellers[0].commertialOffer.PriceWithoutDiscount).replace(/,/g, '.')
      // console.log('PRICE PRODUCT: ',infoProduct)
      let htmlPrintPrice
      if(price!=priceBest){
        htmlPrintPrice = `
          <div class="item__showcase__price price">
          <a target="" title="${Name}" href="${link}">
            <span class="item__showcase__price-old old-price">$${price}</span>
            <span class="item__showcase__price-best price-best">$${priceBest}</span>
          </a>
        </div>
        <a class="btn__more" href="${link}">Conoce más</a>`
      }else{
        htmlPrintPrice = `
          <div class="item__showcase__price price">
              <a target="" title="${Name}" href="${link}">
                  <span class="item__showcase__price-best">$${price}</span>
              </a>
        </div>
        <a class="btn__more" href="${link}">Conoce más</a>`
      }
    if(priceProduct.parentElement.querySelector('.item__showcase__price')) return false
    priceProduct.insertAdjacentHTML('beforebegin', htmlPrintPrice);
    })
}
const getPriceProductOut = async (id)=>{
  try {
    const response = await fetch(`/api/catalog_system/pub/products/search/?fq=productId:${id}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'cache-control': 'no-cache'
      }
    })
    const json = await response.json()
    // console.log(json)
    return json
  } catch (error) {
    console.log('error en getProduct', error);
  }
}

const changeOrderFilter=()=>{
  let orderDesktop = document.querySelector('#orderDesktop')
  let containerFilter = document.querySelector('.search-multiple-navigator')
  if(screen.width>768){
   
    if(!document.querySelector('#titleFilter')){
      orderDesktop.insertAdjacentHTML('afterbegin', '<p id="titleFilter">Ordernar por:</p>');
    }
    containerFilter.appendChild(orderDesktop)
  }else{
    let btnFilter = document.querySelector('.listingProductPage__sidebar__filter__btn')
    if(!document.querySelector('#titleFilter')){
      orderDesktop.insertAdjacentHTML('afterbegin', '<p id="titleFilter">Ordernar por:</p>');
    }
    let containerbefore= document.querySelector('.navigation-tabs')
    if(containerbefore){
      containerbefore.appendChild(orderDesktop)
      if(!document.querySelector('#open-filter')){
        containerbefore.insertAdjacentHTML('beforebegin', '<a id="open-filter" href="javascript:void(0)">Filtrar</a>');
      }
     
    }
  }

}
const hideProductsoffStop=()=>{
  let articlesProducts = document.querySelectorAll('article.False')
  if(articlesProducts.length<1) return false
  articlesProducts.forEach(product => {
      product.parentElement.parentElement.style.display="none"
  });
}

const openMenuMobile=()=>{
  let btnOpen = document.querySelector('#open-filter')
  if(btnOpen){
    btnOpen.addEventListener('click',()=>{
      let menu = document.querySelector('.menu-departamento')
      if(!btnOpen.classList.contains('active')){
        menu.classList.add('active');
        document.querySelector('body').style.overflowY="hidden"
        btnOpen.classList.add('active')
        document.querySelector('.navigation-tabs').style.zIndex="5"
      }else{
        menu.classList.remove('active')
        btnOpen.classList.remove('active')
        document.querySelector('body').style.overflowY="inherit"
        document.querySelector('.navigation-tabs').style.zIndex="0"
      }
    })
  } 
}

const toggleMenuFilters=()=>{
  let btnsfilters = document.querySelectorAll('fieldset>h5')
  let filters =document.querySelectorAll('fieldset>div')
  
  btnsfilters.forEach(btn => {
    btn.addEventListener('click',()=>{
      let isClickFilter= btn.nextElementSibling.classList.contains('close__div')
      filters.forEach(filter => {
        if(!filter.classList.contains('close__div')){
          filter.classList.add('close__div')
          filter.previousElementSibling.classList.add('active__panel')
        }
      });
      // //console.log('ESTA ABIERTO: ',isClickFilter)
      if(!isClickFilter){
        btn.nextElementSibling.classList.remove('close__div')
        btn.classList.remove('active__panel')
      }else{
        btn.nextElementSibling.classList.add('close__div')
        btn.classList.add('active__panel')
      }      
    })
  });

}
const listernerSelect=()=>{
  let select = document.querySelector('#orderbyFilterSelect') 
  select.addEventListener('change',()=>{
    //console.log('cambio de precio')
    setTimeout(()=>{
      textContentBrand()
    },900)
    setTimeout(()=>{
      printPriceProductsOut()
    },2000)
    
  })
}

window.addEventListener('load', function () {
  listernerSelect()
  setInterval(() => {
    textContentBrand()
  }, .2);
  setTimeout(()=>{
    printPriceProductsOut()
  },2000)

})