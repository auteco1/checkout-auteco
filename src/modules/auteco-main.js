import '../styles/main.scss'
// import '../files/js/JS.aut.col.functions.base.js';

const hoverMenu = () => {
  /* hover menu */
  let timer
  const delay = 2
  $('.header-qd-v1-amazing-menu>ul>li').hover(function () {
    const $this = $(this)
    timer = setTimeout(function () {
      $this.find('.active-menu').css({
        opacity: 1,
        visibility: 'visible'
      })
    }, delay)
  }, function () {
    const $this = $(this)
    clearTimeout(timer)
    $this.find('.active-menu').css({
      opacity: 0,
      visibility: 'hidden'
    })
  })
  /* hover menu */
}

/** FORMULARIO DE PAGINA PRODUCTO */
const clickSendFormProduct = () => {
  const modal = $('.call-me-modal')
  $('a.sendInfo').on('click', function (e) {
    e.preventDefault()

    const departmentValue = $('#departament-dropdown').val()
    const cityValue = $('#cities-dropdown').val()

    if (!modal.find('form')[0].checkValidity() || departmentValue == null || cityValue == null) {
      showErrorsForm()
      return false
    }

    $('a.sendInfo').hide()
    $('.content-loading-button').show()
    setTimeout(function () {
      $('.content-loading-button').hide()
      sendDataFormProduct()
    }, 2000)
  })
}

const saveTempData = () => {
  const modal = $('.call-me-modal')
  const ret = {}
  modal.find('form').serializeArray().forEach(function (elem) {
    ret[elem.name] = elem.value
  })

  let marcaVehiculo = $('.brand:first').text()

  switch (marcaVehiculo) {
    case 'Motos Kymco':
      marcaVehiculo = 'KYMCO'
      break
    case 'Motos Victory':
      marcaVehiculo = 'VICTORY'
      break
    case 'Vehículos Stärk':
      marcaVehiculo = 'STARK'
      break
    case 'Motos Kawasaki':
      marcaVehiculo = 'KAWASAKI'
      break
    case 'Bicicletas y Motos Starker':
      marcaVehiculo = 'STARKER'
      break
    case 'BICICLETAS ELÉCTRICAS WOLF':
      marcaVehiculo = 'WOLF'
      break
    case 'Motos Benelli':
      marcaVehiculo = 'BENELLI'
      break
    case 'Motocarros Piaggio':
      marcaVehiculo = 'PIAGGIO'
      break
    case 'Vehículos Jac':
      marcaVehiculo = 'JAC'
      break
    case 'Vehículos Dongfeng':
      marcaVehiculo = 'DONGFENG'
      break
    case 'Motos Combat':
      marcaVehiculo = 'COMBAT'
      break
    case 'Motocarros Ceronte':
      marcaVehiculo = 'CERONTE'
      break
    case 'Motos Super soco':
      marcaVehiculo = 'SUPER SOCO'
      break
    case 'Cuadriciclos zhidou':
      marcaVehiculo = 'ZHIDOU'
      break
    case 'Patinetas eléctricas velocifero':
      marcaVehiculo = 'VELOCIFERO'
      break
    case 'Velocifero':
      marcaVehiculo = 'VELOCIFERO'
      break
    default:
  }

  ret.brand = marcaVehiculo
  ret.name = $('input[name="name"]').val()
  ret.email = $('input[name="email"]').val()
  ret.document_number = $('input[name="document_number"]').val()
  ret.cellular = $('input[name="cellular"]').val()
  ret.moto = $('.productReference:first').text()
  ret.fuente_lead = 'PAGINA PRODUCTO SITIO WEB'
  ret.department = $('#departament-dropdown').children('option:selected').text()
  ret.city = $('#cities-dropdown').children('option:selected').text()
  ret.code_dane = $('#cities-dropdown').children('option:selected').val()
  ret.strategy = 'WEB ORGANICO'
  ret.url_product = window.location.href
  ret.id_product = skuJson.productId
  ret.category_product = vtxctx.categoryName

  $('.email-thank').val(ret.email)

  // console.log(ret)
  return ret
}

const sendDataFormProduct = () => {
  const modal = $('.call-me-modal')
  const formData = saveTempData()

  // console.log(formData)
  $.ajax({
    url: '/api/dataentities/OC/documents?_fields=_all',
    type: 'PATCH',
    dataType: 'json',
    headers: {
      Accept: 'application/vnd.vtex.ds.v10+json',
      'Content-Type': 'application/json; charset=utf-8'
    },
    data: JSON.stringify(formData),
    success: function () {
      /** consultar especificaciones del producto
            //console.log(skuJson.productId);
            var idProduct = skuJson.productId;
            var data = {
                "id" : idProduct,
                "email": formData.email
            }
            var attributesProduct = laQuieroSales(data);
            console.log(attributesProduct); */

      /** validar si usuario exite */

      modal.find('form.callMe').hide()
      modal.find('.contact-qd-v1-form-success').show()
    }
  })
}
/** FORMULARIO DE PAGINA PRODUCTO */

/** FORMULARIO DE WHATSAPP BOT */
const clickSendFormWhatsapp = () => {
  const modal = $('.contact-qd-v1-form')
  $('a.sendContactWhatsapp').on('click', function (e) {
    const departmentValue = $('#departament-dropdown').val()
    const cityValue = $('#cities-dropdown').val()

    if (!modal.find('form')[0].checkValidity() || departmentValue == null || cityValue == null) {
      showErrorsForm()
      return false
    }

    $('a.sendContactWhatsapp').hide()
    $('.content-loading-button').show()
    setTimeout(function () {
      $('a.sendContactWhatsapp').hide()
      $('.content-loading-button').hide()
      saveTempDataWhatsapp()
      sendDataFormWhatsapp()
    }, 2000)
  })
}

const sendDataFormWhatsapp = () => {
  const modal = $('.contact-qd-v1-form')
  const formData = saveTempDataWhatsapp()
  // console.log(formData)
  $.ajax({
    url: '/api/dataentities/CA/documents?_fields=_all',
    type: 'PATCH',
    dataType: 'json',
    headers: {
      Accept: 'application/vnd.vtex.ds.v10+json',
      'Content-Type': 'application/json; charset=utf-8'
    },
    data: JSON.stringify(formData),
    success: function () {
      $('.contact-qd-v1-form').hide()
      $('.contact-qd-v1-form-success').show()
    }
  })
}

const saveTempDataWhatsapp = () => {
  const ret = {}
  $('form:visible').serializeArray().forEach(function (elem) {
    ret[elem.name] = elem.value
  })
  if ($('form:visible').is('#OC')) {
    ret.brand = ret.brands
    ret.moto = $('#form-field-motosdropdown').children('option:selected').text() 
    ret.department = $('#departament-dropdown').children('option:selected').text()
    ret.city = $('#cities-dropdown').children('option:selected').text()
    ret.code_dane = $('#cities-dropdown').children('option:selected').val()

    delete ret.moto_types
    delete ret.brands
  }

  const queryString = window.location.search
  const urlParams = new URLSearchParams(queryString)
  const fuente = urlParams.get('fuente')

  ret.strategy = 'CANALES DE ATENCIÓN AM'
  if (fuente === 'Whatsapp') {
    ret.fuente_lead = 'BOT WHATSAPP'
  }
  return ret
}
/** FORMULARIO DE WHATSAPP BOT */

/** FORMULARIO Quejas y reclamos */
const clickSendFormQuejas = () => {
  const tempData = {}
  $('a.sendContactQuejas').on('click', function (e) {
    e.preventDefault()
    // SE CAPTURA LA CIUDAD Y REGION POR IP
    // getCiudadIp();
    if (!$('form:visible')[0].checkValidity()) {
      showErrorsForm()
      console.log('error')
      return
    }

    setTimeout(function () {
      $('a.sendContactQuejas').hide()
      saveTempDataQuejas()
      sendDataFormQuejas()
    }, 500)
  })
}
const sendDataFormQuejas = () => {
  const formData = saveTempDataQuejas()
  $.ajax({
    url: '/api/dataentities/CT/documents',
    type: 'PATCH',
    dataType: 'json',
    headers: {
      Accept: 'application/vnd.vtex.ds.v10+json',
      'Content-Type': 'application/json; charset=utf-8'
    },
    data: JSON.stringify(formData)
  }).done(function () {
    $('.contact-qd-v1-form').hide()
    $('.contact-qd-v1-form-success').show()
    $(window).scrollTop(0)
  })
}
const saveTempDataQuejas = () => {
  const ret = {}
  $('form:visible').serializeArray().forEach(function (elem) {
    ret[elem.name] = elem.value
  })
  return ret
  // tempData[$(".contact-qd-v1-form-type select").val()] = $.extend(tempData[$(".contact-qd-v1-form-type select").val()], ret);
}
const selectedQuejas = () => {
  const motivo = $('#motivo-contacto-dropdown')
  motivo.prop('selectedIndex', 0)

  $(motivo).change(function () {
    const motivoSelected = $(this).val()
    $('#marca-dropdown').prop('selectedIndex', 0)
    $('.content-referencia input').val('')
    $('.content-other-brand input').val('')

    if (motivoSelected === 'servicio') {
      $('div').removeClass('no-margin')
      $('.interes-servicios').show()
      $('.interes-servicios #servicio-contacto-dropdown').prop('required', true)

      $('.interes-marcas #marca-dropdown').prop('selectedIndex', 0)
      $('.interes-marcas').hide()
      $('.interes-marcas #marca-dropdown').prop('required', false)

      $('.content-other-brand').hide()
      $('.content-other-brand input').prop('required', false)

      $('.content-referencia').hide()
      $('.content-referencia input').prop('required', false)

      $('.interes-servicios').removeClass('col-sm-6')
      $('.interes-servicios').addClass('col-sm-12')
    }

    if (motivoSelected === 'producto') {
      $('.interes-servicios').hide()
      $('.interes-servicios #servicio-contacto-dropdown').prop('selectedIndex', 0)
      $('.interes-servicios #servicio-contacto-dropdown').prop('required', false)

      $('div').removeClass('no-margin')
      $('.interes-marcas').show()
      $('.interes-marcas #marca-dropdown').prop('required', true)

      let motos = {}
      const byBrand = {}
      const wrapperBrand = $('#marca-dropdown')

      // Populate dropdown with list of provinces
      $.getScript('https://auteco.vteximg.com.br/arquivos/auteco-motos.js', function (e) {
        motos = window.QD_jsMotos.mydata

        if (!motos) { return }

        for (const i in motos) {
          if (typeof motos[i] !== 'object') continue
          byBrand[motos[i].key] = byBrand[motos[i].key] || {}
          byBrand[motos[i].key][motos[i].value1] = motos[i]
        }
        populateSelect()
      })

      wrapperBrand.on('change', function () {
        populateSelect($(this).val())
      })

      function populateSelect (brandSelected) {
        if (!brandSelected) {
          wrapperBrand.prop('selectedIndex', 0)
          wrapperBrand.empty().append('<option value="">Elige una marca</option>')
          for (const i in byBrand) {
            wrapperBrand.append('<option value="' + i + '">' + i + '</option>')
          }
          wrapperBrand.append('<option value="OTRA">OTRA</option>')
        }
      }

      $(wrapperBrand).change(function () {
        $('.content-referencia input').val('')
        $('.content-other-brand input').val('')

        if ($(this).val() === 'OTRA') {
          $('.content-referencia').removeClass('col-sm-12')
          $('.content-referencia').addClass('col-sm-6')

          $('.content-other-brand').removeClass('col-sm-12')
          $('.content-other-brand').addClass('col-sm-6')

          $('.content-other-brand').show()
          $('.content-other-brand input').prop('required', true)

          $('.content-referencia').show()
          $('.content-referencia input').prop('required', true)
        } else {
          $('.content-other-brand').hide()
          $('.content-other-brand input').prop('required', false)

          $('.content-referencia').removeClass('col-sm-6')
          $('.content-referencia').addClass('col-sm-12')
          $('.content-referencia').show()
          $('.content-referencia input').prop('required', true)
        }
      })
    }
  })
}
/** FORMULARIO Quejas y reclamos */

/** FORMULARIO CONTACTO ASESOR */
const clickSendFormAsesor = () => {
  const tempData = {}
  $('a.sendContactAsesor').on('click', function (e) {
    // alert('hola')
    e.preventDefault()
    if (!$('form:visible')[0].checkValidity()) {
      showErrorsForm()
      return false
    }
    $('a.sendContactAsesor').hide()
    $('.content-loading-button').show()
    setTimeout(function () {
      $('a.sendContactAsesor').show()
      $('.content-loading-button').hide()
      sendDataFormAsesor()
    }, 3000)
  })
}
const saveTempDataAsesor = () => {
  const ret = {}
  $('form:visible').serializeArray().forEach(function (elem) {
    ret[elem.name] = elem.value
  })

  ret.brand = ret.brand
  ret.moto = $('#vehiculo').children('option:selected').text()
  ret.department = $('#departament-dropdown').children('option:selected').text()
  ret.city = $('#cities-dropdown').children('option:selected').text()
  ret.code_dane = $('#cities-dropdown').children('option:selected').val()
  ret.asesor_id = $('#asesor_id').children('option:selected').val()
  ret.fuente_lead = $('#fuente_lead').children('option:selected').val()
  ret.strategy = 'CANALES DE ATENCIÓN AM'

  if (ret.fuente_lead === 'RECOMPRA') {
    ret.strategy = 'RECOMPRA'
  }

  delete ret.moto_type
  return ret
}
const sendDataFormAsesor = () => {
  const formData = saveTempDataAsesor()
  let entity = 'CA'
  if (formData.fuente_lead === 'SUFI - SITIO WEB' || formData.fuente_lead === 'CREDITO APROBADO - SUFI TU 360' || formData.fuente_lead === 'SUFI - CREDITO APROBADO') {
    entity = 'OF'
    formData.strategy = 'SUFI'
    // delete formData.asesor_id
    delete formData.numer_ticket
  }
  // console.log(formData)
  $.ajax({
    url: '/api/dataentities/' + entity + '/documents',
    type: 'POST',
    dataType: 'json',
    headers: {
      Accept: 'application/vnd.vtex.ds.v10+json',
      'Content-Type': 'application/json'
    },
    data: JSON.stringify(formData),
    success: function () {
      $('.contact-qd-v1-form').hide()
      $('.contact-qd-v1-form-success').show()
    }
  })
}
/** FORMULARIO CONTACTO ASESOR */

/** SELECTED CIUDAD Y DEPARTMANETO */
const selectedDropdownCities = () => {
  const dropdown = $('#departament-dropdown')
  const dropdownCities = $('#cities-dropdown')

  dropdown.empty()

  dropdown.append('<option disabled>Selecciona departamento</option>')
  dropdown.prop('selectedIndex', 0)

  dropdownCities.append('<option disabled>Selecciona Ciudad</option>')
  dropdownCities.prop('selectedIndex', 0)

  const url = 'https:///media.autecomobility.com/recursos/web/contactanos/departamentos/BD-department-cities.json'

  // Populate dropdown with list of provinces
  $.getJSON(url, function (data) {
    const departments = new Array()
    $.each(data, function (key, entry) {
      departments.push(entry.NOMBRE_DEPARTAMENTO)
    })

    const arrayDepartment = $.unique(departments).sort()

    $.each($.unique(arrayDepartment), function (key, value) {
      dropdown.append($('<option></option>').attr('value', value).text(value))
    })

    $('select#departament-dropdown').change(function () {
      dropdownCities.empty()
      dropdownCities.append('<option disabled>Selecciona Ciudad</option>')
      dropdownCities.prop('selectedIndex', 0)

      const selectedDepartamentID = $(this).children('option:selected').val()
      // alert(selectedDepartamentID);
      $('.region-register').val(selectedDepartamentID)

      $.each(data, function (key, entry) {
        if (selectedDepartamentID === entry.NOMBRE_DEPARTAMENTO) {
          dropdownCities.append($('<option></option>').attr('value', entry.codigo_dane).text(entry.NOMBRE_MUNICIPIO))
        }
      })
    })
  })
}
/** SELECTED CIUDAD Y DEPARTMANETO */

/** SELECTED DE MARCA */
const selectedDropdownBrandParams = () => {
  const queryString = window.location.search
  const urlParams = new URLSearchParams(queryString)
  const marca = urlParams.get('marca')
  const fuente = urlParams.get('fuente')

  const dropdowBrand = jQuery('#form-field-marcadropdown')
  const dropdownMotorcycle = jQuery('#form-field-motosdropdown')

  dropdowBrand.empty()

  dropdowBrand.append('<option selected="true" disabled>Selecciona Marca</option>')
  dropdowBrand.prop('selectedIndex', 0)

  dropdownMotorcycle.empty()

  dropdownMotorcycle.append('<option selected="true" disabled>Selecciona Moto</option>')
  dropdownMotorcycle.prop('selectedIndex', 0)

  const url = 'https://media.autecomobility.com/recursos/web/contactanos/auteco-motos.json'
  // Populate dropdown with list of provinces
  jQuery.get(url, function (data) {
    const marcas = new Array()
    jQuery.each(data, function (key, entry) {
      marcas.push(entry.key)
    })

    jQuery.each(jQuery.unique(marcas), function (key, value) {
      if (value === marca) {
        dropdowBrand.append(jQuery('<option></option>').attr('value', value).attr('selected', true).text(value))

        dropdownMotorcycle.empty()
        dropdownMotorcycle.append('<option selected="true" disabled>Selecciona Moto</option>')
        dropdownMotorcycle.prop('selectedIndex', 0)

        jQuery.each(data, function (key, entry) {
          if (marca === entry.key) {
            dropdownMotorcycle.append(jQuery('<option></option>').text(entry.value1))
          }
        })
      } else {
        dropdowBrand.append(jQuery('<option></option>').attr('value', value).text(value))

        jQuery('select#form-field-marcadropdown').change(function () {
          dropdownMotorcycle.empty()
          dropdownMotorcycle.append('<option selected="true" disabled>Selecciona Moto</option>')
          dropdownMotorcycle.prop('selectedIndex', 0)

          const selectedBrandNombre = jQuery(this).val()
          jQuery('#form-field-brand').val(selectedBrandNombre)

          jQuery.each(data, function (key, entry) {
            if (selectedBrandNombre === entry.key) {
              dropdownMotorcycle.append(jQuery('<option></option>').text(entry.value1))
            }
          })
        })
      }
    })

    jQuery('select#form-field-motosdropdown').change(function () {
      const selectedMotoName = jQuery(this).children('option:selected').text()
      jQuery('#form-field-moto').val(selectedMotoName)
    })
  })
}
/** SELECTED DE MARCA */

/** Manejo de errores */
const showErrorsForm = () => {
  $('form:visible').find('.error').removeClass('error')
  $('[required]', 'form:visible').each(function () {
    if (!this.checkValidity() || !$(this).val()) $(this).closest('fieldset').addClass('error')
  })
  $('input, textarea, select, .radioType', '.error').on('focus change click', function () {
    $(this).closest('.error').removeClass('error')
  })
}
/** Manejo de errores */

const stickyMobile = () => {
  //* banners web y mobile*/
  let isMobile = false
  if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) {
    isMobile = true

    // console.log('entro');
    $('.breadcrumb-qd-v1 .brandName.Motos-Kawasaki a').attr('href', '/kawasaki')
    $('.breadcrumb-qd-v1 .brandName.Motos-Kawasaki a').html('<img src="https://media.autecomobility.com//recursos/imagenes/botones-mas/BOTON_MAS_DE_kawasaki_mobile.jpg" title="Kawasaki"/>')
    $('.breadcrumb-qd-v1 .brandName.Motos-Kawasaki a').addClass('marcador-show')

    $('.breadcrumb-qd-v1 .brandName.Motos-Victory a').attr('href', '/victory')
    $('.breadcrumb-qd-v1 .brandName.Motos-Victory a').html('<img src="https://media.autecomobility.com/recursos/imagenes/botones-mas/BOTON_MAS_DE_victory_mobile.jpg" title="Te sentirás como un triunfador. Conócelas" alt="Te sentirás como un triunfador. Conócelas"/>')
    $('.breadcrumb-qd-v1 .brandName.Motos-Victory a').addClass('marcador-show')

    $('.breadcrumb-qd-v1 .brandName.Motos-Kymco a').attr('href', '/kymco')
    $('.breadcrumb-qd-v1 .brandName.Motos-Kymco a').html('<img src="https://media.autecomobility.com/recursos/imagenes/botones-mas/BOTON_MAS_DE_kymco_mobile.jpg" title="La marca experta en motos fáciles de manejar. conócelas!" alt="La marca experta en motos fáciles de manejar. conócelas!"/>')
    $('.breadcrumb-qd-v1 .brandName.Motos-Kymco a').addClass('marcador-show')

    $('.breadcrumb-qd-v1 .brandName.Bicicletas-y-Motos-Starker a').attr('href', '/starker')
    $('.breadcrumb-qd-v1 .brandName.Bicicletas-y-Motos-Starker a').html('<img src="https://media.autecomobility.com/recursos/imagenes/botones-mas/BOTON_MAS_DE_starker_mobile.jpg" title="Líderes en movilidad eléctrica" alt="Líderes en movilidad eléctrica"/>')
    $('.breadcrumb-qd-v1 .brandName.Bicicletas-y-Motos-Starker a').addClass('marcador-show')

    let url = $('.icon-wp').attr('href')
    if (url) {
      url = url.replace('https://web.whatsapp.com/send?phone=', 'https://api.whatsapp.com/send?phone=')
      $('.icon-wp').attr('href', url)
    }
  } else {
    $('.breadcrumb-qd-v1 .brandName.Motos-Kawasaki a').attr('href', '/kawasaki')
    $('.breadcrumb-qd-v1 .brandName.Motos-Kawasaki a').html('<img src="https://media.autecomobility.com/recursos/imagenes/botones-mas/BOTON_MAS_DE_kawasaki.jpg" title="Pura adrenalina verde. Conócelas" alt="Pura adrenalina verde. Conócelas"/>')
    $('.breadcrumb-qd-v1 .brandName.Motos-Kawasaki a').addClass('marcador-show')

    $('.breadcrumb-qd-v1 .brandName.Motos-Victory a').attr('href', '/victory')
    $('.breadcrumb-qd-v1 .brandName.Motos-Victory a').html('<img src="https://media.autecomobility.com/recursos/imagenes/botones-mas/BOTON_MAS_DE_victory.jpg" title="Te sentirás como un triunfador. Conócelas" alt="Te sentirás como un triunfador. Conócelas"/>')
    $('.breadcrumb-qd-v1 .brandName.Motos-Victory a').addClass('marcador-show')

    $('.breadcrumb-qd-v1 .brandName.Motos-Kymco a').attr('href', '/kymco')
    $('.breadcrumb-qd-v1 .brandName.Motos-Kymco a').html('<img src="https://media.autecomobility.com/recursos/imagenes/botones-mas/BOTON_MAS_DE_kymco.jpg" title="La marca experta en motos fáciles de manejar. conócelas!" alt="La marca experta en motos fáciles de manejar. conócelas!"/>')
    $('.breadcrumb-qd-v1 .brandName.Motos-Kymco a').addClass('marcador-show')

    $('.breadcrumb-qd-v1 .brandName.Bicicletas-y-Motos-Starker a').attr('href', '/starker')
    $('.breadcrumb-qd-v1 .brandName.Bicicletas-y-Motos-Starker a').html('<img src="https://media.autecomobility.com/recursos/imagenes/botones-mas/BOTON_MAS_DE_starker.jpg" title="Líderes en movilidad eléctrica" alt="Líderes en movilidad eléctrica"/>')
    $('.breadcrumb-qd-v1 .brandName.Bicicletas-y-Motos-Starker a').addClass('marcador-show')
  }
}

document.addEventListener('DOMContentLoaded', (e) => {
  hoverMenu()
  stickyMobile()
  selectedDropdownBrandParams()
  selectedDropdownCities()
  selectedQuejas()
  clickSendFormQuejas()
  clickSendFormWhatsapp()
  clickSendFormAsesor()
  clickSendFormProduct()
})
