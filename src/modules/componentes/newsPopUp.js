let map, marker;
async function iniciarMap() {
    findMe()
    console.log('ENTRO A POP UP 2')
    changeAddress()
    document.querySelector('.btn_location').addEventListener('click', findMe())
}

const findMe = () => {
    let output = document.getElementById('map')
    if (navigator.geolocation) {
        output
    } else {
        output.innerHTML = "<p>Tu navegador no soporta Geolocalización</p>"
    }
    function localizacion(position) {
        let latitude = position.coords.latitude
        let longitude = position.coords.longitude

        let coord = { lat: latitude, lng: longitude };
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 10,
            center: coord
        });
        marker = new google.maps.Marker({
            position: coord,
            map: map
        });
    }

    function error() {
        output.innerHTML = "<p>No se pudo obtener tu ubicación actual</p>"
    }
    navigator.geolocation.getCurrentPosition(localizacion, error);
}
const changeAddress =  () => {
    let inputAdress = document.querySelector('#input_place')
    inputAdress.addEventListener('keyup', async  () => {
        await getAddress(inputAdress.value)
    })

}
const getAddress = async (address) => {
    let keyGoogle = "AIzaSyBPA-A4kloeKDpQkh7hd7rTIM8mOE8j3tY"
    try {
        const response = await fetch(`https://maps.googleapis.com/maps/api/geocode/json?address=${encodeURIComponent(address)}, Bogota, Cundinamarca, Colombia")"&key=${keyGoogle}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'cache-control': 'no-cache'
            }
        })
        const json = await response.json()
        if (json["status"] == "OK") {
            console.log('ENTRO!!!!', json);
            latitude = json["results"][0]["geometry"]["location"]["lat"];
            longitude = json["results"][0]["geometry"]["location"]["lng"];
            pointZoom = 17;
            changeMap(latitude, longitude, pointZoom);
        } else {
            alert("No se encuentran resultados.");
        }
        return json
    } catch (error) {
        console.log('error en address', error);
    }
    function changeMap(latitude, longitude, pointZoom) {
        deleteMarker();
        map.setCenter(new google.maps.LatLng(latitude, longitude));
        map.setZoom(pointZoom);
        var location = { lat: latitude, lng: longitude };
        marker = new google.maps.Marker({
            position: location,
            map
        });
    }
}
function deleteMarker() {
    if (marker != "") {
        marker.setMap(null);
    }
}
export default iniciarMap;