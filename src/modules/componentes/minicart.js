import MinicartObj from './utils/minicartClass'

let element;
element = 'minicartDeskt'

export const minicartInstance = new MinicartObj({
    divContMinicart: element,
    btnTotalProduct: '.numberofItems',
    classActiveMinicart: 'active'
});
