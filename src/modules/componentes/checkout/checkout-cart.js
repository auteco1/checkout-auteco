import newDirection from "./newDirection";

const btnBuy = () => {

    const buttonToCart = document.getElementById('cart-to-orderform')
    buttonToCart && buttonToCart.addEventListener('click', () => {
        window.scroll({
            top: 0,
            left: 0,
            behavior: 'smooth'
        });

    })

    const buttonToCartCome = document.getElementById('orderform-minicart-to-cart')
    buttonToCartCome && buttonToCartCome.addEventListener('click', () => {
        // console.log('Button Clicked volver');
        window.scroll({
            top: 0,
            left: 0,
            behavior: 'smooth'
        });

    })

}



const newHTML = () => {
    const html = '<p class="srp-description mw5">Vea todas las opciones de envío para sus productos, incluyendo los plazos y los precios de envío</p>'
    return html
}

function addText() {

    if (location.hash == '#/cart') {

        const texto = document.getElementById('shipping-calculate-link')
        const contenText = document.getElementById('shipping-preview-container')


        if (texto) {
            texto.addEventListener('click', () => {
                contenText && contenText.insertAdjacentHTML('afterbegin', newHTML())
                // console.log("texto, click")

            })
        }

    }

}

/* INTEGRACION QR - CONSTRUCTOR MODAL */

let PopupInformativo = `
<h3>El carrito se creó con éxito</h3>
<p>Para continuar tu proceso escanea el código QR con la cámara de tu celular.</p>
<div id="qrcode"></div>
<div class="buttonQR">Aceptar</div>`;

const bloqueQR = () =>{
    const cajaContenedora = '<div class="botonGeneraQR" id="generadorQR"></div>';
    document.getElementById("checkoutMainContainer").insertAdjacentHTML("afterend", cajaContenedora);
    let botonInfo = document.querySelector("#generadorQR");
      botonInfo.addEventListener("click", (e) => {
        
        let   nuevoCarro = vtexjs.checkout.orderFormId;
        creadorPopup(PopupInformativo);
        new QRCode(document.getElementById("qrcode"), "https://www.autecomobility.com/checkout/?orderFormId=" + nuevoCarro + "#cart");
    });
   
}

function creadorPopup(valor) {
    var divPopup = document.createElement("div");
    divPopup.classList.add("popup");
    document.body.appendChild(divPopup);
    let contenidoPopup = `<div class="contenido">
            ${valor}
            <div class="cerrarPopup">&times;</div></div>`;
    divPopup.innerHTML = contenidoPopup;
    let cerrarPopup = document.querySelector(".cerrarPopup");
    cerrarPopup.addEventListener("click", (e) => {
      document.querySelector(".popup").remove();
    });
    let cerrarPopupBoton = document.querySelector(".buttonQR");
    cerrarPopupBoton.addEventListener("click", (e) => {
      document.querySelector(".popup").remove();
    });
}

/*** INTEGRACION QR FIN *****/


const newHTML2 = (refId) => {
    const html = `<span class="refId" id="refId">Ref: ${refId}</span>`
    return html
}



function btnIncrementEvento(id) {
    return document.getElementById(`item-quantity-change-increment-${id}`)
}
function btnDecrementEvento(id) {
    return document.getElementById(`item-quantity-change-decrement-${id}`)
}


function addRef() {

    if (location.hash == '#/cart') {

        setTimeout(() => {


            vtexjs.checkout.getOrderForm().done(function (orderForm) {


                const refContent = document.querySelectorAll('.table.cart-items .product-name');
                // console.log("refContent", refContent)
                // console.log("orderForm", orderForm)

                refContent.forEach((element, index) => {

                    setTimeout(() => {
                        btnIncrementEvento(orderForm.items[index].id).addEventListener('click', () => {
                            setTimeout(() => {

                                addRef()
                            }, 1000);
                        })
                        btnDecrementEvento(orderForm.items[index].id).addEventListener('click', () => {
                            setTimeout(() => {

                                addRef()
                            }, 1000);
                        })

                    }, 1000);

                    element.insertAdjacentHTML('beforeend', newHTML2(orderForm.items[index].ean))

                });
            })


        }, 1300);





    }


}

function comeBackHeader() {

    if (window.innerWidth <= 800) {

        const containerDiv = document.createElement("div"),
            aLink = document.createElement("a"),
            imgContent = document.createElement("img"),
            aText = document.createTextNode("Volver"),
            contentHome = document.querySelectorAll('.container.container-main'),
            containerDivExist = document.getElementById('container_comeback_id');

        containerDiv.classList.add("container_comeback_mobile");
        containerDiv.setAttribute('id', "container_comeback_id");

        aLink.classList.add("link_comeback_mobile");
        aLink.setAttribute('id', "link_comeback_id");

        imgContent.classList.add("img_comeback_mobile");
        imgContent.setAttribute('id', "img_comeback_id");
        imgContent.setAttribute('src', "https://qaauteco.vteximg.com.br/arquivos/flech.png");

        aLink.appendChild(imgContent)
        aLink.appendChild(aText)
        containerDiv.appendChild(aLink)

        contentHome.length > 0 && (containerDivExist == null && contentHome[0].insertBefore(containerDiv, contentHome[0].firstChild))

    }


}

const btnDomiciles=()=>{
    let btnCalcular= document.querySelector('#shipping-calculate-link')
    if(btnCalcular){
        btnCalcular.addEventListener('click',()=>{
            newDirection()
        })
    }
   let btnDomicile = document.querySelector('.srp-toggle__delivery')
   if(btnDomicile){
    btnDomicile.addEventListener('click',()=>{
        setTimeout(()=>{
            newDirection()
        },1000)
    })
   }
   let btnchangeAddress =document.querySelector('.srp-address-title')
   if(btnchangeAddress){
    btnchangeAddress.addEventListener('click',()=>{
        setTimeout(()=>{
            newDirection()
        },1000)
    })
   }
}

$(window).on("orderFormUpdated.vtex", function (t, e) {
    btnDomiciles()
})

async function checkoutCart () {
    btnBuy()
    addText()
    comeBackHeader()
    bloqueQR()
    newDirection()
    // changePrice()
    // addRef()
}
export default checkoutCart;