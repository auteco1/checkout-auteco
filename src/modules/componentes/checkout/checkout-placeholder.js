function placeholderEmail() {

    if (location.hash == '#/email') {
        // setTimeout(() => {
            
        // }, 500);
        document.getElementById('client-pre-email').placeholder = "Ingresa tu mail aquí"
     }



}

function placeholderProfile() {

    if (location.hash == '#/profile') {
        const nombres = document.getElementById('client-first-name'),
            apellidos = document.getElementById('client-last-name'),
            documento = document.getElementById('client-new-document'),
            telefono = document.getElementById('client-phone'),
            razonSocial = document.getElementById('client-company-name'),
            nombreEmpresa = document.getElementById('client-company-nickname'),
            nit = document.getElementById('client-company-ie'),
            rut = document.getElementById('client-company-document'),
            clientEmail = document.getElementById('client-email'),
            documentType = document.getElementById('client-document-type')



        clientEmail && (clientEmail.tabIndex = 1)
        nombres ? ((nombres.placeholder = "Ingresa tu nombre") && (nombres.tabIndex = 2)) : null;
        apellidos ? ((apellidos.placeholder = "Ingresa tu apellido") && (apellidos.tabIndex = 3)) : null;
        documentType && (documentType.tabIndex = 4)
        documento ? ((documento.placeholder = "Ingresa tu documento") && (documento.tabIndex = 5)) : null;
        documento && (documento.type = "number");
        documento && (documento.required = "required");
        telefono ? ((telefono.placeholder = "Ingresa tu telefono") && (telefono.tabIndex = 6)) : null;
        razonSocial && (razonSocial.placeholder = "Ingresa la razón social");
        nombreEmpresa && (nombreEmpresa.placeholder = "Ingresa el nombre de la empresa");
        nit && (nit.placeholder = "Ingresa NIT");
        rut && (rut.placeholder = "Ingresa RUT");
        // direccion && (direccion.placeholder = "Carrera 24A # 83 - 15");

    }

    if (location.hash == '#/shipping') {
        const shipNeighborhood = document.getElementById('ship-neighborhood'),
            shipComplement = document.getElementById('ship-complement'),
            shipReceiverName = document.getElementById('ship-receiverName')

        shipNeighborhood && (shipNeighborhood.placeholder = "Ingresa tu barrio")
        shipComplement && (shipComplement.placeholder = "302")
        shipReceiverName && (shipReceiverName.placeholder = "Ingresa nombre de persona que recibe")

    }

    if (location.hash == '#/payment') {

        setTimeout(() => {
            const creditCardpayment = document.querySelectorAll('creditCardpayment-card-0Number');
    
            creditCardpayment && (creditCardpayment.placeholder = "Ingresa número de tarjeta")
            
        }, 1000);



    }

}


function placeholder() {
    // placeholderCart()
    placeholderEmail()
    placeholderProfile()
}
export default placeholder;
