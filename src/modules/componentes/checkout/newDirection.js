
const changeSelectDir = (e, addressGoogle) => {
    let value = "";
    if (addressGoogle && e == undefined) {
        value = addressGoogle
        return value
    } else {
        value = e.target.value
        return value
    }

}
const newHtml = (step) => {

    const html = `
    <div class="newdDirection" id="newdDirection">
        <div class="container-custom-address">
            <div class="container_option_address">
            <label>Departamento</label>
            <select id="departaments" required >
                <option selected value="" >Selecciona una opción</option>
            </select>
        </div>
        <div class="container_option_address" >
            <label>Ciudad</label>
            <select id="city" required>
                <option selected value="" >Selecciona una opción</option>
            </select>
        </div>
        <select id="dirStreet1" name="dirStreet1" class="dirStreet1" required>
            <option disabled selected>Selecciona tipo de vía</option>
            <option value="Avenida">Avenida</option>
            <option value="Calle">Calle</option>
            <option value="Carrera">Carrera</option>
            <option value="Circular">Circular</option>
            <option value="Diagonal">Diagonal</option>
            <option value="Transversal">Transversal</option>
            <option value="Autopista">Autopista</option>
            <option value="Kilometro">Kilometro</option>
            <option value="Circunvalar">Circunvalar</option>
            <option value="Manzana">Manzana</option>
            <option value="Apartado Aéreo">Apartado Aéreo</option>
            <option value="Avenida Calle">Avenida Calle</option>
            <option value="Avenida Carrera">Avenida Carrera</option>
        </select>
        <input type="text" name="dirStreet2" id="dirStreet2"  class="dirStreet2" placeholder="Ej: 12B" disabled="" required>
            <span class="dirStreetSpan">#</span>
        <input type="text" name="dirStreet3" id="dirStreet3" class="dirStreet3" placeholder="65" disabled="" required>
            <span class="dirStreetSpan"> - </span>
            ${step?` <input type="text" name="dirStreet4" id="dirStreet4" class="dirStreet4" placeholder="128" disabled="" required>
            <input type="text" name="barrio" id="barrio" class="barrio" placeholder="Barrio" required>
            <input type="text" name="piso" id="piso" class="piso" placeholder="Conjunto y/o apartamento" required>
            <div class="container_option_address content_input_map">
            </div>
        </div>
        <a href="javascript:void(0)" class="btns_success">Aceptar</a>
        </div>`:`<input type="text" name="dirStreet4" id="dirStreet4" class="dirStreet4" placeholder="128" disabled="" required>
        <div class="container_option_address content_input_map">
            </div>
        </div>
        <a href="javascript:void(0)" class="btns_success">Aceptar</a>
        </div>`}`
    return html
}
const addDirection = (inputDirectionVtex, step) => {
    if (document.getElementById('newdDirection')) {
        //document.getElementById('newdDirection').remove()
    } else {
        inputDirectionVtex.parentElement.insertAdjacentHTML('beforebegin', newHtml(step));
        addDepartamentsMap()
        let dir2 = document.getElementById('dirStreet2'),
            dir3 = document.getElementById('dirStreet3'),
            dir4 = document.getElementById('dirStreet4'),
            updateDir
        document.getElementById('dirStreet1').addEventListener('change', function (e) {
            dir2.value = ''
            dir3.value = ''
            dir4.value = ''
            if (changeSelectDir(e)) {
                inputDirectionVtex.value = changeSelectDir(e)
                dir2.disabled = false
                dir2.focus()
                updateDir = changeSelectDir(e)
            } else {
                inputDirectionVtex.value = ''
                dir2.disabled = true
                dir3.disabled = true
                dir4.disabled = true
                dir2.value = ''
                dir3.value = ''
                dir4.value = ''
            }
        })
        dir2.addEventListener('focusout', () => {
            dir3.value = ''
            dir4.value = ''
            dir3.disabled = false
            inputDirectionVtex.value = updateDir + ' ' + dir2.value
        })
        dir3.addEventListener('focusout', () => {
            dir4.value = ''
            dir4.disabled = false
            inputDirectionVtex.value = updateDir + ' ' + dir2.value + ' # ' + dir3.value
        })
        dir4.addEventListener('focusout', (event) => {
            let string = updateDir + ' ' + dir2.value + ' # ' + dir3.value + ' - ' + dir4.value,
                input = inputDirectionVtex,
                apply = Object.getOwnPropertyDescriptor(window.HTMLInputElement.prototype, "value").set;
            apply.call(input, string)
            let bubble = new Event("input", { bubbles: !0 });
            input.dispatchEvent(bubble);
            if (dir4.value !== "") {
                // inputDirectionVtex.focus()
                let optionsGoogle = document.querySelectorAll('.pac-item')
                if (optionsGoogle.length > 0) {
                    optionsGoogle[0].click()
                }
            }
        })
    }
}
const newDirection = () => {
    //console.log('ENTRO A DIRECCIÓN 1')
    if (location.hash == "#/shipping") {
        // //console.log('ENTRO A DIRECCIÓN 2')
        let inputDirectionVtex = document.getElementById('ship-addressQuery')
        if (inputDirectionVtex) {
            inputDirectionVtex.disabled = true
        }
       
        setTimeout(() => {
            if (inputDirectionVtex) {
                addDirection(inputDirectionVtex,true)
            }else{
                let inputAddress= document.querySelector('#ship-street')
                inputAddress.disabled = true
                addDirection(inputAddress,true)
            }
        }, 1000)
        setTimeout(()=>{
            addInputMap()
            addDepartamentsMap()
            submitAddress(true)
        },1000)
    }
    if(location.hash == "#/cart"){
        setTimeout(()=>{
            let inputDirectionVtex = document.getElementById('ship-addressQuery')
            if (inputDirectionVtex) {
                inputDirectionVtex.disabled = true
            }
            addDepartamentsMap()
            addDirection(inputDirectionVtex,false)
            addInputMap()
            submitAddress(false)
        },100)
    }
}

const sednAddress = (res) => {
    //console.log('RES: ', res)
    let receiverName = vtexjs.checkout.orderForm.clientProfileData.firstName + " " + vtexjs.checkout.orderForm.clientProfileData.lastName,
        address = document.querySelector('#ship-addressQuery')
    vtexjs.checkout.getOrderForm()
        .then(function (orderForm) {
            var shippingData = orderForm.shippingData
            //   shippingData.selectedAddresses[0].geoCoordinates = [res.results[0].geometry.location.lat,res.results[0].geometry.location.lng]
            // shippingData.selectedAddresses[0].street =shippingData.selectedAddresses[0].street.split('Geo')[0]+' Geo:'+coordinates 
            // //console.log('clientProfileData: ', shippingData)
            if (shippingData.selectedAddresses.length > 0) {
                shippingData.selectedAddresses[0].geoCoordinates = [res.results[0].geometry.location.lat, res.results[0].geometry.location.lng]
                shippingData.selectedAddresses[0].street = res.results[0].formatted_address
                shippingData.selectedAddresses[0].city = res.results[0].address_components[4].long_name
                shippingData.selectedAddresses[0].state = res.results[0].address_components[5].long_name
                shippingData.selectedAddresses[0].receiverName = receiverName
                shippingData.selectedAddresses[0].addressType = "residential"
                shippingData.selectedAddresses[0].complement = null
                shippingData.selectedAddresses[0].country = res.results[0].address_components[6].long_name
                shippingData.selectedAddresses[0].isDisposable = ""
                shippingData.selectedAddresses[0].neighborhood = res.results[0].address_components[2].long_name
                shippingData.selectedAddresses[0].number = res.results[0].address_components[7].long_name
                shippingData.selectedAddresses[0].postalCode = res.results[0].address_components[7].long_name
            } else {
                shippingData.selectedAddresses.push({
                    geoCoordinates: [res.results[0].geometry.location.lat, res.results[0].geometry.location.lng],
                    street: res.results[0].formatted_address,
                    city: res.results[0].address_components[4].long_name,
                    state: res.results[0].address_components[5].long_name,
                    receiverName: receiverName,
                    addressType: "residential",
                    addressId: res.results[0].place_id,
                    complement: null,
                    country: res.results[0].address_components[6].long_name,
                    isDisposable: "",
                    neighborhood: res.results[0].address_components[2].long_name,
                    number: res.results[0].address_components[7].long_name,
                    postalCode: res.results[0].address_components[7].long_name,
                    reference: res.results[0].place_id
                });
            }

            return vtexjs.checkout.sendAttachment(
                "shippingData",
                shippingData
            )
        })
        .done(function (orderForm) {
            //console.log("MODIFICADO: ", orderForm.shippingData)
        })
}

const getDepartaments = async () => {
    try {
        const response = await fetch(`../files/dataDepartaments.json`, {
            method: 'GET'
        })
        const json = await response.json()
        //console.log('ENTRO!!!!', json);
        return json
    } catch (error) {
        //console.log('error en departaments get', error);
    }
}

const addDepartamentsMap = async () => {
    const res = await getDepartaments()
    let divDepartaments = document.querySelector('.newdDirection #departaments')
    if (divDepartaments) {
        res.departamentos.map(item => {
            if (!divDepartaments.querySelector('.departament_' + item.id)) {
                divDepartaments.insertAdjacentHTML('beforeend', '<option class="departament_' + item.id + '" value="' + item.departamento + '" >' + item.departamento + '</option>');
            }
        })
        changeDepartament(divDepartaments, res)
    }
}

const changeDepartament = (divDepartaments, res) => {
    divDepartaments.addEventListener('change', () => {
        let valueDepaartament = divDepartaments.value
        let divCity = document.querySelector('.newdDirection #city')
        divCity.innerHTML = ""
        divCity.insertAdjacentHTML('beforeend', '<option selected value="" >Selecciona una opción</option>');
        if (divCity) {
            res.departamentos.filter(item => {
                if (item.departamento == valueDepaartament) {
                    item.ciudades.map(city => {
                        if (!divCity.querySelector('.city_' + city)) {
                            divCity.insertAdjacentHTML('beforeend', '<option class="city_' + city + '" value="' + city + '" >' + city + '</option>');
                        }

                    })
                }
            })
            divCity.addEventListener('change', () => {
                if (divCity.value == "") {
                    document.querySelector('#ship-addressQuery').parentElement.style.display = "none"
                } else {
                    document.querySelector('#ship-addressQuery').parentElement.style.display = "flex"
                }
            })
        }
    })
}

const addInputMap = () => {
    let inputMap = document.querySelector('.input.ship-addressQuery')
    if (inputMap){
        document.querySelector('#newdDirection .content_input_map').append(inputMap)
    }else{
        let inputAddress = document.querySelector('.input.ship-street')
        document.querySelector('#newdDirection .content_input_map').append(inputAddress)
    }
    
}
const submitAddress = (isShipping) => {
    setTimeout(() => {
        let btnSubmit = document.querySelector('.newdDirection .btns_success')
        btnSubmit.addEventListener('click', async () => {
            let address = document.querySelector('.newdDirection #ship-addressQuery')
            if(!address){
                address = document.querySelector('.newdDirection #ship-street')
                if(!address) return false
            }
            let city = document.querySelector('.newdDirection #city')
            let state = document.querySelector('.newdDirection #departaments'),
                coordenadasLupap = await addCoordinates(city.value, state.value, address.value)
            //console.log('COORDENADAS LUPAP', coordenadasLupap);
            if (!coordenadasLupap.message) {
                if (document.querySelector('.newdDirection .error')) {
                    document.querySelector('.newdDirection .error').remove()
                }
                let respuestaVTEX = await sendAddress(coordenadasLupap,isShipping)
                // //console.log('RESPUESTA VTEX : ', respuestaVTEX);
            } else {
                // alert('¡¡Esta dirección no existe!! intenteló nuevamente')
                if (!document.querySelector('.newdDirection .error')) {
                    document.querySelector('#ship-addressQuery').insertAdjacentHTML('afterend', '<p class="error">¡¡Esta dirección no existe!! intentelo nuevamente</p>');
                }
            }

        })
        let btnBack = document.querySelector('.newdDirection .btn_back')
        btnBack.addEventListener('click', () => {
            document.querySelector('.modal_maps').style.display = "none"
            document.querySelector('.opacity-div').style.display = "none"
        })
    }, 2500)
}

const addCoordinates = async (city, state, address) => {

    //console.log('LA DIRECCIÓN ES : ' + city + ' ' + address);
    let webtoken = "4e1c77aca55b138e669bc7ab64827c18e39cfedd"
    try {
        const response = await fetch(`https://api.lupap.co/v2/co/${city}?key=${webtoken}&a=${encodeURIComponent(address)}`, {
            // const response = await fetch(`https://api.lupap.co/v2/${state}/${city}?a=${encodeURIComponent(address)}&key=${webtoken}`, {
            method: 'GET'
        })
        const json = await response.json()
        //console.log('ENTRO!!!!', json);
        return json
    } catch (error) {
        //console.log('error en  get lupap', error);
    }
}

const sendAddress = async (coordenadasLupap,isShipping) => {
    // document.querySelector('#ship-addressQuery').insertAdjacentHTML('afterend', '<p class="success">Diección:' + coordenadasLupap.response.properties.admin5 + ' ' + coordenadasLupap.response.properties.address + ' COORDENADAS :' + coordenadasLupap.response.geometry.coordinates[0] + ' ' + coordenadasLupap.response.geometry.coordinates[0] + '</p>');
    //console.log(('coodenadas lupap: ',coordenadasLupap));
    if(isShipping){
        var barrio = document.querySelector('#barrio').value
        var especificaciónCasa= document.querySelector('#piso').value
        vtexjs.checkout.getOrderForm()
        .then(function (orderForm) {            
            var country = 'COL';
            var address = {
                "geoCoordinates": coordenadasLupap.response.geometry.coordinates,
                "postalCode": coordenadasLupap.response.properties.postcode,
                "country": country,
                "street": coordenadasLupap.response.properties.address,
                "city": coordenadasLupap.response.properties.admin3,
                "state": coordenadasLupap.response.properties.admin2,
                "neighborhood": coordenadasLupap.response.properties.admin5,
                "complement": barrio+' '+especificaciónCasa,
            };
            return vtexjs.checkout.calculateShipping(address)
        })
        .done(function (orderForm) {
            //console.log(orderForm.shippingData);
            //console.log(orderForm.totalizers);
            window.location.reload()
        });
    }else{
        vtexjs.checkout.getOrderForm()
        .then(function (orderForm) {
            var country = 'COL';
            var address = {
                "geoCoordinates": coordenadasLupap.response.geometry.coordinates,
                "postalCode": coordenadasLupap.response.properties.postcode,
                "country": country,
                "street": coordenadasLupap.response.properties.address,
                "city": coordenadasLupap.response.properties.admin3,
                "state": coordenadasLupap.response.properties.admin2,
                "neighborhood": coordenadasLupap.response.properties.admin5,
            };
            return vtexjs.checkout.calculateShipping(address)
        })
        .done(function (orderForm) {
            //console.log(orderForm.shippingData);
            //console.log(orderForm.totalizers);
        });
    }
   
    //     let orderFormId = vtexjs.checkout.orderForm.orderFormId
    // var shippingData = vtexjs.checkout.orderForm.shippingData;
    // let receiverName = vtexjs.checkout.orderForm.clientProfileData.firstName + ' ' + vtexjs.checkout.orderForm.clientProfileData.lastName
    // if(shippingData.length>1){
    //     shippingData.selectedAddresses.push(
    //         {
    //             geoCoordinates: coordenadasLupap.response.geometry.coordinates,
    //             street: coordenadasLupap.response.properties.address,
    //             city: coordenadasLupap.response.properties.admin3,
    //             state: coordenadasLupap.response.properties.admin2,
    //             postalCode: coordenadasLupap.response.properties.postcode,
    //             neighborhood: coordenadasLupap.response.properties.admin5,
    //             receiverName: receiverName,
    //             country: coordenadasLupap.response.properties.country,
    //         });
    //     //console.log('DATA A ENVIAT A VTEX: ', shippingData.selectedAddresses);
    
    
    //     let url = 'https://autecoteste.myvtex.com.br/api/checkout/pub/orderForm/' + orderFormId + '/attachments/shippingDatan';
    //     let method = 'POST';
    //     let createCORSRequest = function (method, url) {
    //         var xhr = new XMLHttpRequest();
    //         if ("withCredentials" in xhr) {
    //             // Most browsers.
    //             xhr.open(method, url, true);
    //         } else if (typeof XDomainRequest != "undefined") {
    //             // IE8 & IE9
    //             xhr = new XDomainRequest();
    //             xhr.open(method, url);
    //         } else {
    //             // CORS not supported.
    //             xhr = null;
    //         }
    //         return xhr;
    //     };
    //     let xhrf = createCORSRequest(method, url);
    
    //     xhrf.onload = function () {
    //         // Success code goes here.
    //         let res = JSON.parse(xhrf.response)
    //         //console.log(' xhrf RESPUESTA: ', res);
    //     };
    
    //     xhrf.onerror = function () {
    //         // Error code goes here.
    //         //console.log('ERROR: ', xhrf)
    //         return false
    //     };
    
    //     // xhrf.setRequestHeader('Content-Type','application/json');
    //     // xhrf.setRequestHeader('cache-control','no-cache');
    //     xhrf.send(JSON.stringify(shippingData));
    // }else{
    //     vtexjs.checkout.getOrderForm()
    //     .then(function (orderForm) {
    //         var geoCoordinates = coordenadasLupap.response.geometry.coordinates
    //         var postalCode = '11000';  // may also be without the hyphen
    //         var country = 'COL';
    //         var address = {
    //             "geoCoordinates": coordenadasLupap.response.geometry.coordinates,
    //             "postalCode": coordenadasLupap.response.properties.postcode,
    //             "country": coordenadasLupap.response.properties.country
    //         };
    //         return vtexjs.checkout.calculateShipping(address)
    //     })
    //     .done(function (orderForm) {
    //         alert('Shipping calculated.');
    //         //console.log(orderForm.shippingData);
    //         //console.log(orderForm.totalizers);
    //     });
    // }
}

export default newDirection
