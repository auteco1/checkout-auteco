
const changeSelectDir = (e) => {
    let value = e.target.value
    return value
}
const newHtml = () => {
    const html = `
    <div class="newdDirection" id="newdDirection">
        <select id="dirStreet1" name="dirStreet1" class="dirStreet1">
            <option disabled selected>Completa tu dirección de entrega</option>
            <option value="Avenida">Avenida</option>
            <option value="Calle">Calle</option>
            <option value="Carrera">Carrera</option>
            <option value="Circular">Circular</option>
            <option value="Diagonal">Diagonal</option>
            <option value="Transversal">Transversal</option>
            <option value="Autopista">Autopista</option>
            <option value="Kilometro">Kilometro</option>
            <option value="Circunvalar">Circunvalar</option>
            <option value="Manzana">Manzana</option>
            <option value="Apartado Aéreo">Apartado Aéreo</option>
            <option value="Avenida Calle">Avenida Calle</option>
            <option value="Avenida Carrera">Avenida Carrera</option>
        </select>
        <div class= "directionContent">
            <input type="text" name="dirStreet2" id="dirStreet2"  class="dirStreet2" placeholder="Ej: 12B" disabled="">
                <span class="dirStreetSpan">#</span>
            <input type="text" name="dirStreet3" id="dirStreet3" class="dirStreet3" placeholder="65" disabled="">
                <span class="dirStreetSpan"> - </span>
            <input type="text" name="dirStreet4" id="dirStreet4" class="dirStreet4" placeholder="128" disabled="">
        </div>
    </div>
    `
    return html
}
const addDirection = (inputDirectionVtex) => {
    // console.log("validar newdDirection", document.getElementById('newdDirection'))
    //document.getElementById('newdDirection').remove()

    // console.log("verificar  si exisste", document.getElementById('newdDirection'))
    // console.log("inputDirectionVtex", inputDirectionVtex)
    if (document.getElementById('newdDirection') == null) {


        inputDirectionVtex && inputDirectionVtex.parentElement.insertAdjacentHTML('beforebegin', newHtml());
        let dir2 = document.getElementById('dirStreet2'),
            dir3 = document.getElementById('dirStreet3'),
            dir4 = document.getElementById('dirStreet4'),
            updateDir
        document.getElementById('dirStreet1').addEventListener('change', function (e) {
            dir2.value = ''
            dir3.value = ''
            dir4.value = ''
            if (changeSelectDir(e)) {
                inputDirectionVtex.value = changeSelectDir(e)
                dir2.disabled = false
                dir2.focus()
                updateDir = changeSelectDir(e)
            } else {
                inputDirectionVtex.value = ''
                dir2.disabled = true
                dir3.disabled = true
                dir4.disabled = true
                dir2.value = ''
                dir3.value = ''
                dir4.value = ''
            }
        })
        dir2.addEventListener('focusout', () => {
            dir3.value = ''
            dir4.value = ''
            dir3.disabled = false
            inputDirectionVtex.value = updateDir + ' ' + dir2.value
        })
        dir3.addEventListener('focusout', () => {
            dir4.value = ''
            dir4.disabled = false
            inputDirectionVtex.value = updateDir + ' ' + dir2.value + ' # ' + dir3.value
        })
        dir4.addEventListener('focusout', (event) => {
            let string = updateDir + ' ' + dir2.value + ' # ' + dir3.value + ' - ' + dir4.value,
                input = inputDirectionVtex,
                apply = Object.getOwnPropertyDescriptor(window.HTMLInputElement.prototype, "value").set;
            apply.call(input, string)
            let bubble = new Event("input", { bubbles: !0 });
            input.dispatchEvent(bubble);
        })
    }
}

function newDirection() {

    if (location.hash == "#/shipping") {
        setTimeout(() => {

            if (document.getElementById('newdDirection') == null) {
                // console.log("Validar newDirection", document.getElementById('newdDirection'))

                const inputDirectionVtex = document.getElementById('ship-street')

                // console.log("inputDirectionVtex", inputDirectionVtex)

                if (inputDirectionVtex) {

                    inputDirectionVtex.disabled = true
                    inputDirectionVtex.placeholder = "Carrera 24A # 83 - 15"
                    addDirection(inputDirectionVtex)

                }

                // setTimeout(() => {
                //     if (inputDirectionVtex) {
                //         addDirection(inputDirectionVtex)
                //     }
                // }, 1000)
            }

        }, 1000);

        const changeDir = document.getElementById('force-shipping-fields')
        if (changeDir) {

            changeDir.addEventListener('click', () => {
                setTimeout(() => {
                    const inputDirectionVtex2 = document.getElementById('ship-street')
                    // console.log("changeDir", inputDirectionVtex)


                    addDirection(inputDirectionVtex2)


                    // console.log(inputDirectionVtex)
                }, 500)
            })
        }

    }




}

export default newDirection