const newHtml = () => {
    const html = `
    <div id ="container-modal-email">
   
            <div id="conteiner-email"></div>
                <div id="email-modal" class="modal">
                    <div class="modal-content">
                        <div class="modal-header">
                            <span class="close" id="close-btn-mail"> <img src="https://qaauteco.vteximg.com.br/arquivos/close-icon.png" alt="close" width="20" height="20"/></span>
                            <h2>¡Atención!</h2>
                        </div>
                        <div class="modal-body">
                            <p>Todos los campos se llenarán automáticamente de acuerdo a los datos guardados.</p>
                        </div>
                        <div class="modal-footer">
                            <button id = "modal-cancelar" type="button" class="btn btn-secondary">Cancelar</button>
                            <button id = "modal-confirmar" type="button" class="btn btn-primary">Confirmar</button>
                        </div>
                    </div>
                </div>
                </div>`
    return html

}



const emailModal = () => {



    if (location.hash == '#/profile') {

        if (document.getElementById('email-modal') == null) {
            const contenedor = document.querySelectorAll('.container.container-main.container-order-form');
            contenedor[0] && contenedor[0].parentElement.insertAdjacentHTML("beforeend", newHtml())

            const closeBTN = document.getElementById('close-btn-mail'),
                contentEmailModal = document.getElementById('conteiner-email'),
                fontEmailModal = document.getElementById('email-modal'),
                modalCancelar = document.getElementById('modal-cancelar'),
                modalconfirmar = document.getElementById('modal-confirmar')

            closeBTN.addEventListener('click', (e) => {
                e.preventDefault()
                // console.log("close-btn")
                contentEmailModal.style.display = "none"
                // fontEmailModal.style.display = "none"
                fontEmailModal.classList.add("inactive-email")
                const parentElement = document.getElementById('container-modal-email')
                setTimeout(() => {
                    parentElement.remove()
                }, 1000);
                // console.log("parentElement", parentElement)
                history.back()
                // location.href = '#/cart'

                // setTimeout(() => {
                //     document.getElementById('container-modal-email').remove()
                // }, 3000);

            })

            contentEmailModal.addEventListener('click', (e) => {
                e.preventDefault()
                // console.log("close-contentEmailModal")
                contentEmailModal.style.display = "none"
                // fontEmailModal.style.display = "none"
                fontEmailModal.classList.add("inactive-email")
                // location.href = '#/cart'


            })
            modalconfirmar.addEventListener('click', (e) => {
                e.preventDefault()
                // console.log("close-contentEmailModal")
                contentEmailModal.style.display = "none"
                // fontEmailModal.style.display = "none"
                fontEmailModal.classList.add("inactive-email")
                // location.href = '#/cart'


            })

            modalCancelar.addEventListener('click', (e) => {
                contentEmailModal.style.display = "none"
                fontEmailModal.classList.add("inactive-email")
                const parentElement = document.getElementById('container-modal-email')
                setTimeout(() => {
                    parentElement.remove()
                }, 1000);
                // console.log("parentElement", parentElement)
                history.back()


                // location.href = '#/cart'

            })

        }



    }


}

const modalPages = () => {
    emailModal()
}

export default modalPages;