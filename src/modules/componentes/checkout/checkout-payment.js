
const newHtml = () => {
    const html = `
    <button id="payment-data-submit" class="submit btn btn-success btn-large btn-block" data-bind="fadeVisible: !window.router.sac.isActive(), click: submit, disable: window.checkout.disablePaymentButton" tabindex="200">
    <i class="icon-lock" data-bind="visible: !window.checkout.disablePaymentButton()" style=""></i>
    <i class="icon-spinner icon-spin" data-bind="visible: window.checkout.disablePaymentButton()" style="display: none;"></i>
    <span data-i18n="paymentData.confirm">Comprar ahora</span>
    </button>`
    return html

}


const payment = () => {

    if (location.hash == "#/payment") {
        // console.log("object #/payment")

        const validacion = document.querySelectorAll(".step.accordion-group.store-country-COL #payment-data-submit");
        const insercion = document.querySelectorAll(".accordion-body.collapse.in.payment-body");

        // console.log("validacion", validacion)

        // console.log("object #/payment", insercion[0])
        if (validacion.length == 0) {
            insercion[0].parentElement.insertAdjacentHTML("beforeend", newHtml())

            document.querySelectorAll(".step.accordion-group.store-country-COL #payment-data-submit")[0].addEventListener('click', () => {
                // console.log("click button")
                document.querySelectorAll(".payment-confirmation-wrap #payment-data-submit")[1].click()
            })

            // setTimeout(() => {
                
            // }, 5000);
        }
        
        
    }
    
}
// addbtnLisen()


// const addbtnLisen = () => {
// //     console.log("funciona", document.querySelectorAll(".accordion-body.collapse.in.payment-body #payment-data-submit"))

// }

export default payment;