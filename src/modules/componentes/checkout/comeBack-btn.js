const buttonBack = () => {

    return `<div class="comeback-container">
                <a data-bind="attr: {href: (window.__RUNTIME__ &amp;&amp; window.__RUNTIME__.rootPath ? (window.__RUNTIME__.rootPath + (window.__RUNTIME__.workspace != 'master' ? ('?workspace=' + window.__RUNTIME__.workspace) : '')) : '/') }" id="comeback-home-page" class="more link-choose-more-products" href="/">
                    Seguir comprando
                </a>
            </div>`;

}

const eventoButtonBack = () => {

    const check = document.getElementById('comeback-home-page'),
        contentAccordionGroup = document.querySelector('#go-to-cart-button');

    // console.log("contentAccordionGroup", contentAccordionGroup)

    if (check === null) {
        contentAccordionGroup && contentAccordionGroup.insertAdjacentHTML('afterbegin', buttonBack())
    }


    if (location.hash == '#/email') {
        const comebackEmail = document.querySelectorAll('#client-profile-data .pre-email.row-fluid .client-email'),
            checkEmail = document.querySelectorAll('#client-profile-data .pre-email.row-fluid .client-email .comeback-container')

        // console.log('#/email', checkEmail)

        if (checkEmail.length == 0) {
            comebackEmail[0].insertAdjacentHTML('beforeend', buttonBack())

        }


    }



}


const comeBack = () => {

    setTimeout(() => {
        const comeBackHeader = document.getElementById('volver-link'),
            comeBackMobile = document.getElementById('link_comeback_id');

        // console.log("comeBackMobile", comeBackMobile)

        comeBackHeader && comeBackHeader.addEventListener('click', () => {
            if (location.hash == '#/email' || location.hash == '#/profile' || location.hash == '#/shipping' || location.hash == '#/payment') {
                location.href = "#/cart";
            } else {
                // location.href = "/";
                history.back()
            }
        });
        comeBackMobile && comeBackMobile.addEventListener('click', () => {
            if (location.hash == '#/email' || location.hash == '#/profile' || location.hash == '#/shipping' || location.hash == '#/payment') {
                location.href = "#/cart";
            } else {
                // location.href = "/";
                history.back()
            }

        });
    }, 1500);


}


const buttonBackCheckout = () => {
    eventoButtonBack()
    comeBack()
}

export default buttonBackCheckout;