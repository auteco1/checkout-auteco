function autecoFunction() {


    //actualizar
    setTimeout(function () {

        //condición de compra minima aliados
        if (location.hash == '#/cart') {
            var total = document.querySelectorAll('[data-bind="text: totalLabel"]')[0].innerHTML;
            var totalformat = total.replace(/[.,\s]/g, '');
            var nocurrency = totalformat.replace('$', '');
            if (nocurrency <= 40000) {
                alert("El valor de la compra debe ser mayor o igual a $40.000");
                window.location.href = "https://www.autecomobility.com";
            }
        }

        var total = document.querySelectorAll('[data-bind="text: totalLabel"]')[0];
        total && (total = document.querySelectorAll('[data-bind="text: totalLabel"]')[0].innerHTML)
        var totalformat = total && total.replace(/[.,\s]/g, '');
        var nocurrency = totalformat && totalformat.replace('$', '');

        // ocultar pagos en efectivo si son mayores a 3 millones
        setTimeout(function () {
            if (nocurrency >= 3000000 || nocurrency <= 20000) {
                document.querySelectorAll(".bank-invoice-item-efecty")[0].style.display = "none";

                document.querySelectorAll(".bank-invoice-item-davivienda")[0] ? document.querySelectorAll(".bank-invoice-item-davivienda")[0].style.display = "none" : null
                // document.querySelectorAll(".bank-invoice-item-davivienda")[0].style.display = "none";

                if (nocurrency >= 3000000) {
                    document.querySelectorAll(".bank-invoice-item-baloto")[0].style.display = "none";
                    document.querySelectorAll(".pg-baloto, .pg-efecty ")[0].style.display = "none";
                    document.getElementById("payment-group-creditCardPaymentGroup").click();
                }
            }

            if (nocurrency >= 1000000) {
                // console.log('entro borrar');
                //document.querySelectorAll(".box-payment-group2")[0].style.display = "block";
                document.querySelectorAll(".bank-invoice-item-baloto")[0].style.display = "none";
            }
        }, 1400);

    }, 2500);


    var dmlscript = document.createElement("script");
    dmlscript.src = "https://http2.mlstatic.com/storage/bmsdk/js/dml-0.0.7.min.js";
    dmlscript.onload = () => {
        new DMLSDK({
            publicKey: "APP_USR-5d0d45b9-57f5-469d-912f-04f7528ea5d3",
            out: "vtex.deviceFingerprint"
        });
    }
    document.body.appendChild(dmlscript);

    // //console.log('entro' +vtexjs.checkout.orderFormId);
}


function autecoHashchange() {
    window.addEventListener("hashchange", function (event) {

        if (location.hash == '#/profile') {
            // console.log('entro');
            /* validacion campo nombre checkout */
            var nombre = document.getElementById("client-first-name").oninput = function () {
                var response = convert_accented_characters(this.value);
                this.value = response;
            };

            var apellido = document.getElementById("client-last-name").oninput = function () {
                var responseApellido = convert_accented_characters(this.value);
                this.value = responseApellido;
            };

            function convert_accented_characters(str) {
                var conversions = new Object();
                conversions['ae'] = 'ä|æ|ǽ';
                conversions['oe'] = 'ö|œ';
                conversions['ue'] = 'ü';
                conversions['Ae'] = 'Ä';
                conversions['Ue'] = 'Ü';
                conversions['Oe'] = 'Ö';
                conversions['A'] = 'À|Á|Â|Ã|Ä|Å|Ǻ|Ā|Ă|Ą|Ǎ';
                conversions['a'] = 'à|á|â|ã|å|ǻ|ā|ă|ą|ǎ|ª';
                conversions['C'] = 'Ç|Ć|Ĉ|Ċ|Č';
                conversions['c'] = 'ç|ć|ĉ|ċ|č';
                conversions['D'] = 'Ð|Ď|Đ';
                conversions['d'] = 'ð|ď|đ';
                conversions['E'] = 'È|É|Ê|Ë|Ē|Ĕ|Ė|Ę|Ě';
                conversions['e'] = 'è|é|ê|ë|ē|ĕ|ė|ę|ě';
                conversions['G'] = 'Ĝ|Ğ|Ġ|Ģ';
                conversions['g'] = 'ĝ|ğ|ġ|ģ';
                conversions['H'] = 'Ĥ|Ħ';
                conversions['h'] = 'ĥ|ħ';
                conversions['I'] = 'Ì|Í|Î|Ï|Ĩ|Ī|Ĭ|Ǐ|Į|İ';
                conversions['i'] = 'ì|í|î|ï|ĩ|ī|ĭ|ǐ|į|ı';
                conversions['J'] = 'Ĵ';
                conversions['j'] = 'ĵ';
                conversions['K'] = 'Ķ';
                conversions['k'] = 'ķ';
                conversions['L'] = 'Ĺ|Ļ|Ľ|Ŀ|Ł';
                conversions['l'] = 'ĺ|ļ|ľ|ŀ|ł';
                conversions['N'] = 'Ñ|Ń|Ņ|Ň';
                conversions['n'] = 'ñ|ń|ņ|ň|ŉ';
                conversions['O'] = 'Ò|Ó|Ô|Õ|Ō|Ŏ|Ǒ|Ő|Ơ|Ø|Ǿ';
                conversions['o'] = 'ò|ó|ô|õ|ō|ŏ|ǒ|ő|ơ|ø|ǿ|º';
                conversions['R'] = 'Ŕ|Ŗ|Ř';
                conversions['r'] = 'ŕ|ŗ|ř';
                conversions['S'] = 'Ś|Ŝ|Ş|Š';
                conversions['s'] = 'ś|ŝ|ş|š|ſ';
                conversions['T'] = 'Ţ|Ť|Ŧ';
                conversions['t'] = 'ţ|ť|ŧ';
                conversions['U'] = 'Ù|Ú|Û|Ũ|Ū|Ŭ|Ů|Ű|Ų|Ư|Ǔ|Ǖ|Ǘ|Ǚ|Ǜ';
                conversions['u'] = 'ù|ú|û|ũ|ū|ŭ|ů|ű|ų|ư|ǔ|ǖ|ǘ|ǚ|ǜ';
                conversions['Y'] = 'Ý|Ÿ|Ŷ';
                conversions['y'] = 'ý|ÿ|ŷ';
                conversions['W'] = 'Ŵ';
                conversions['w'] = 'ŵ';
                conversions['Z'] = 'Ź|Ż|Ž';
                conversions['z'] = 'ź|ż|ž';
                conversions['AE'] = 'Æ|Ǽ';
                conversions['ss'] = 'ß';
                conversions['IJ'] = 'Ĳ';
                conversions['ij'] = 'ĳ';
                conversions['OE'] = 'Œ';
                conversions['f'] = 'ƒ';
                for (var i in conversions) {
                    var re = new RegExp(conversions[i], "g");
                    str = str.replace(re, i);
                }
                return str;
            }

            // Adicionando select para tipo de documento
            var divSelect = $('#client-profile-data .box-client-info-pf .document-box .other-document .client-document-type')
            var createSelect = (
                '<select id="documentType">' +
                '<option value="CEDULACOL">Cédula de Ciudadanía</option>' +
                '<option value="CEDULAEXT">Cédula de Extranjería</option>' +
                '<option value="PASAPORTE">Pasaporte</option>' +
                '</select>'
            )
            if (!$('#documentType').length) {
                divSelect.append(createSelect)
            }

            // Salvando tipo de documento no master data
            var documentType = $('#client-profile-data .box-client-info-pf .document-box .other-document #documentType')
            var inputDocumentType = $('#client-profile-data .box-client-info-pf .document-box .other-document .client-document-type input')
            documentType.on('change', function () {
                inputDocumentType.val(documentType.val().toUpperCase())
            })
        }

        if (location.hash == '#/payment') {
            var total = document.querySelectorAll('[data-bind="text: totalLabel"]')[0].innerHTML;
            var totalformat = total.replace(/[.,\s]/g, '');
            var nocurrency = totalformat.replace('$', '');

            setTimeout(function () {
                if (nocurrency >= 3000000 || nocurrency <= 20000) {
                    document.querySelectorAll(".bank-invoice-item-efecty")[0].style.display = "none";

                    document.querySelectorAll(".bank-invoice-item-davivienda")[0] ? document.querySelectorAll(".bank-invoice-item-davivienda")[0].style.display = "none" : null
                    // document.querySelectorAll(".bank-invoice-item-davivienda")[0].style.display = "none";

                    //document.querySelectorAll(".pg-efecty")[0].style.display = "none";
                    //document.querySelectorAll(".box-payment-group2")[0].style.display = "none";  
                    if (nocurrency >= 3000000) {
                        document.querySelectorAll(".bank-invoice-item-baloto")[0].style.display = "none";
                        document.querySelectorAll(".pg-baloto, .pg-efecty ")[0].style.display = "none";
                        document.getElementById("payment-group-creditCardPaymentGroup").click();
                    }
                }

                if (nocurrency >= 1000000) {
                    //document.querySelectorAll(".box-payment-group2")[0].style.display = "block";
                    document.querySelectorAll(".bank-invoice-item-baloto")[0].style.display = "none";
                }
            }, 1300);

        }

        if (location.hash == '#/cart') {
            var total = document.querySelectorAll('[data-bind="text: totalLabel"]')[0].innerHTML;
            var totalformat = total.replace(/[.,\s]/g, '');
            var nocurrency = totalformat.replace('$', '');
            // alert(nocurrency);
            if (nocurrency <= 40000) {
                alert("El valor de la compra debe ser mayor o igual a $40.000");
                window.location.href = "https://www.autecomobility.com";
            }
        }
    });
}



function addCompanyData() {

    let divContent = document.createElement("div");

    divContent.className = "container-links-unstyled"

    let addA = document.createElement("a"),
        addSecondA = document.createElement("a");

    addA.innerHTML = "Agregar datos de empresa";

    addA.className = "btn-links-unstyled"

    addSecondA.innerHTML = "No incluir"

    addSecondA.className = "btn-links-unstyled-second"



    divContent.appendChild(addA)
    divContent.appendChild(addSecondA)

    let content = document.querySelector('.box-client-info-pf')

    let addCompanExist = document.querySelector('.btn-links-unstyled')



    if (addCompanExist === null) {

        content && content.append(divContent);

    }

    addA.addEventListener('click', () => {

        if (document.querySelector('.corporate-info-box').classList.contains('show-hide')) {
            // // console.log('contiene')
            document.querySelector('.btn-links-unstyled-second').classList.remove('show-hide')
            document.querySelector('.corporate-info-box').classList.remove('show-hide')

            addA.style.opacity = '1'
            addA.style.pointerEvents = 'inherit'



        } else {
            // console.log('no contiene')
            document.querySelector('.btn-links-unstyled-second').classList.add('show-hide')
            document.querySelector('.corporate-info-box').classList.add('show-hide')
            // addA.style.opacity = '0.4'
            // addA.style.pointerEvents = 'none'
            addA.className += " activeBtn"

        }

    })

    addSecondA.addEventListener('click', () => {

        if (document.querySelector('.btn-links-unstyled-second').classList.contains('show-hide')) {

            document.querySelector('.btn-links-unstyled-second').classList.remove('show-hide')
            document.querySelector('.corporate-info-box').classList.remove('show-hide')
            // addA.style.opacity = '1'
            // addA.style.pointerEvents = 'inherit'
            addA.classList.remove("activeBtn")


        } else {

            document.querySelector('.btn-links-unstyled-second').classList.add('show-hide')
            document.querySelector('.corporate-info-box').classList.add('show-hide')
            // addA.style.opacity = '0.4'
            // addA.style.pointerEvents = 'none'
            addA.className += " activeBtn"


        }
    })

    // console.log('contenido add', content)

    // console.log('is null ', addCompanExist)

}

const newHtml = () => {
    const html = `
    <label class="insert-checkbox-label" id="id-checkbox-label">
    <input type="checkbox" id="opt-insert-checkbox-label" checked>
    <a id="new-label"></a>
    <span class="insert-newsletter-text">Autorizo a Auteco Mobility S.A.S para que mis datos personales sean tratados conforme al manual interno de políticas y procedimientos de datos personales que se encuentra publicada en la página web www.autecomobility.com. <a href="www.autecomobility.com">Disponible Aquí</a></span>
    </label>
  `
    return html
}

const addCheckContent = () => {

    if (location.hash == '#/profile') {

        // console.log("ingreso al")
        const content = document.querySelectorAll('.newsletter'),
            checkId = document.getElementById('id-checkbox-label');

        if (content.length == 1 && checkId == null) {
            content[0].insertAdjacentHTML('beforeend', newHtml())
        }


    }

}

function checkContent() {

    if (location.hash == '#/profile') {

        setTimeout(() => {

            const buttom = document.getElementById('go-to-shipping'),
                check = document.getElementById('opt-insert-checkbox-label');

            if (check.checked == true) {
                buttom.style.opacity = '1'
                buttom.style.pointerEvents = 'inherit'
                if (buttom.nextElementSibling) {
                    buttom.nextElementSibling.remove()
                }
            } else {
                buttom.style.opacity = '0.4'
                buttom.style.pointerEvents = 'none'
            }

            check.addEventListener('change', (e) => {
                if (e.target.checked) {
                    buttom.style.opacity = '1'
                    buttom.style.pointerEvents = 'inherit'
                    if (buttom.nextElementSibling) {
                        buttom.nextElementSibling.remove()
                    }
                } else {
                    buttom.style.opacity = '0.4'
                    buttom.style.pointerEvents = 'none'
                }
            })
        }, 500);
    }




}



function checkouProfile() {
    autecoFunction()
    autecoHashchange()
    addCompanyData()
    addCheckContent()
    checkContent()
}
export default checkouProfile;