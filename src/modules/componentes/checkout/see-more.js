

const newHtml = () => {
    const html = `
    <p class="seeMore" id="seeMoreCartContent">
    El costo de los trámites de tránsito no están incluido en el precio pagado en el 
    sitio web y deberá asumirlos el cliente de <span>manera adicional, con posibilidad de 
    tramitarlo a través del punto de venta habilitado.</span> <a id="seeMoreCart">Leer más</a>
    </p>
    <p id = "seeMoreCartHide" class="seeMoreCart">
    El costo de los trámites de tránsito no están incluido en el precio pagado en el sitio web y deberá asumirlos el cliente de manera adicional, con posibilidad de tramitarlo a través del punto de venta habilitado. <a id="seeMinusCart"> Leer menos</a>
    </p>
  `
    return html

}




const seeMoreCart = () => {

    if (location.hash == '#/cart') {

        setTimeout(() => {

            const content = document.querySelectorAll('.cart-template.full-cart.span12.active .summary-template-holder'),
                seeMoreCartContent = document.getElementById('seeMoreCartContent')


            if (seeMoreCartContent == null) {
                content[0] ? content[0].insertAdjacentHTML('beforeend', newHtml()) : null

                const seeMoreCart = document.getElementById('seeMoreCart');

                setTimeout(() => {
                    const seeMoreCartContentAdd = document.getElementById('seeMoreCartContent')
                    seeMoreCart.addEventListener("click", () => {

                        if (seeMoreCartContentAdd.classList.contains('seeMoreCarBlock')) {
                            seeMoreCartContentAdd.classList.remove('seeMoreCarBlock')
                        } else {
                            seeMoreCartContentAdd.classList.add('seeMoreCarBlock')

                        }
                    })

                }, 1000);

            }
        }, 500);


    }


}


const seeMore = () => {
    seeMoreCart()
}

export default seeMore;