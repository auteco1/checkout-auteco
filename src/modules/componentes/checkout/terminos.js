// // import mediaQ from '../utils/mediaqueries';
const htmlTerminos = () => {
  //console.log('Setting terms');
  return `
      <div class="accordion-terminos">
          <p>
              <input type="checkbox" id="terminosch" value="terminoschx" required="required">   
              <span>Acepto <a href="/preguntas-frecuentes?terminos" target="_blank">términos y condiciones</a>
              y autorizo el tratamiento de mis datos personales conforme a la 
              <a href="/preguntas-frecuentes?tratamiento" target="_blank">política de privacidad y tratamiento de datos personales.</a>
              </span>

          </p>
      </div>
  `;
}

const eventoTerminos = () => {

  let check = document.getElementById('terminosch'),
    buttonGo = document.getElementById('cart-to-orderform')
    //console.log('Dia sin ivaaaaaaaaa', check.checked);
  
  if (check.checked == true) {
    buttonGo.style.opacity = '1'
    buttonGo.style.pointerEvents = 'inherit'
    if(buttonGo.nextElementSibling) {
      buttonGo.nextElementSibling.remove()
    }
  } else {
    buttonGo.style.opacity = '0.4'
    buttonGo.style.pointerEvents = 'none'
  }
  check.addEventListener('change', () => {
    if (event.target.checked) {
      buttonGo.style.opacity = '1'
      buttonGo.style.pointerEvents = 'inherit'
      if(buttonGo.nextElementSibling) {
        buttonGo.nextElementSibling.remove()
      }
    } else {
      buttonGo.style.opacity = '0.4'
      buttonGo.style.pointerEvents = 'none'
    }
  })
}

let terminosCheckout = () => {
  // console.log('terminooooos');
  
  document.querySelector('.summary-template-holder').insertAdjacentHTML('beforeend', htmlTerminos())
  eventoTerminos()
}
export default terminosCheckout;