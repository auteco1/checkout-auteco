import newDirection from "./newDirection"

const newHtmlTerminos = () => {
    const html = `
    <div id="acuerdo-producto">
        <p class="primer-parrafo">Revisa estas recomendaciones de acuerdo al producto que estés comprando: <b>VEHÍCULOS:</b> recógelo en uno de nuestros puntos de venta seleccionando la opción de <b>RECOGER EN TIENDA</b> y elige el punto de venta que prefieras. Si no te aparecen selecciona otra ciudad o ubicación.</p>
        <p class="segundo-parrafo"><b>ACCESORIOS, REPUESTOS Y BICICLETAS:</b> selecciona de ENVÍO A DOMICILIO e ingresa la dirección donde quieres recibir tu compra.</p>
    </div>
    `
    return html
}

const addTerminos = () => {
    if (location.hash == "#/shipping") {
        let busqueda = document.querySelectorAll('.accordion-inner.shipping-container .box-step #postalCode-finished-loading .vtex-omnishipping-1-x-deliveryChannelsWrapper'),
            validarTerminos = document.querySelectorAll('.accordion-inner.shipping-container .box-step #acuerdo-producto')


        // //console.log("validarTerminos", busqueda[0])


        if (validarTerminos.length == 0 && busqueda[0] != undefined) {
            busqueda[0] && busqueda[0].parentElement.insertAdjacentHTML('beforeend', newHtmlTerminos());
        }
    } else {
        let btnMap = document.querySelector('.open_map')
        if (!btnMap) return false
        btnMap.remove()
    }
    //funciones Google maps api
    closeMap()

    setTimeout(() => {
        newDirection()
    }, 1500)
}

const ocultarTerminos = () => {

    if (location.hash == "#/shipping") {

        const show = document.getElementById('shipping-option-pickup-in-point'),
            hide = document.getElementById('shipping-option-delivery'),
            component = document.getElementById('acuerdo-producto')



        show && show.addEventListener('click', () => {
            // //console.log("mostrar", component)
            component.style.display = 'block'
        })

        hide && hide.addEventListener('click', () => {
            // //console.log("ocultar", component)

            component.style.display = 'none'

        })

    }



}

const shipping = () => {
    newDirection()
    addTerminos()
    ocultarTerminos()
}

$(window).on("orderFormUpdated.vtex", function (t, e) {
    setTimeout(() => {  
    let forceChangeAddress = document.querySelector('#force-shipping-fields')
        if(forceChangeAddress){
            forceChangeAddress.addEventListener('click',()=>{
                setTimeout(() => {  
                    //console.log("click force change address")
                    newDirection()    
                }, 1000);
            })
        }
    }, 1000);
    setTimeout(() => {
        const postalCode = document.getElementById('shipping-option-delivery');
        if (postalCode) {
            postalCode.addEventListener("click", () => {
                setTimeout(() => {  
                    newDirection()    
                }, 1000);
            })
            
        }
    }, 1000);
})
export default shipping