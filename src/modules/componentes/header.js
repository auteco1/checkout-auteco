
const arraProductsComplete ={}
const getProductsMenu=async()=>{
  const arrayProducts={
    Transporte:await getCollection(853,12),
    Deportivas: await getCollection(854,12),
    Enduro: await getCollection(855,12),
    Automaticas: await getCollection(856,12),
    Semiatomaticas:await getCollection(859,12),
    Gama:await getCollection(858,12),
    Electricas:await getCollection(859,12),
    Electricas2:await getCollection(859,12),
  }
  console.log('arrayProducts: ' ,arrayProducts)
  arraProductsComplete.Transporte =Object.keys(arrayProducts.Transporte).map(product=>arrayProducts.Transporte[product].productID)
  arraProductsComplete.Deportivas= Object.keys(arrayProducts.Deportivas).map(product=>arrayProducts.Deportivas[product].productID)
  arraProductsComplete.Enduro =Object.keys(arrayProducts.Enduro).map(product=>arrayProducts.Enduro[product].productID)
  arraProductsComplete.Automaticas =Object.keys(arrayProducts.Automaticas).map(product=>arrayProducts.Automaticas[product].productID)
  arraProductsComplete.Semiatomaticas =Object.keys(arrayProducts.Semiatomaticas).map(product=>arrayProducts.Semiatomaticas[product].productID)
  arraProductsComplete.Gama =Object.keys(arrayProducts.Gama).map(product=>arrayProducts.Gama[product].productID)
  arraProductsComplete.Electricas =Object.keys(arrayProducts.Electricas).map(product=>arrayProducts.Electricas[product].productID)
  arraProductsComplete.Electricas2 = Object.keys(arrayProducts.Electricas2).map(product=>arrayProducts.Electricas2[product].productID)
  
  console.log('arraProductsComplete: ' ,arraProductsComplete)
  printContent(arraProductsComplete)
}

const arrayProducts = {
  // Deportivas: await getCollection(854.14),
  // Deportivas: [8913, 8914, 8915, 8910, 3008, 2887, 7326, 7418, 7419, 8934, 11112, 12341],
  // Enduro: [12459, 12253, 12252, 3342, 3341, 60],
  // Scooter: [12606, 12445, 12259, 12258, 11135, 11134, 7893, 7807, 7323, 2610, 2607, 104],
Transporte: [12608, 12604, 12476, 12462, 12444, 12443, 12342, 12341, 12256, 11112, 10244, 8915],
  // Electricas: [12470, 12469, 12266, 11177, 7810, 7337, 2671, 2417, 2416, 116, 115, 111],
  Electricas2: [12470, 12469, 12266, 11177, 7810, 7337, 2671, 2417, 2416, 116, 115, 111],
  Semiatomaticas: [12262, 11729, 3717, 2600, 108],
  Gama: [12628, 12441, 8934, 8913, 8912, 8911, 8909, 8908, 8907, 8906, 12634, 12635],
  Cascos: [11244, 12251, 7481, 11267, 8475],

  // Automaticas: [12606, 101, 11135, 7893, 102, 2607, 12258, 12445, 104, 2610, 7323, 7807],

  // victory: [7340, 982, 7666, 7667, 3715, 8934, 8909, 3008, 8913, 12441, 8907, 7344],
  // benelli: [7340, 982, 7666, 7667, 3715, 8934, 8909, 3008, 8913, 12441, 8907, 7344],
  // kawasaki: [7340, 982, 7666, 7667, 3715, 8934, 8909, 3008, 8913, 12441, 8907, 7344],
  // kymco: [7340, 982, 7666, 7667, 3715, 8934, 8909, 3008, 8913, 12441, 8907, 7344],
  starker: [12470, 12469, 12266, 11177, 7810, 7337, 2671, 2417, 2416, 116, 115, 111],
  supper: [12470, 12469, 12266, 11177, 7810, 7337, 2671, 2417, 2416, 116, 115, 111],
  Guantes: [12146, 12145, 12144, 12143, 12138],
  Chaquetas: [12303, 7804, 7801, 12623, 12134],
  Impermeables: [7487, 7484, 7288, 7286, 7284],
  vans: [7816, 11171],
  camiones: [3746, 9712, 11172],
  pickUp: [12449],
  motocarros: [3253, 3254, 3258, 3256, 3257, 7763, 7817, 7818, 7819],
  motocarrosPasajeros: [3256, 3253],
  motocarroscarga: [3254, 3258, 3257, 7763, 7817, 7818, 7819],
  patinetasElectricas: [2661, 7834, 12741, 12742, 12743, 12745, 12746],
  // faltan estos
  biciElectricas: [94, 95, 3845, 3259, 3260, 3845, 11716, 11717, 11718, 12264, 12265, 12307, 12468],
  biciTodoTerreno: [12264, 11717],
  biciUrbanas: [12610],
  biciPlegables: [3260, 11718, 12468, 12471, 11716],
  wolfBicicleta: [12264, 12265, 12307, 12468],
  starkerBicicleta: [12610, 3260, 12264, 12471, 11717, 11716],
  dongfeng: [7816, 9712, 12449],
  stark: [3746],
  ceronte: [7763, 7817, 7818, 7819, 7763],
  jac: [11171, 11172],
  zhidou: [11110],
  zhidouAutos: [11110],
  dongfengAutos: [12449, 11110],
  ceronteMotocarro: [7763, 7817, 7818, 7819, 7763, 7763],
  piaggioMotocarro: [3253, 3254, 3258, 3256, 3257]
}
const getCollection=async(collectionID,productQuantity)=>{
  try {
    const response = await fetch(`https://mas.autecomobility.com/wp-json/auteco/v2/get_products/${collectionID}/${productQuantity}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'cache-control': 'no-cache'
      }
    })
    const json = await response.json()
    // if (id == "12606") {
    //     console.log(json[0].productClusters)
    
    // }
    // console.log('ARRAY MENU: ',json)
    return json
  } catch (error) {
    // console.log('error en getProduct', error);
  }
}
const printContent = async () => {
  positionBannersItems()
  const btnsBack = document.querySelectorAll('.menu-mobile.back')
  btnsBack.forEach(btn => {
    btn.nextElementSibling.classList.add('desktop')
  })
  // console.log('arraProductsComplete: ' ,arraProductsComplete)
  Object.keys(arraProductsComplete).forEach(category => {
    // console.log('categoria:',category
    arraProductsComplete[category].map(id => {
      let liId = document.createElement('li') 
      liId.classList.add('menu-item-list-level-3')
      liId.classList.add('disable')
      liId.setAttribute('id','product'+id)
      let containerProduct =  document.querySelector('li.productos[data-name='+category+'] .container-menu-level-3-content')
      if(containerProduct){
        containerProduct.appendChild(liId)
      }
      
    })
  })
  Object.keys(arraProductsComplete).forEach(category => {
    // console.log('categoria:',category);
    arraProductsComplete[category].map(async (product) => {      
      const dataProduct = await getProduct(product)
      // console.log('DATA PRODUCT: ', dataProduct, product)
      if (dataProduct.length > 0) {

        mapDivsPrint(dataProduct, category,product)
      }
    })
  })
}
const getProduct = async (id) => {
  try {
    const response = await fetch(`/api/catalog_system/pub/products/search/?fq=productId:${id}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'cache-control': 'no-cache'
      }
    })
    const json = await response.json()
    // if (id == "12606") {
    //     console.log(json[0].productClusters)
    //     console.log(json)
    // }
    return json
  } catch (error) {
    // console.log('error en getProduct', error);
  }
}
const positionBannersItems = () => {
  setTimeout(() => {
    // let banners= document.querySelectorAll('header .new-header-v1 .new-header-display-scroll .new-header-menu-wrapper .new-header-menu .new-container-header-menu .menu-item-list-level-1 li .container-menu-level-3-content.inver-content .menu-item-list-level-3:nth-child(1)')
    // banners.forEach(banner => {
    //         let container = banner.parentElement
    //         container.append(banner)
    // });
  }, 10000)
}

const fadeInOutTag = (element) => {
  element.fadeIn(1000, function () {
    element.fadeOut(1500, function () {
      element.fadeIn(1500, function () {
        setTimeout(fadeInOut, 500)
      })
    })
  })
}

const mapDivsPrint = (product, category,id) => {
  // let productClusters = product[0].productClusters
  // console.log('productClusters :', productClusters);
  const divsContainerPrint = document.querySelectorAll('.menu-item-types.productos')
  // if(!divsContainerPrint.length<1) return false
  divsContainerPrint.forEach(divContainer => {
    if (divContainer.getAttribute('data-name') == null || divContainer.getAttribute('data-name') === undefined) return false
    // if (product[0].productId === 12606) {
    //   // console.log('divContainer: ', product);
    // }
    let productContainer= divContainer.querySelector('#product'+id)
    if(productContainer){
      if (divContainer.getAttribute('data-name') === category && productContainer.children.length<1) {
        const brandVar = product[0].brand.replaceAll(' ', '-').toLowerCase()
        if (!divContainer.querySelector('.container-menu-level-3-content')) return false
        divContainer.querySelector('#product'+id).classList.add(brandVar)
        divContainer.querySelector('#product'+id).classList.remove('disable')
        divContainer.querySelector('#product'+id).innerHTML +=
                  `<section class="container-product-level-3">
                      <span class="product-level-3-flag-brand"></span>
                      <a class="product-level-3-link" href="${product[0].link}" title="${product[0].productReference.includes('CHAQUETA') || product[0].productReference.includes('GUANTE') || product[0].productReference.includes('CASCO') || product[0].productReference.includes('IMP-AP') ? product[0].productName : product[0].productReference}" itemprop="url">
                          ${Object.keys(product[0].productClusters).map(promo =>
                      product[0].productClusters[promo] === 'Nueva' ? '<span class="product-level-3-flag animate-flicker">Nueva</span>' : ''// console.log('PROMO: ',product[0].productClusters)
                  ).join('')}
                  ${Object.keys(product[0].productClusters).map(promo =>
                      product[0].productClusters[promo] === 'Diesel' ? '<span class="product-level-3-flag animate-flicker green">Diesel</span>' : ''// console.log('PROMO: ',product[0].productClusters)
                  ).join('')}
                  ${Object.keys(product[0].productClusters).map(promo =>
                      product[0].productClusters[promo] === 'Gasolina' ? '<span class="product-level-3-flag animate-flicker orange">Gasolina</span>' : ''// console.log('PROMO: ',product[0].productClusters)
                  ).join('')}
                  ${Object.keys(product[0].productClusters).map(promo =>
                      product[0].productClusters[promo] === 'Eléctrico' ? '<span class="product-level-3-flag animate-flicker blue">Eléctrico</span>' : ''// console.log('PROMO: ',product[0].productClusters)
                  ).join('')}
                              <div class="product-level-3-image" style="margin-top:10px">
                                  <img src="${'https://auteco.vteximg.com.br/arquivos/ids/' + product[0].items[0].images[0].imageId + '-121-121.webp'}" width="121" height="121" loading="lazy"/>
                              </div>
                          <div class="product-level-3-name">
                              <span>${product[0].productReference.includes('CHAQUETA') || product[0].productReference.includes('GUANTE') || product[0].productReference.includes('CASCO') || product[0].productReference.includes('IMP-AP') ? product[0].productName : product[0].productReference.replace(/VICTORY|BENELLI|KYMCO|VELOCIFERO|KAWASAKI|STARKER|WOLF|PIAGGIO|SUPER SOCO|CERONTE|DONGFENG|JAC|ZHIDOU|STARK/g, '')}</span>
                          </div>
                          <div class="product-level-3-price">
                              ${product[0].items[0].sellers[0].commertialOffer.ListPrice === product[0].items[0].sellers[0].commertialOffer.PriceWithoutDiscount ? `<span class="price-affter">$${new Intl.NumberFormat().format(product[0].items[0].sellers[0].commertialOffer.ListPrice).replace(/,/g, '.')}</span>` : `<span class="price-before">$${new Intl.NumberFormat().format(product[0].items[0].sellers[0].commertialOffer.ListPrice).replace(/,/g, '.')}</span> <span class="price-affter">$${new Intl.NumberFormat().format(product[0].items[0].sellers[0].commertialOffer.PriceWithoutDiscount).replace(/,/g, '.')}</span>`}                            
                          </div>
                      </a>
                  </section>`
        // divContainer.querySelector('.container-menu-level-3-title span').textContent =category 
        // agregar conbustible, productReference(chaquetas guantes etc), dos precios
      }
    }
    
  })
}
const openMenuMobile = () => {
  if (screen.width < 1000) {
    const body = document.body
    const btnOpen = document.querySelector('.open-menu-mobile')
    const btnClose = document.querySelector('.close-menu-mobile')
    const brands = document.querySelectorAll('.menu-item-brands a')
    brands.forEach(brand => {
      brand.classList.add('redirect')
    })
    const categories = document.querySelectorAll('.btn-text')
    categories.forEach(category => {
      category.classList.add('redirect')
    })
    btnClose.addEventListener('click', (e) => {
      e.preventDefault()
      document.querySelector('.new-header-menu-wrapper').style.left = '-900px'
      btnOpen.classList.remove('active')
      btnClose.style.display = 'none'
      btnOpen.style.display = 'block'
      document.querySelector('.shadow-cart').classList.remove('active')
      body.classList.remove('open-minicart')
    })
    btnOpen.addEventListener('click', (e) => {
      e.preventDefault()
      document.querySelector('.new-header-menu-wrapper').style.left = '0px'
      btnOpen.classList.add('active')
      btnOpen.style.display = 'none'
      btnClose.style.display = 'block'
      document.querySelector('.shadow-cart').classList.add('active')
      body.classList.add('open-minicart')
    })
    openSubmenus()
  }
}

const placeHolderSearch = () => {
  const searchs = document.querySelectorAll('.busca input')
  const messageSearch = searchs[1].value
  searchs[1].addEventListener('focusin', () => {
    searchs[1].value = ''
  })
  searchs[1].addEventListener('focusout', () => {
    searchs[1].value = messageSearch
  })
}

const resizeSubMenu = () => {
  if (screen.width > 1000) {
    const itemsHover = document.querySelectorAll('li.menu-item-list.menu-item-list-level-1:not(.menu-mobile,.menu-patinetas-electricas)')
    itemsHover.forEach(item => {
      item.addEventListener('mouseenter', () => {
        // console.log('ENTRO', item)
        const submenu1 = item.querySelector('ul.menu-item-list-column-content.brands').offsetHeight
        const subMenu2 = item.querySelector('.menu-item-list-column-content').offsetHeight
        const contentSubMenu = item.querySelector('li.menu-item-list-level-2.menu-item-list-column-wrapper')
        if (subMenu2 + submenu1 > 558) {
          item.querySelector('ul.new-container-header-menu.new-container-header-menu-level-3.new-container-dropdown-menu').style.height = submenu1 + subMenu2 + 'px'
        }
        // contentSubMenu.style.height = submenu1+subMenu2+'px'
      })
    })
  }
}
const headerFunctions = async () => {
  getProductsMenu()
  openMenuMobile()
  hiddernMenuFixedPDP()
  positionFixedBtns()
  sizeContainerProducts()
  placeHolderSearch()
  resizeSubMenu()
  // perspectiveCarrousel();
  // postSlider();
}
const openSubmenus = () => {
  const btnsLevel1 = document.querySelectorAll('.menu-item-link-level-1')
  btnsLevel1.forEach(btn => {
    const level2 = btn.parentElement.querySelector('.new-container-header-menu-level-2')
    if (level2) {
      const itemsLevel2 = level2.querySelectorAll('.menu-item-types a')
      const backLevel2 = level2.querySelectorAll('.btn-back')[0]
      let level3, backLevel3
      btn.addEventListener('click', (e) => {
        e.preventDefault()
        const containersLevel2 = document.querySelectorAll('.new-container-header-menu-level-2')
        containersLevel2.forEach(container => {
          container.style.opacity = '0'
          container.style.visibility = 'hidden'
          container.style.height = '0px'
        })
        document.querySelector('.new-container-header-menu-level-1').style.overflowY = 'inherit'
        document.querySelector('.new-container-header-menu-level-1').style.visibility = 'hidden'
        level2.style.overflowY = 'auto'
        level2.style.opacity = '1'
        level2.style.visibility = 'visible'
        level2.style.height = 'auto'
        level2.style.maxHeight = '100%'
        backLevel2.addEventListener('click', (e) => {
          e.preventDefault()
          level2.style.opacity = '0'
          level2.style.visibility = 'hidden'
          // level2.style.overflow = "inherit"
          level2.style.height = '0px'
          document.querySelector('.new-container-header-menu-level-1').style.overflowY = 'auto'
          document.querySelector('.new-container-header-menu-level-1').style.height = '100%'
          document.querySelector('.new-container-header-menu-level-1').style.visibility = 'visible'
          document.querySelector('.new-container-header-menu-level-1').style.maxHeight = '80vh'
          container.style.height = '100%'
        })
      })
      itemsLevel2.forEach(item => {
        item.addEventListener('click', (e) => {
          if (!item.classList.contains('redirect')) {
            e.preventDefault()
            backLevel3 = item.parentElement.querySelector('.btn-back')
            level3 = item.parentElement.querySelector('.new-container-header-menu-level-3')
            // console.log('backLevel3: ',backLevel3);
            const containersLevel3 = document.querySelectorAll('.new-container-header-menu-level-3')
            containersLevel3.forEach(container => {
              container.style.opacity = '0'
              container.style.visibility = 'hidden'
              container.style.height = '0px'
            })
            level2.style.overflow = 'inherit'
            level2.style.visibility = 'hidden'
            level2.style.height = '100vh'
            level2.style.maxHeight = '100vh'
            level3.style.opacity = '1'
            level3.style.visibility = 'visible'
            level3.style.zIndex = '3'
            level3.style.padding = '0px'
            level3.style.maxHeight = '100vh'
            level3.style.overflowY = 'auto'
            level3.style.height = '82%'
            backLevel3.addEventListener('click', (e) => {
              e.preventDefault()
              level2.style.overflowY = 'auto'
              level2.style.maxHeight = '82vh'
              level2.style.visibility = 'visible'
              level3.style.opacity = '0'
              level3.style.visibility = 'hidden'
              level3.style.zIndex = '-1'
              // level3.style.overflow = "inherit"
              level3.style.height = '0px'
            })
          }
        })
      })
    }
  })
}

const hiddernMenuFixedPDP = () => {
  const isPdp = document.querySelector('body.product-brand')
  if (!isPdp) return false
  window.addEventListener('scroll', () => {
    const menuFixed = document.querySelector('.brand-menu-floating')
    const currentColor = window.getComputedStyle(menuFixed, null).position
    const menuPrincipal = document.querySelector('header')
    if (currentColor === 'fixed') {
      menuPrincipal.style.display = 'none'
    } else {
      menuPrincipal.style.display = 'block'
    }
  })
}
const positionFixedBtns = () => {
  const body = document.querySelector('body')
  //console.log('entroo en body');
  if (screen.width < 1000 && body.classList.contains('produto')) {
    document.querySelector('.fixedButtons').style.bottom = '155px'
  }
}

const sizeContainerProducts = () => {
  if (screen.width > 1000) {
    const btnsProducts = document.querySelectorAll('.menu-item-list-column-content .menu-item-types.productos')
    btnsProducts.forEach(btn => {
      btn.addEventListener('mouseenter', () => {
        const items = btn.querySelectorAll('.menu-item-list-level-3')
        // let container = btn.parentElement.parentElement.parentElement
        // let columnLeft = btn.parentElement.parentElement
        // let columnRight = btn.querySelector('.new-container-dropdown-menu')
        if (items.length < 5 && items.length > 0) {
          // container.style.width = "70%"
          // container.style.right="0px"
          // container.style.margin="auto"
          // columnLeft.style.width ="26%"
          // columnRight.style.paddingLeft="24%"
          // console.log('total items',items.length);
          items.forEach(item => {
            item.style.maxWidth = '25%'
          })
        } else {
          // container.style.width = "100%"
          // columnLeft.style.width ="19%"
          // columnRight.style.paddingLeft="19%"
          items.forEach(item => {
            item.style.maxWidth = '15%'
          })
        }
      })
    })
  }
}
export default headerFunctions

// llamado de combustibles antes
// ${product[0].COMBUSTIBLE ? Object.keys(product[0].COMBUSTIBLE).map(tipo =>
//     product[0].COMBUSTIBLE[tipo] == "Diesel" && product[0].productName.includes("MOTOCARRO") ? `<span class="product-level-3-flag green">Diesel</span>` : product[0].COMBUSTIBLE[tipo] == "Gasolina corriente" && product[0].productName.includes("MOTOCARRO") ? `<span class="product-level-3-flag orange">Gasolina</span>` : ''
// ).join('') : product[0].productName.includes("MOTOCARRO") ? `<span class="product-level-3-flag blue">Eléctrico</span>` : ''}
