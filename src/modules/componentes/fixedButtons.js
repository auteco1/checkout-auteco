const unfoldingsButtons = () => {
    let unfoldingButton = document.querySelector('.unfoldingButtons')
    let actionsButtons = document.querySelectorAll('.actionsButtons')
    let buttonOpen = document.querySelector('.buttonOpen')
    let buttonClose = document.querySelector('.buttonClose')
    if(!unfoldingButton) return false
    unfoldingButton.addEventListener('click', () => {

        buttonOpen.classList.toggle('buttonimg--close')
        buttonClose.classList.toggle('buttonimg--open')

        actionsButtons.forEach(actionsB => {
            actionsB.classList.toggle('open--buttons')
        });
        actionsButtons[2].classList.toggle('open--buttons--call')
        actionsButtons[1].classList.toggle('open--buttons--test')
        actionsButtons[0].classList.toggle('open--buttons--cotizacion')
    })


}

const fixedButtons = () => {
    unfoldingsButtons()
}

export default fixedButtons