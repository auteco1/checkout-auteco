

const desplegableFooter = () => {

    let botonesDespl = document.querySelectorAll('.cont--img__footer')
    let botonDesplOpen = document.querySelectorAll('.cont__links--footer')

    botonesDespl.forEach(btn__footer => {
        btn__footer.addEventListener('click', () => {
            let indexBtn = btn__footer.getAttribute('data-index');
            botonDesplOpen[indexBtn - 1].classList.toggle('cont--links--active')
            btn__footer.classList.toggle('btn--active')
        })
    });
}

const footer = () => {
    desplegableFooter()
    submitForm()
}
const  submitForm=async()=> {
    // /* console.log('el formu',this.formId) */
    // habeasData()
    let formId = document.querySelector('.contNewsletter__form form')
    if(!formId) return false
    formId.addEventListener('submit',async (e)=>{
        e.preventDefault()
        let data = {
            firstName: formId.querySelector('.nombre--form').value,
            email: document.querySelector('.email--form').value,
            isNewsletterOptIn: document.querySelector('#checkAceptacion').checked
         }
         //console.log('DATA',data);
        let send = await senAjax(data)
        if(send.Id =! null){
            // let senWo = this.sendWoowUp(e)
            /* console.log(senWo) */
            formId.innerHTML = `<h4>¡Gracias por registrarte! </h4>`
        }
        
      
    })
}
const senAjax = async (data)=>{
    const responset = await fetch(`/api/dataentities/CL/documents`, {
        method: 'POST',
        headers: {
        "Content-Type": "application/json",
        "cache-control": "no-cache",
        },
        body: JSON.stringify(data)
    })
    const res = await responset.json();
    return res

}

export default footer




