import { tns } from "../../../../node_modules/tiny-slider/src/tiny-slider"

const openCity = (evt, cityName) => {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}

const tabsListen = () => {
    let tabs = document.querySelectorAll('.tablinks')
    tabs.forEach(tab => {
        tab.addEventListener('click', (e) => {
            let idBrand = tab.getAttribute('data-name')
            openCity(e, idBrand)
        })
    });
}
const slidersBrandsItems = () => {
    if (screen.width > 1000) {
        if (document.querySelector('.slider__brand__victory div h2')) document.querySelector('.slider__brand__victory div h2').style.display = "none"
        if (document.querySelector('.slider__brand__benelli div h2')) document.querySelector('.slider__brand__benelli div h2').style.display = "none"
        if (document.querySelector('.slider__brand__kawasaki div h2')) document.querySelector('.slider__brand__kawasaki div h2').style.display = "none"
        if (document.querySelector('.slider__brand__kymco div h2')) document.querySelector('.slider__brand__kymco div h2').style.display = "none"
        if (document.querySelector('.slider__brand__starker div h2')) document.querySelector('.slider__brand__starker div h2').style.display = "none"
        if (document.querySelector('.slider__brand__supersoco div h2')) document.querySelector('.slider__brand__supersoco div h2').style.display = "none"
        if (document.querySelector('.slider__brand__wolf div h2')) document.querySelector('.slider__brand__wolf div h2').style.display = "none"
        if (document.querySelector('.slider__brand__segway div h2')) document.querySelector('.slider__brand__segway div h2').style.display = "none"
        if (document.querySelector('.slider__brand__velocifero div h2')) document.querySelector('.slider__brand__velocifero div h2').style.display = "none"
        if (document.querySelector('.slider__brand__victory div')) {
            var slider = tns({
                container: document.querySelector('.slider__brand__victory div'),
                items: 4,
                slideBy: 'page',
                autoplay: false,
                nav: true,
                controls: true
            });
        }

        if (document.querySelector('.slider__brand__starker div')) {
            var slider = tns({
                container: document.querySelector('.slider__brand__starker div'),
                items: 4.1,
                slideBy: 'page',
                autoplay: false,
                nav: true,
                controls: true
            });
        }

        if (document.querySelector('.slider__brand__kawasaki div')) {
            var slider = tns({
                container: document.querySelector('.slider__brand__kawasaki div'),
                items: 4,
                slideBy: 'page',
                autoplay: false, 
                nav: true,
                controls: true
            });
        }
        if (document.querySelector('.slider__brand__wolf div')) {
            var slider = tns({
                container: document.querySelector('.slider__brand__wolf div'),
                items: 4,
                slideBy: 'page',
                autoplay: false, 
                nav: true,
                controls: true
            });
        }
        if (document.querySelector('.slider__brand__kymco div')) {
            var slider = tns({
                container: document.querySelector('.slider__brand__kymco div'),
                items: 4.00,
                slideBy: 'page',
                autoplay: false,
                nav: true,
                controls: true
            });
        }
        if (document.querySelector('.slider__brand__benelli div')) {
            var slider = tns({
                container: document.querySelector('.slider__brand__benelli div'),
                items: 4,
                slideBy: 'page',
                autoplay: false,
                nav: true,
                controls: true
            });
        }

       

        if (document.querySelector('.slider__brand__supersoco div')) {
            var slider = tns({
                container: document.querySelector('.slider__brand__supersoco div'),
                items: 4,
                slideBy: 'page',
                autoplay: false, 
                nav: true,
                controls: true
            });
        }

        if (document.querySelector('.slider__brand__segway div')) {
            var slider = tns({
                container: document.querySelector('.slider__brand__segway div'),
                items: 4,
                slideBy: 'page',
                autoplay: false, 
                nav: true,
                controls: true
            });
        }

        if (document.querySelector('.slider__brand__velocifero div')) {
            var slider = tns({
                container: document.querySelector('.slider__brand__velocifero div'),
                items: 4,
                slideBy: 'page',
                autoplay: false,
                nav: true,
                controls: true
            });
        }  

    }else{
        if (document.querySelector('.slider__brand__victory div h2')) document.querySelector('.slider__brand__victory div h2').style.display = "none"
        if (document.querySelector('.slider__brand__benelli div h2')) document.querySelector('.slider__brand__benelli div h2').style.display = "none"
        if (document.querySelector('.slider__brand__kawasaki div h2')) document.querySelector('.slider__brand__kawasaki div h2').style.display = "none"
        if (document.querySelector('.slider__brand__kymco div h2')) document.querySelector('.slider__brand__kymco div h2').style.display = "none"
        if (document.querySelector('.slider__brand__starker div h2')) document.querySelector('.slider__brand__starker div h2').style.display = "none"
        if (document.querySelector('.slider__brand__supersoco div h2')) document.querySelector('.slider__brand__supersoco div h2').style.display = "none"
        if (document.querySelector('.slider__brand__segway div h2')) document.querySelector('.slider__brand__segway div h2').style.display = "none"
        if (document.querySelector('.slider__brand__wolf div h2')) document.querySelector('.slider__brand__wolf div h2').style.display = "none"
        if (document.querySelector('.slider__brand__velocifero div h2')) document.querySelector('.slider__brand__velocifero div h2').style.display = "none"
        if (document.querySelector('.slider__brand__victory div')) {
            var slider = tns({
                container: document.querySelector('.slider__brand__victory div'),
                items: 2.1,
                slideBy: 'page',
                autoplay: false,
                nav: true,
                controls: true
            });
        }
        if (document.querySelector('.slider__brand__kawasaki div')) {
            var slider = tns({
                container: document.querySelector('.slider__brand__kawasaki div'),
                items: 2.1,
                slideBy: 'page',
                autoplay: false,
                nav: true,
                controls: true
            });
        }
        if (document.querySelector('.slider__brand__velocifero div')) {
            var slider = tns({
                container: document.querySelector('.slider__brand__velocifero div'),
                items: 2.1,
                slideBy: 'page',
                autoplay: false,
                nav: true,
                controls: true
            });
        }      
        if (document.querySelector('.slider__brand__wolf div')) {
            var slider = tns({
                container: document.querySelector('.slider__brand__wolf div'),
                items: 2.1,
                slideBy: 'page',
                autoplay: false,
                nav: true,
                controls: true
            });
        }   
        if (document.querySelector('.slider__brand__kymco div')) {
            var slider = tns({
                container: document.querySelector('.slider__brand__kymco div'),
                items: 2.1,
                slideBy: 'page',
                autoplay: false,
                nav: true,
                controls: true
            });
        }
        if (document.querySelector('.slider__brand__benelli div')) {
            var slider = tns({
                container: document.querySelector('.slider__brand__benelli div'),
                items: 2.1,
                slideBy: 'page',
                autoplay: false,
                nav: true,
                controls: true
            });
        }
        if (document.querySelector('.slider__brand__starker div')) {
            var slider = tns({
                container: document.querySelector('.slider__brand__starker div'),
                items: 2.1,
                slideBy: 'page',
                autoplay: false,
                nav: true,
                controls: true
            });
        }

        if (document.querySelector('.slider__brand__supersoco div')) {
            var slider = tns({
                container: document.querySelector('.slider__brand__supersoco div'),
                items: 2.1,
                slideBy: 'page',
                autoplay: false,
                nav: true,
                controls: true
            });
        }

        if (document.querySelector('.slider__brand__segway div')) {
            var slider = tns({
                container: document.querySelector('.slider__brand__segway div'),
                items: 2.1,
                slideBy: 'page',
                autoplay: false,
                nav: true,
                controls: true
            });
        }
    }
}
const textContentBrand = () => {
    let brands = document.querySelectorAll('.item__brand p')
    brands.forEach(brand => {
        brand.textContent = brand.textContent.split('-')[1]
        brand.style.textTransform = 'uppercase';
    });

    let titleItems = document.querySelectorAll('.product-name')
    titleItems.forEach(title => {
        // console.log('titles: ', title);
        title.textContent = title.textContent.replace('MOTO', '')
        title.textContent = title.textContent.replace('ELÉCTRICA', '')
        title.textContent = title.textContent.replace('PATINETA', '')
        title.textContent = title.textContent.replace('VELOCIFERO', '')
        title.textContent = title.textContent.replace('BICICLETA', '')
        title.textContent = title.textContent.replace('VICTORY', '')
        title.textContent = title.textContent.replace('KAWASAKI', '')
        title.textContent = title.textContent.replace('STARKER', '')
        title.textContent = title.textContent.replace('STÄRKER', '')
        title.textContent = title.textContent.replace('SUPERSOCO', '')
        title.textContent = title.textContent.replace('SUPER', '')
        title.textContent = title.textContent.replace('SUPER SOCO', '')
        title.textContent = title.textContent.replace('SEGWAY', '')
        title.textContent = title.textContent.replace('VICTORBENELLI', '')
        title.textContent = title.textContent.replace('KYMCO', '')
        title.textContent = title.textContent.replace('RESERVA -', '')
        title.style.textTransform = 'uppercase';
    });
    let pricesProducts = document.querySelectorAll('.tabs__brands .price span')
    pricesProducts.forEach(priceProduct => {
    //   console.log('precio inicial :',priceProduct.textContent.split(',')[0])
      priceProduct.textContent=priceProduct.textContent.split(',')[0]
    })

}

const printPriceProductsOut=()=>{
    let pricesProducts = document.querySelectorAll('.item__showcase__outofstock')
      pricesProducts.forEach(async (priceProduct) => {
        // https://service.autecomobility.com/sold-out-product-price/get-by-id/2246
        let id =priceProduct.parentElement.getAttribute('id')
        // console.log('CONTENEDOR PRODUCTO: ',containerProduct);
        let infoProduct = await getPriceProductOut(id)
        let Name = infoProduct[0].productName;
        let link = infoProduct[0].link
        let price = new Intl.NumberFormat().format(infoProduct[0].items[0].sellers[0].commertialOffer.ListPrice).replace(/,/g, '.')
        let priceBest =new Intl.NumberFormat().format(infoProduct[0].items[0].sellers[0].commertialOffer.PriceWithoutDiscount).replace(/,/g, '.')
        let htmlPrintPrice 
        if(price!=priceBest){
            console.log('PRICE PRODUCT: ',infoProduct)
            htmlPrintPrice = `
            <span class="price">
                <a title="${Name}" href="${link}">
                    <span class="old-price">$${price}</span>
                    <span class="best-price">$${priceBest}</span>
                </a>
            </span>`
        }else{
            htmlPrintPrice = `
            <span class="price">
                <a title="${Name}" href="${link}">
                    <span class="best-price">$${price}</span>                    
                </a>
            </span>`
        }
        console.log('PRICE PRODUCT: ',infoProduct)
    
      if(priceProduct.parentElement.querySelector('.best-price')) return false
      priceProduct.insertAdjacentHTML('beforebegin', htmlPrintPrice);
      priceProduct.style.display="none"
      })
  }
  const getPriceProductOut = async (id)=>{
    try {
      const response = await fetch(`/api/catalog_system/pub/products/search/?fq=productId:${id}`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'cache-control': 'no-cache'
        }
      })
      const json = await response.json()
      // console.log(json)
      return json
    } catch (error) {
      console.log('error en getProduct', error);
    }
  }
  

const slidersBrands = () => {
    tabsListen()
    textContentBrand()
    slidersBrandsItems()
    printPriceProductsOut()
}
export default slidersBrands