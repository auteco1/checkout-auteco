import { tns } from "../../../../node_modules/tiny-slider/src/tiny-slider"

const sliderPrincipal=()=>{
    if(document.querySelector('.slider-principal')){
      var slider = tns({
        container: document.querySelector('.slider-principal'),
        items: 1,
        slideBy: 'page',
        autoplay: false,
        nav:true,
        controls:true
      });
    }
    scrollSectionBanners()
}
const scrollSectionBanners =()=>{
  let icon = document.querySelector('.slider-principal__icon')
  if(icon){
      icon.addEventListener('click',(e)=>{
      e.preventDefault()
      if(screen.width>1450){
        window.scroll({
          top: 900,
          left: 0,
          behavior: 'smooth'
        });
      }else{
        window.scroll({
          top: 700,
          left: 0,
          behavior: 'smooth'
        });
      }

      
    })
  }
  
}

export default sliderPrincipal