function hoverProduct () {
    itemSlide();
}
const itemSlide = () => {
    const items = [...document.querySelectorAll('.hoverImg')]
    items.forEach(ele => {
      ele.addEventListener('mouseenter', (e) => {
        e.stopPropagation();
        if (e.target.classList.contains('hoverImg-simple')) {
          let element = e.target,
            href = element.getAttribute('data-href'),
            id = element.getAttribute('id')
          element.classList.remove('hoverImg-simple')
          vtexjs.catalog.getProductWithVariations(id).done(function (product) {
  
            let getData = getSkuData(product.skus[0].sku)
             console.log(product)
            console.log('imagenes', href) 
            slideGenerate(getData.images, id, element, href)
          });
  
        }
  
      })
  
    })
  
}
const slideGenerate = (images, id, element, href) => {
console.log('elemto este',element)
let value = null
const container = document.createElement('div'),
    preContainer = document.createElement('div')

container.setAttribute('id', `hoverImg__slide${id}`);
container.setAttribute('class', `hoverImg__slide`);
for (let item of images) {
    value += 1;
    if (value < 3) {
    
        let templateImages;
            if(document.body.classList.contains('page-department')){
                templateImages = `
                    <div class="hoverImg__slide__item">
                        <a href="${href}">
                            <img src="${item[2].Path.replace('-220-326', '-414-317')}" class="hoverImg__slide__image" />
                        </a>
                    </div>`
            }
            if (document.body.classList.contains('page-category') || document.body.classList.contains('resultado-busca')) {
                console.log('body hover')
                templateImages = `
                    <div class="hoverImg__slide__item">
                        <a href="${href}">
                            <img src="${item[2].Path.replace('-220-326', '-325-325')}" class="hoverImg__slide__image" />
                        </a>
                    </div>`
            }

    container.insertAdjacentHTML('beforeend', templateImages)

    }

}
element.innerHTML = ''
element.appendChild(container);
console.log(`#hoverImg__slide${id}`)
let slider = tns({
    container: `#hoverImg__slide${id}`,
    items: 1,
    slideBy: 'page',
    loop: false,
    autoplay: false,
    nav: false,
    startIndex: 1
});
setTimeout(function () {
    element.addEventListener("mouseout", (e) => {
    //console.log(e.target)
    e.preventDefault()
    e.stopPropagation()
    //console.log('mouseout');
    setTimeout(() => {
        slider.goTo(0);
    }, 100)

    });
    element.addEventListener("mouseenter", (e) => {
    //console.log('mouseenter');
    e.preventDefault()
    e.stopPropagation()
    setTimeout(() => {
        slider.goTo(2);
    }, 200)

    })
}, 500)




}
export default hoverProduct;