import easydropdown from 'easydropdown'
import Ordercart from './ordercart'
// import clickOnFlag from '../flagsRedirect'
// import getVitrina from '../pumProduct'
let extend = function (defaults, options) {
  var extended = {};
  var prop;
  for (prop in defaults) {
    if (Object.prototype.hasOwnProperty.call(defaults, prop)) {
      extended[prop] = defaults[prop];
    }
  }
  for (prop in options) {
    if (Object.prototype.hasOwnProperty.call(options, prop)) {
      extended[prop] = options[prop];
    }
  }
  return extended;
};
const flagDescuento = () => {
  let exist = document.querySelector('.montones-de-juguetes-3-octubre-2020');
  if (exist) {
    let getFlag = document.querySelectorAll('.discountData');

    setTimeout(() => {
      getFlag.forEach(element => {
        element.classList.add('green');
      });
    }, 800);
  }
  const flsDes = [...document.querySelectorAll('.discountData:not(.ok)')]

  ////console.log('descuentos')
  if (flsDes) {
    flsDes.forEach(i => {
      if (i.innerHTML.trim().indexOf('%') == -1) {
        i.classList.add('hide');
      } else {
        i.classList.add('ok');

        i.innerHTML = i.innerHTML.replace(/\,.*/, '%')
      }
    })
  }
}
class SmartFilter {
  constructor(options) {
    let defaults = {
      pageLimit: null,
      scrollInfinito: false,
      loadMoreWithButton: false,
      loadContent: ".showcases__grid[id^=ResultItems]>.showcases__grid",
      classGrid: ".showcases__grid",
      linksMenu: ".search-single-navigator",
      insertMenuAfter: ".search-multiple-navigator h3:first",
      selectOrderCustom: true,
      selectORderDivAppend: null,
      departamentBodyclass: "page-departament",
      departamentListProduct: true,
      categoryBodyClass: "page-category",
      buscaBodyClass: "page-busca",
      title: 'Tus Filtros',
      clearPrepend: false,
      compare: false,
      divMultiSelect: true,
      divMultiSelectDiv: null,
      callbackGeneral: function () { },
      callbackShowcases: function () { }
    };

    this.options = extend(defaults, options);
    this.state = {
      last: false,
      valuesSelects: [],
      updateValuesSelects: false,
      buscaPUrl: '',
      currentPage: 2,
      moreResults: true,
      morePaginate: false
    }
    this.pageLimit = this.options.pageLimit
    this.scrollInfinito = this.options.scrollInfinito
    this.loadMoreWithButton = this.options.loadMoreWithButton
    this.loadContent = document.querySelector(this.options.loadContent)
    this.classGrid = this.options.classGrid
    this.linksMenu = this.options.linksMenu
    this.insertMenuAfter = this.options.insertMenuAfter
    this.selectOrderCustom = this.options.selectOrderCustom
    this.selectORderDivAppend = document.querySelector(this.options.selectORderDivAppend);
    this.departamentBodyclass = this.options.departamentBodyclass
    this.departamentListProduct = this.options.departamentListProduct
    this.categoryBodyClass = this.options.categoryBodyClass
    this.buscaBodyClass = this.options.buscaBodyClass
    this.compare = this.options.compare

    this.callbackGeneral = this.options.callbackGeneral
    this.callbackShowcases = this.options.callbackShowcases

    this.selecHtml = `<select name="oderbyFilter" id="orderbyFilterSelect">
        <option value="">Seleccionar</option>
        <option value="OrderByReleaseDateDESC">Fecha de lanzamiento</option>
        <option value="OrderByBestDiscountDESC">Mejor descuento</option>
        <option value="OrderByPriceDESC">Mayor precio</option>
        <option value="OrderByPriceASC">Menor precio</option>
        <option value="OrderByNameASC">A - Z</option>
        <option value="OrderByNameDESC">Z - A</option>
        </select>
    `
    this.searchMultipleNavigator = document.querySelector('.search-multiple-navigator')
    this.searchSingleNavigator = document.querySelector('.search-single-navigator')
    this.contSelectLabel = `
                <div class="search__multiple__selected">

                        <div class="search__multiple__selected__action">

                            <button id="prevAction" class="prevAction">
                              prev
                            </button>
                            <button id="nextAction" class="nextAction">
                              next
                            </button>

                          <div id="search__multiple__selected__action__list" class="search__multiple__selected__action__list">
                          </div>

                        </div>
                        <a href="#" id="cleanFilters" class="search__multiple__selected__clearSelectAll">
                          Limpiar [X]
                        </a>

                </div>
            `
    this.moreResults = true
    this.currentPage = 2
    this.divMultiSelect = this.options.divMultiSelect,
      this.divMultiSelectDiv = document.querySelector(this.options.divMultiSelectDiv),
      this.init()
  }
  init() {
    //opciones para departamento
    this.optionsCategoryOrDepartaments()

  }
  optionsCategoryOrDepartaments() {

    if (document.body.classList.contains(this.departamentBodyclass)) {
      //console.log('entro a departamento', document.body.classList.contains(this.departamentBodyclass))
      if (!this.departamentListProduct) {
        //console.log('departamento completo');
        this.cleanFilter()
        this.createSelect();
        if (this.divMultiSelect) {

          this.multiSelectHtml()
        }
        this.setState({
          buscaPUrl: this.getSearchUrl(),
          morePaginate: true
        }, false)
        this.scrollInfinitoInit()
        this.loadMoreWithButtonR()
      } else {
        //console.log('departamento slingle')
        this.cleanFilter()
        this.closeDivsFilter()
      }
    }

    //validacion para categprias{
    if (!document.body.classList.contains(this.departamentBodyclass) || document.body.classList.contains(this.categoryBodyClass) || document.body.classList.contains(this.buscaBodyClass)) {
      //console.log('listin produc')
      this.cleanFilter()
      this.createSelect();
      if (this.divMultiSelect) {

        this.multiSelectHtml()
      }
      this.setState({
        buscaPUrl: this.getSearchUrl(),
        morePaginate: true
      }, false)
      this.scrollInfinitoInit()
      this.loadMoreWithButtonR()
    }

  }
  render(booleanRender) {
    ////console.log('el estadoes',this.state)
    if (booleanRender) {
      this.ajaxRender()
    }
    if (this.divMultiSelect) {
      this.addToSelect()
    }



  }
  setState(newState, booleanRender) {
    Object.assign(this.state, newState);

    this.render(booleanRender);

  }
  ajaxRender() {


    let request = new XMLHttpRequest();
    request.open('GET', this.state.buscaPUrl, true);
    request.onload = () => {
      if (request.status >= 200 && request.status < 400) {
        let data = request.responseText;
        //console.log("FUERA", request)
        if (data === "" && request.response === "") {
          //console.log("There is no more results for page", request)
          this.setState({
            moreResults: false,
            morePaginate: false,
            currentPage: 2
          }, false)
          // alert('ingreso a no existe!!')
          let d1 = document.querySelector('.contenedor__bg--pp')
          let failSerch = `<div class="fail-search">Sin resultados</div>`
          if (!document.querySelector('.fail-search')) {
            d1.insertAdjacentHTML('beforebegin', failSerch);
            document.querySelector('.showcases__grid.n1colunas').innerHTML = ""
          }


        } else {
          console.log(data)
          if(document.querySelector('.fail-search')){
            document.querySelector('.fail-search').remove()
          }
          if (this.state.last) {
            let container = document.implementation.createHTMLDocument().documentElement;
            container.innerHTML = data;
            this.callbackGeneral();
            this.callbackShowcases(container)


            var nodeList = container.querySelector(this.classGrid).innerHTML;
            ////console.log(nodeList)

            this.loadContent.innerHTML = nodeList


          } else {
            let container = document.implementation.createHTMLDocument().documentElement;
            container.innerHTML = data;
            this.callbackGeneral();
            this.callbackShowcases(container)

            var nodeList = container.querySelector(this.classGrid).innerHTML;

            this.loadContent.insertAdjacentHTML('beforeend', nodeList)




          }
          flagDescuento();
        }
      }
    };
    request.send();
    if (this.state.morePaginate) {
      let currentPage = this.state.currentPage + 1
      ////console.log('cambio el current',currentPage)
      this.setState({
        currentPage: currentPage
      }, false)
    }



  }
  getSearchUrl() {
    let url;
    let preg = /\/buscapagina\?.+&PageNumber=/i,
      pregCollection = /\/paginaprateleira\?.+PageNumber=/i

    //console.log('URL: ', url)
    $("script:not([src])").each(function () {
      var content = this.innerHTML;
      if (content.indexOf("buscapagina") > -1) {
        url = preg.exec(content);
        //console.log('URL: ', url)
        return false;
      } else if (content.indexOf("paginaprateleira") > -1) {
        url = pregCollection.exec(content);
        //console.log('URL: ', url)
        return false;
      }
    });

    if (typeof url === "object" && typeof url[0] !== "undefined")
      return url[0].replace("paginaprateleira", 'buscapagina');
    else {
      log("No se puede encontrar la URL de búsqueda de la página. \ N Intente agregar .js al final de la página. \n[Método: getSearchUrl]");
      return "";
    }

  }
  cleanFilter() {

    if (this.searchSingleNavigator) {
      let subcategories = this.searchSingleNavigator.querySelectorAll('h4,h5,h3')
      if (this.searchSingleNavigator.querySelector('ul.Faixa.de.preço')) {
        //oculta links de precios
        this.searchSingleNavigator.querySelector('h5.HideFaixa-de-preco').style.display = 'none';
        this.searchSingleNavigator.querySelector('ul.Faixa.de.preço').style.display = 'none';
      }

      //limpiar numeros de link en search-single-navigator
      ////console.log('subcategorias',subcategories)
      subcategories.forEach(ele => {

        let links = ele.querySelector('a')
        if (links) {
          let text = links.textContent
          text = text.replace(/\([0-9]+\)/ig, " ")
          links.textContent = text;
        }

        ////console.log(ele.nextElementSibling.tagName)
        if (ele.nextElementSibling && ele.nextElementSibling.tagName == 'UL') {
          let linkSubs = ele.nextElementSibling.querySelectorAll('a')
          for (let element of linkSubs) {

            let text = element.textContent

            text = text.replace(/\([0-9]+\)/ig, " ")

            element.textContent = text;

          }

        }
        let itemH4 = ele.querySelector('a');
        if (itemH4) {
          itemH4.addEventListener('click', (e) => {
            if (ele.nextElementSibling.children.length > 0) {
              e.preventDefault()
              ele.nextElementSibling.classList.toggle('open')
              itemH4.classList.toggle('open')
              // gsap.from(ele.nextElementSibling, {
              //   y: -40,
              //   opacity: 1,
              //   duration: 0.5
              // });
            }
          });
        }

      })

    }
    if (this.searchMultipleNavigator) {
      let fieldsetsMultiple = this.searchMultipleNavigator.querySelectorAll('fieldset')
      fieldsetsMultiple.forEach((ele) => {
        let fieldH5 = ele.querySelector('h5'),
          inputs = ele.querySelectorAll('input'),
          divIn = ele.querySelector('div')

        if (divIn.children.length == 0) {
          divIn.parentElement.style.display = 'none';
        }

        if (fieldH5.textContent == 'Faixa de preço') {
          fieldH5.textContent = 'Precio'
        }
        if (fieldH5.textContent == 'Genero') {
          fieldH5.textContent = 'Género'
        }
        for (let element of inputs) {

          let label = element.parentElement,
            text = label.textContent
          text = text.replace(/\([0-9]+\)/ig, "")
          label.textContent = text;
          label.insertAdjacentElement('afterbegin', element);
          label.classList.add(`fil-${text.replace(/\s/g, "")}`);
          label.setAttribute('data-value', `${text.replace(/\s/g, "")}`);

          element.addEventListener('change', this.actionChecbox.bind(this))
          ////console.log('inpurts',element)
        }
        divIn.classList.add('close__div')
        fieldH5.insertAdjacentHTML('beforeend', '<span class="iconp-angle-right"></span>')

        fieldH5.addEventListener('click', (e) => {
          e.preventDefault()

          e.target.classList.toggle('active__panel')
          e.target.nextElementSibling.classList.toggle('close__div')
          if (!e.target.nextElementSibling.classList.contains('close__div')) {
            // gsap.from(e.target.nextElementSibling, {
            //   y: -40,
            //   opacity: 0,
            //   duration: 0.5
            // });
          }
        })
      })
    }

  }
  closeDivsFilter() {
    if (document.querySelectorAll('fieldset')) {
      document.querySelectorAll('fieldset').forEach(ele => {
        ////console.log('searchMultipleNavigator', this.searchMultipleNavigator)
        document.querySelector('div').classList.add('close__div')
      })
    }
  }
  actionChecbox(e) {
    let checkbox = e.target,
      fq = checkbox.getAttribute('rel'),
      textValueSect = checkbox.parentElement.getAttribute('data-value'),
      last = false

    this.currentPage = 2

    if (document.getElementById('cleanFilters')) {
      document.getElementById('cleanFilters').style.display = 'block'
    }
    if (e.target.checked == true) {
      let dataValue = e.target.parentElement.dataset.value
      let containerInputs = e.target.parentElement.parentElement.querySelectorAll('label')
      containerInputs.forEach(label => {
        if (label.dataset.value !== dataValue && label.querySelector('input').checked) {
          label.querySelector('input').click()
          label.querySelector('input').checked = false
        }
      });
      setTimeout(() => {
        let newbuscaPUrl = `${this.state.buscaPUrl.replace(`&PS`, `&${fq}&PS`).replace(/pagenumber\=[0-9]*/i, "PageNumber=1")}`,
          newSelect = [...this.state.valuesSelects, textValueSect]
        e.target.parentElement.classList.add('checkeado');
        e.target.parentElement.parentElement.previousElementSibling.textContent = e.target.parentElement.textContent
        // //console.log('hshhshs', newbuscaPUrl)
        this.setState({
          last: true,
          valuesSelects: newSelect,
          updateValuesSelects: true,
          buscaPUrl: newbuscaPUrl,
          moreResults: true,
          currentPage: 2
        }, true)
      }, 90)

    } else {
      let newbuscaPUrl = `${this.state.buscaPUrl.replace(`&${fq}`, '').replace(/pagenumber\=[0-9]*/i, "PageNumber=1")}`,
        removeSelects = this.state.valuesSelects.filter(item => item != textValueSect),
        urlFilter = newbuscaPUrl.split('?')
      // //console.log('newbuscaPUrl: ', newbuscaPUrl);
      urlFilter = urlFilter[1].split('&')
      // //console.log('urlFilter 02', urlFilter);
      e.target.parentElement.classList.remove('checkeado')
      this.setState({
        last: true,
        valuesSelects: removeSelects,
        updateValuesSelects: true,
        buscaPUrl: newbuscaPUrl,
        moreResults: true,
        currentPage: 2
      }, true)
    }
    if (document.getElementById('loadMoreContentBtn')) {
      document.getElementById('loadMoreContentBtn').style.opacity = ' 1'
      document.getElementById('loadMoreContentBtn').innerHTML = `<span></span><span></span><span></span><span></span><p class="smartFilter__loadMoreContent__p">Cargar más productos</p>
          <p class="smartFilter__loadMoreContent__icon"></p>
          <span class="icon-next-arrow"></span>`
    }
  }
  getUrlParameter(name, url) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(url);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
  }
  createSelect() {

    if (this.selectOrderCustom) {
      this.selectORderDivAppend.insertAdjacentHTML('beforeend', this.selecHtml)
      const orderBy = easydropdown('#orderbyFilterSelect', {
        callbacks: {
          onSelect: value => {
            this.changeSelectOrder(value)
          }
        }
      });

    }
  }
  changeSelectOrder(value) {
    if (value == "") {
      let last = false,
        newbuscaPUrl = this.state.buscaPUrl.replace('&O=/\?*/&PS=.', `&O=${value}&PS=`).replace(/pagenumber\=[0-9]*/i, "PageNumber=1")

      this.setState({
        last: last,
        updateValuesSelects: false,
        buscaPUrl: newbuscaPUrl,
        currentPage: 2,
        moreResults: true,
        morePaginate: true
      }, true)

    } else {
      let last = true,
        urlParams = new URLSearchParams(this.state.buscaPUrl);
      if (urlParams.has('O')) {
        let valPar = this.getUrlParameter('O', this.state.buscaPUrl),
          newbuscaPUrl = this.state.buscaPUrl.replace(`&O=${valPar}`, `&O=${value}`).replace(/pagenumber\=[0-9]*/i, "PageNumber=1")
        ////console.log('select1', newbuscaPUrl)
        this.setState({
          last: last,
          updateValuesSelects: false,
          buscaPUrl: newbuscaPUrl,
          currentPage: 2,
          moreResults: true,
          morePaginate: false
        }, true)
      } else {
        let newbuscaPUrl = this.state.buscaPUrl.replace('&PS=', `&O=${value}&PS=`).replace(/pagenumber\=[0-9]*/i, "PageNumber=1")
        //console.log('select2')
        this.setState({
          last: last,
          buscaPUrl: newbuscaPUrl,
          currentPage: 2,
          moreResults: true,
          morePaginate: false
        }, true)
      }
    }
    if (document.getElementById('loadMoreContentBtn')) {
      document.getElementById('loadMoreContentBtn').style.opacity = ' 1'
      document.getElementById('loadMoreContentBtn').innerHTML = `<span></span><span></span><span></span><span></span><p class="smartFilter__loadMoreContent__p">Cargar más productos</p>
      <p class="smartFilter__loadMoreContent__icon"></p>
      <span class="icon-next-arrow"></span>`
    }

  }
  multiSelectHtml() {

    if (this.divMultiSelectDiv) {
      this.divMultiSelectDiv.insertAdjacentHTML('beforebegin', this.contSelectLabel)
      document.querySelector('.search__multiple__selected').addEventListener('click', this.actionMultiple)

    }
  }
  actionMultiple(e) {

    if (e.target.hasAttribute('data-value')) {
      e.preventDefault()

      let value = e.target.getAttribute('data-value'),
        elementSelected = document.querySelector(`.search-multiple-navigator label.checkeado[data-value*="${value}"]`)
      elementSelected.click();

    }
    if (e.target.id == 'cleanFilters') {
      e.preventDefault()
      let elementSelected = document.querySelectorAll('.search-multiple-navigator label.checkeado'),
        index = 4;

      for (let element of elementSelected) {
        setTimeout(function () {
          element.click();
        }, 600 + index)
        index = index * 4
      }
    }

    if (e.target.id == 'nextAction') {
      document.querySelector('#search__multiple__selected__action__list').scrollLeft += 20;
    }
    if (e.target.id == 'prevAction') {
      document.querySelector('#search__multiple__selected__action__list').scrollLeft -= 20;
    }


  }
  addToSelect() {
    let searchMultiple = document.querySelector('.search__multiple__selected');
    if (searchMultiple) {
      let buttonHtml = '',
        constButton = searchMultiple.querySelector('#search__multiple__selected__action__list')

      if (this.state.valuesSelects.length > 0) {
        //console.log('estaso', this.state.valuesSelects)
        this.state.valuesSelects.forEach((ele, i) => {
          buttonHtml += `
        <button type="button" data-value="${ele}" class="button">${ele} <span class="search__multiple__selected__action__list__filter">X</span></button>
        `

        })

        searchMultiple.querySelector('#search__multiple__selected__action__list').innerHTML = buttonHtml;
        searchMultiple.querySelector('#cleanFilters').style.display = 'flex'
        //console.log('evalueee', constButton.offsetWidth < constButton.scrollWidth)
        if (constButton.offsetWidth < constButton.scrollWidth) {
          /* searchMultiple.querySelector('#prevAction').style.display = 'block'
          searchMultiple.querySelector('#nextAction').style.display = 'block' */
        } else {
          searchMultiple.querySelector('#prevAction').style.display = 'none'
          searchMultiple.querySelector('#nextAction').style.display = 'none'
        }

      } else {
        searchMultiple.querySelector('#search__multiple__selected__action__list').innerHTML = '';
        searchMultiple.querySelector('#cleanFilters').style.display = 'none'
        searchMultiple.querySelector('#prevAction').style.display = 'none'
        searchMultiple.querySelector('#nextAction').style.display = 'none'
      }
    }
  }
  scrollInfinitoInit() {

    if (this.scrollInfinito) {
      window.addEventListener('scroll', () => {
        ////console.log('scroll funciona')
        if (this.state.moreResults) {

          if (window.scrollY > (this.loadContent.offsetHeight + this.loadContent.offsetTop - 440)) {
            ////console.log('haciendo scolrr1')
            let newbuscaPUrl = `${this.state.buscaPUrl.replace(/pagenumber\=[0-9]*/i, "PageNumber=" + this.state.currentPage)}`
            this.setState({
              last: false,
              buscaPUrl: newbuscaPUrl,
              morePaginate: true
            }, true)

          }
        }
      })
    }
  }
  loadMoreWithButtonR() {

    if (this.loadMoreWithButton) {

      let itemsLoad = document.querySelectorAll('.showcases__grid[id^="ResultItems"] ul').length,
        totalItems = document.querySelector('.resultado-busca-numero .value').textContent,
        btnHtml = `<div class="smartFilter__loadMoreContent">
                      <button href="#" id="loadMoreContentBtn" class="smartFilter__loadMoreContent__btn animated-button">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <p class="smartFilter__loadMoreContent__p">Cargar más productos</p>
                        <p class="smartFilter__loadMoreContent__icon"></p>
                        <span class="icon-next-arrow"></span>
                      </button>
                    </div>`;
      //console.log('items', itemsLoad)
      document.querySelector('.listingProductPage .grid').insertAdjacentHTML('afterend', btnHtml)
      //this.loadContent.insertAdjacentHTML('afterend', btnHtml)
      if (itemsLoad >= 37) {

        document.getElementById('loadMoreContentBtn').addEventListener('click', (e) => {
          e.preventDefault()
          document.querySelector('.smartFilter__loadMoreContent__p').style.display = 'none';
          document.querySelector('.smartFilter__loadMoreContent__icon').classList.add('loader')
          setTimeout(() => {
            document.querySelector('.smartFilter__loadMoreContent__icon').classList.remove('loader')
            document.querySelector('.smartFilter__loadMoreContent__p').style.display = 'block';
            let newbuscaPUrl = `${this.state.buscaPUrl.replace(/pagenumber\=[0-9]*/i, "PageNumber=" + this.state.currentPage)}`
            this.setState({
              last: false,
              buscaPUrl: newbuscaPUrl,
              morePaginate: true
            }, true)
            //console.log('mas productos', itemsLoad);
            setTimeout(() => {
              //   clickOnFlag()
              //   getVitrina()
            }, 1500);
            document.querySelector('.showcases__grid.n1colunas').scrollIntoView({
              behavior: "smooth",
              block: "end",
              inline: "nearest"
            });
          }, 2000);
        })
      } else {
        this.setState({
          moreResults: false,
          morePaginate: false,
          currentPage: 2
        }, false)
        // //console.log("There is no more results for page")
        if (document.getElementById('loadMoreContentBtn')) {
          document.getElementById('loadMoreContentBtn').style.opacity = ' 0.6'
          document.getElementById('loadMoreContentBtn').innerHTML = 'No hay más productos'
        }
      }


    }
  }
}

export default SmartFilter;
