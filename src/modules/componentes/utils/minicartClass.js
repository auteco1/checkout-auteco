import Ordercart from './ordercart';
let extend = function (defaults, options) {
  var extended = {};
  var prop;
  for (prop in defaults) {
    if (Object.prototype.hasOwnProperty.call(defaults, prop)) {
      extended[prop] = defaults[prop];
    }
  }
  for (prop in options) {
    if (Object.prototype.hasOwnProperty.call(options, prop)) {
      extended[prop] = options[prop];
    }
  }
  return extended;
};
class MinicartObj {
  constructor(options) {
    let defaults = {
      divContMinicart: null,
      btnTotalProduct: null,
      classActiveMinicart: 'active',

    }
    this.options = extend(defaults, options);
    this.state = {
      products: [],
      totals: [],
      itemTotals: 0
    }
    this.divContMinicart = document.getElementById(this.options.divContMinicart)
    this.btnTotalProduct = document.querySelector(this.options.btnTotalProduct)
    this.classActiveMinicart = this.options.classActiveMinicart
    this.contItem = document.createElement('div')
    this.contItem.setAttribute('class', 'minicart__items-cont')
    this.init()
  }
  setState(newsState) {

    Object.assign(this.state, newsState);
    this.render()
  }
  render() {
   
    const {products,totals,itemTotals} = this.state,
      divCont = this.contItem,
      divAppen = document.getElementById('itemsMinicart')
      // console.log('state: ',this.state);
    //console.log('minicar', products.length);
    
    if (products.length >= 1) {
      if (document.getElementById('minicart')) {
        document.querySelector('.minicart__total').classList.remove('ceroProducts');
        document.getElementById('minicart').classList.remove('ceroProducts');
        document.getElementById('itemsMinicart').style = 'display: block';
        document.querySelector('.minicart__items__promo').style = 'display: none';
        document.querySelector('.minicart__footer__buy').textContent="Finalizar compra"
        
        // let TextsEmpty = document.querySelectorAll('.no-products')
        // TextsEmpty.forEach(text => {
        //   text.style.dis
        // });
      }
     
      this.contItem.innerHTML = '';
      this.contItem.innerHTML = products.map((element, index) => {
        // console.log('PRODUCTO CARRITO: ',element)
        let bestPriceT = '',
          listPriceT = ''
      
        
          //console.log('paso por aca 1')
          if (element.price > element.sellingPrice) {
            bestPriceT = `${element.sellingPrice.toString().substring(0, element.sellingPrice.toString().length - 2)}`
            listPriceT = `${element.price.toString().substring(0, element.price.toString().length - 2)}`
            listPriceT = `<span class="last">$ ${Ordercart.currency(parseInt(listPriceT))}</span>`
            //console.log('precio descuento',listPriceT)
  
          } else {
            bestPriceT = `${element.price.toString().substring(0, element.price.toString().length - 2)}`
          }
          bestPriceT = `<span class="best">$ ${Ordercart.currency(parseInt(bestPriceT))}</span>`
  
        

        return `
                    <div class="itemM__box">
                      <a href="#" class="itemM__box__removeProduct" data-index="${index}" data-qy="${element.quantity}">
                       
                      </a>
                        <div class="itemM__box__content">
                            <figure class="itemM__box__figure">
                                <a href="${element.detailUrl}">
                                    <img class="itemM__box__figure__image" src="${element.imageUrl.replace('-55-55','-160-160')}" alt="${element.name}">
                                </a>
                            </figure>
                            <div class="itemM__box__namePrice">
                                <div class="itemM__box__price">
                                  <h4 class="itemM__box__namePrice__name">
                                      ${element.name}
                                  </h4>
                                  ${element.skuName.includes('TALLA ')?`<p clas="itemM__box__namePrice__sku">${element.skuName}</p>`:''}
                                  <p class="itemM__box__namePrice__price">
                                      ${bestPriceT}<br>
                                      ${listPriceT}
                                  </p>
                                </div>
                                <div class="itemM__box__qy">
                                  <div class="itemM__box__qy__int">
                                      <button type="button" class="btnIcon-muminus btn itemM__box__qy__int__btn" data-type="minus" data-field="mpquant[${element.productId}]" data-index="${index}"></button>
                                      
                                      <input size="20" type="text" class="itemM__box__qy__int__quantity" 
                                      value="${element.quantity}" productindex="0" style="display:block" 
                                      name="mpquant[${element.productId}]" min="1" max="20">
                                    
                                      <button type="button" class="btnIcon-muplus btn itemM__box__qy__int__btn" data-type="plus" data-field="mpquant[${element.productId}]" data-index="${index}"></button>
                                  </div>

                                </div>
                            </div>
                        </div>
                    </div>
            `

      }).join('')
      if (divAppen) {
        divAppen.innerHTML = ''
        divAppen.appendChild(this.contItem)
      }
    } else if (products.length == 0) {
      //console.log('no hay productos')
      if (document.querySelector('.minicart__total')) {
        document.querySelector('.minicart__total').classList.add('ceroProducts')
        document.getElementById('minicart').classList.add('ceroProducts');
        document.getElementById('itemsMinicart').style = 'display: none';
        document.querySelector('.minicart__items__promo').style = 'display: block';
      }
      if( document.querySelector('.minicart__footer__buy')){
        document.querySelector('.minicart__footer__buy').textContent="Agregar productos"
      }
      let title = document.querySelector('.minicart__header p')
      if(title){
        title.style.display="none"
        title.parentElement.style.justifyContent="end"
      }
      if(document.querySelector('.minicart__total')){
        document.querySelector('.minicart__total').style.background="#fff"
      }
      document.querySelector('#minicart__totals .minicart__totals')?document.querySelector('#minicart__totals .minicart__totals').style.display="none":false

    }
    
    // console.log('TOTAL: ',totals.total);
    let totale = totals.total>0? `
    <div class="minicart__totals">
        <p style="${totals.discounts.length > 0 ? 'display:flex;' : 'display:none;'}" class="minicart__totals__descuento">
            <span class="minicart__totals__text">Descuentos</span>
            <span id="minicart__discounts" class="minicart__totals__total">
            $ ${totals.discounts.length > 0 ? Ordercart.currency(parseInt(totals.discounts[0].value.toString().substring(0, totals.discounts[0].value.toString().length-2))) : ''}
            
            </span>
        </p>
        <p style="${totals.shipping.length > 0 ? 'display:flex;' : 'display:none;'}" class="minicart__totals__envio">
            <span class="minicart__totals__text">Envío</span>
            <span id="minicart__discounts" class="minicart__totals__total">
              $ ${totals.shipping.length > 0 ? Ordercart.currency(parseInt(totals.shipping[0].value.toString().substring(0, totals.shipping[0].value.toString().length - 2))) : ''}
              
            </span>
          </p>

        <p class="minicart__totals__totalP">
            <span class="minicart__totals__text">Total</span>
            <span id="minicart__total" class="minicart__totals__total">
            $ ${Ordercart.currency(parseInt(totals.total.toString().substring(0, totals.total.toString().length-2)))}
            </span>
        </p>
    </div>
    `:''
    if(document.getElementById('minicart__totals')){
      document.getElementById('minicart__totals').innerHTML = ''
      document.getElementById('minicart__totals').innerHTML = totale
    }
    

    let items = [...document.querySelectorAll('.numberofItems')]
        items.forEach(ele=>{
            ele.innerHTML =  itemTotals
    })
  }
  async init() {
    //console.log('inic minicart', this.divContMinicart)
    this.setHtmlCart()
    let order = await Ordercart.miniCart()
    this.setState({
      products: order.items,
      totals: order.totals,
      itemTotals: order.tItems
    })
    truncateProduct()
  }
  setHtmlCart() {
    let htmlMini = `
    <div class="shadow-cart"></div>
      <aside id="minicart" class="minicart">
        <div class="minicart__container">
          <div class="minicart__header">
            <p>Mi Carrito de Compras</p>
            <button id="closeMinicart">Cerrar Carrito
              <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTgiIGhlaWdodD0iMTgiIHZpZXdCb3g9IjAgMCAxOCAxOCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZD0iTTEuMTc0NzIgMS4xNzUwMkMxLjYzMDMzIDAuNzE5NDEgMi4zNjkwMiAwLjcxOTQxIDIuODI0NjMgMS4xNzUwMkw4Ljk5OTY3IDcuMzUwMDZMMTUuMTc0NyAxLjE3NTAyQzE1LjYzMDMgMC43MTk0MSAxNi4zNjkgMC43MTk0MSAxNi44MjQ2IDEuMTc1MDJDMTcuMjgwMiAxLjYzMDYzIDE3LjI4MDIgMi4zNjkzMyAxNi44MjQ2IDIuODI0OTRMMTAuNjQ5NiA4Ljk5OTk4TDE2LjgyNDYgMTUuMTc1QzE3LjI4MDIgMTUuNjMwNiAxNy4yODAyIDE2LjM2OTMgMTYuODI0NiAxNi44MjQ5QzE2LjM2OSAxNy4yODA1IDE1LjYzMDMgMTcuMjgwNSAxNS4xNzQ3IDE2LjgyNDlMOC45OTk2NyAxMC42NDk5TDIuODI0NjMgMTYuODI0OUMyLjM2OTAyIDE3LjI4MDUgMS42MzAzMyAxNy4yODA1IDEuMTc0NzIgMTYuODI0OUMwLjcxOTEwNSAxNi4zNjkzIDAuNzE5MTA1IDE1LjYzMDYgMS4xNzQ3MiAxNS4xNzVMNy4zNDk3NiA4Ljk5OTk4TDEuMTc0NzIgMi44MjQ5NEMwLjcxOTEwNSAyLjM2OTMzIDAuNzE5MTA1IDEuNjMwNjMgMS4xNzQ3MiAxLjE3NTAyWiIgZmlsbD0iYmxhY2siLz4KPC9zdmc+Cg==" alt="Cerrar Carrito">
            </button>
          </div>
          <div id="itemsMinicart" class="minicart__items"></div>
          <div class="minicart__items__promo ceroProducts">
            <h3 class="no-products">Tu carrito está vacío</h3>
            <div>
              <p class="no-products">Tenemos muchos productos para ti, elije lo que más te guste.</p>
            </div>
          </div>
          <div class="minicart__total">
            <div id="minicart__totals">
            </div>
            <div class="minicart__footer">
              <a href="/checkout/#/cart" class="minicart__footer__buy buttons buttons__v1 btn">
                Finalizar compra
              </a>
            </div>
          </div>
        </div>
      </aside>`
    if (this.divContMinicart) {
      this.divContMinicart.insertAdjacentHTML('afterend', htmlMini)
      document.getElementById('itemsMinicart').addEventListener('click',(e)=>{
        this.eventsMinicart(e)
      })
      document.getElementById('closeMinicart').addEventListener('click',(e)=>{
          this.openAction('close')
          document.querySelector('.shadow-cart').classList.toggle('active')
          document.querySelector('#minicartDeskt').classList.toggle('active')
      })
    }
  }
  eventsMinicart(e){
    let element = e.target;
    console.log(element)
    if(e.target.classList.contains('itemM__box__qy__int__btn')) {
        e.preventDefault();
        e.stopImmediatePropagation()
       
        Ordercart.buttonsChangeQ(e.target,(value)=>{
          console.log('el valor cmabio',value)
            let index = element.getAttribute('data-index')
           
            console.log('el index',index)
            Ordercart.changeQuyCart(index,value,()=>{
              //this.update()
              setTimeout(() => {
                // console.log('89089898')
                this.update()
              }, 400);
            })
        });
    }
    if(element.classList.contains("itemM__box__removeProduct")) {
        e.preventDefault();
        e.stopImmediatePropagation()
        element.closest('.itemM__box');
        let updateC = this.update
        // console.log('minicart 1', index)
        let index = element.getAttribute('data-index'),
            qy = element.getAttribute('data-qy')
        Ordercart.removeItem(index,qy,async (orderForm)=>{
            element.closest('.itemM__box').remove()
            setTimeout(()=>{
              this.update()
            },300)
            //openAction()
        })
    }
    truncateProduct()
  }
  openAction(action){
    const miniCartCont = document.getElementById('minicart'),
    body = document.body
    if (miniCartCont) {
      if(action == 'open') {
        if (document.querySelector('.header__actions__minicart .addedProductImage')) {
          document.querySelector('.header__actions__minicart .addedProductImage').classList.add('addedImage')
          setTimeout(() => {
            document.querySelector('.header__actions__minicart .addedProductImage').classList.remove('addedImage')
            document.querySelector('.productP__infoCont__buy').classList.remove('addedProduct')
          },3500)  
        }
        setTimeout(() => {
          miniCartCont.classList.toggle('minicart-open')
          body.classList.toggle('open-minicart')
        },3500)
      }else if(action == 'toggle'){
          miniCartCont.classList.toggle('minicart-open')
          body.classList.toggle('open-minicart')
      }else {
          miniCartCont.classList.remove('minicart-open')
          body.classList.remove('open-minicart')
          
      }
    }
  }
  async update(){
    let order = await Ordercart.miniCart()
    this.setState({
      products: order.items,
      totals: order.totals,
      itemTotals: order.tItems
    })
    truncateProduct()
  }
}
const truncateProduct=()=>{
  let namesProductsCart = document.querySelectorAll('.itemM__box__namePrice__name')
  namesProductsCart.forEach(name => {
    name.textContent = truncate(name.textContent)
  });
}
const truncate=(input)=> {

  if (input.length > 15) {
    // console.log('entro nombre',input.substring(0,5) + '...');
     return input.substring(0, 75) + '...';
  }
  return input;
}
export default MinicartObj;
