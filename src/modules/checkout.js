import '../styles/checkout.scss';
import checkoutCart from './componentes/checkout/checkout-cart';
import checkoutFooter from './componentes/checkout/checkout-footer';
import checkouProfile from './componentes/checkout/checkout-profile';
import buttonBackCheckout from './componentes/checkout/comeBack-btn';
import shipping from './componentes/checkout/checkout-shipping';
import payment from './componentes/checkout/checkout-payment';
import modalPages from './componentes/checkout/checkout-modal';
import seeMore from './componentes/checkout/see-more';
import placeholder from './componentes/checkout/checkout-placeholder';
import orderFormVitex from './componentes/checkout/checkout-orderFormVitex';
// import newDirection from './componentes/checkout/checkout-newDirection';
// import confirmation from './componentes/checkout/checkout-confirmation-custom';

window.addEventListener('load', function () {
  

  checkoutCart()
  checkouProfile()
  buttonBackCheckout()
  shipping()
  payment()
  modalPages()
  seeMore()
  placeholder()
  checkoutFooter()
  // newDirection()
  // confirmation()


  // vtexjs.checkout
  // .getOrderForm()
  // .then(function (orderForm) {
  //   //console.log("orderForm", orderForm);
  // })




});

const hashTypeDocument = () => {

  var inputDocumentType = $('#client-profile-data .box-client-info-pf .document-box .other-document .client-document-type input')
  var documentTypeSelectField = $('#client-profile-data .box-client-info-pf .document-box .other-document #documentType')
  documentTypeSelectField.on('change', function () {
    inputDocumentType.val(documentTypeSelectField.val().toUpperCase())
    saveOpenTextField(documentTypeSelectField.val().toUpperCase())
  });

  // Consultando tipo de documento em cliente já cadastrado
  function searchTypeDocument() {
    // Salvando tipo de documento no master data
    inputDocumentType.val(documentTypeSelectField.val())

    var clientData = vtexjs.checkout.orderForm.clientProfileData;
    if (!clientData) {
      return
    }

    var documentType = clientData.documentType

    if (documentType !== null) {
      return documentType
    } else {
      return inputDocumentType.val()
    }
  }

  // Salvando tipo de documento no campo de comentário
  function saveOpenTextField(typeDocument) {
    vtexjs.checkout.getOrderForm()
      .then(function (orderForm) {
        var type = 'Tipo del documento: ' + typeDocument
        return vtexjs.checkout.sendAttachment('openTextField', {
          value: type
        });
      })
  }

  // Tipo de documento
  saveOpenTextField(searchTypeDocument())
}

window.addEventListener("hashchange", () => {
  hashTypeDocument();
  checkoutCart()
  shipping()
  buttonBackCheckout()
  modalPages()
  seeMore()
  placeholder()
  checkouProfile()
  // confirmation()


  vtexjs.checkout.getOrderForm().done(function (orderForm) {
    orderFormVitex(orderForm)
  })

});



addEventListener('DOMContentLoaded', () => {

  shipping()
  buttonBackCheckout()
  modalPages()
  // newDirection()
  // confirmation()



})
window.addEventListener("hashchange", () => {
  hashTypeDocument();
  setTimeout(() => {
    var addiAllySlug='autecomobility-ecommerce';
    $.getScript('<https://s3.amazonaws.com/statics.addi.com/vtex/js/vtex-checkout-co.bundle.min.js>');
    payment()
    shipping()
    buttonBackCheckout()

    modalPages()

  }, 2000)

  if (location.hash == "#/shipping") {



  }


});

$(window).on("orderFormUpdated.vtex", function (evt, orderForm) {
  // //console.log("orderForm", orderForm)
  orderFormVitex(orderForm)
  // newDirection()
})

/* SE AGREGA LIBRERIA GENERADOR QR */
$.getScript('/files/qrcode.min.js');