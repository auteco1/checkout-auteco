import '../styles/auteco/checkout/_checkout-confirmation-custom.scss';
import checkoutFooter from './componentes/checkout/checkout-footer';
import buttonBackCheckout from './componentes/checkout/comeBack-btn';
import seeMore from './componentes/checkout/see-more';

// import '../styles/_checkout-confirmation-custom.scss';

const newHtml = (orderId) => {

    const d = new Date();
    const months = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
    const dateDom =  document.querySelectorAll("time");

    const html = `<div class="insert-orderid" id="insert-order-id">
    <p class="content-orderId">Pedido ${orderId}</p>
    <p class="date-payment">${dateDom.length == 1 && dateDom[0].innerText}</p>
    </div>`
    return html
}
const newHtmlEmail = (email) => {

    const html = `
    <p class="insert-email" id="insert-email-id">${email}</p>
    `
    return html
}


function addOrderId() {

    setTimeout(() => {
        if (document.getElementById('insert-order-id') == null) {
            const orderId = document.querySelectorAll("#order-id"),
                contentOrderId = document.querySelectorAll('div div.cconf-bank-invoice-container.row-fluid');

            // console.log("orderId", orderId)

            orderId.length != 0 && (contentOrderId && contentOrderId[0].insertAdjacentHTML('afterend', newHtml(orderId[0].innerHTML.replace('#', ''))))
        }

    }, 500);

}

function addMail() {
    setTimeout(() => {

        if (document.getElementById('insert-email-id') == null) {
            const email = document.querySelectorAll('#app-top .cconf-client-email'),
                content = document.querySelectorAll('.cconf-profile .lh-copy');



            content.length == 1 && content[0].insertAdjacentHTML('beforeend', newHtmlEmail(email[0].innerText))

        }


    }, 500);
}


function confirmation() {

    addOrderId();
    addMail();
}



window.addEventListener('load', function () {
    confirmation()
    checkoutFooter()
    buttonBackCheckout()
    seeMore()
});

window.addEventListener("hashchange", () => {
    confirmation()

});

addEventListener('DOMContentLoaded', () => {
    confirmation()


})
