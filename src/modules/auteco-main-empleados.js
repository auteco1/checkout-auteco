import '../styles/empleados.scss'
// import '../files/js/JS.aut.col.functions.base.js';

/** Manejo de errores */
const showErrorsForm = () => {
  $('form:visible').find('.error').removeClass('error')
  $('[required]', 'form:visible').each(function () {
    if (!this.checkValidity() || !$(this).val()) $(this).closest('fieldset').addClass('error')
  })
  $('input, textarea, select, .radioType', '.error').on('focus change click', function () {
    $(this).closest('.error').removeClass('error')
  })
}
/** Manejo de errores */

/** EMPLEADOS */

const clickSendEmpleados = () => {
  $('.btn-default').on('click', function (e) {
    e.preventDefault()
    // console.log($("form:visible")[0].checkValidity());
    if (!$('form:visible')[0].checkValidity()) {
      // alert('hola');
      showErrorsForm()
      return false
    }
    $('.content-loading-button').show()
    $('.btn-default').hide()
    setTimeout(function () {
      validateUserID()
    }, 3000)
  })
}

const saveTempDataEmpleados = () => {
  const ret = {}
  $('form:visible').serializeArray().forEach(function (elem) {
    ret[elem.name] = elem.value
  })

  ret.approved = true
  delete ret.data_use
  return ret
}

const getInfoUP = () => {
  const formData = saveTempDataEmpleados()
  const url = '/api/dataentities/UP/search?_where=%28document=%2A' + formData.document + '%2A%29&_fields=id'
  let result
  $.ajax({
    headers: {
      Accept: 'application/vnd.vtex.ds.v10+json',
      'Content-Type': 'application/json'
    },
    crossDomain: true,
    async: false,
    type: 'GET',
    url: url,
    success: function (data) {
      result = data
    },
    error: function (data) {
      console.log('Error al consumir el crÃ©dito de Cliente', data)
      return false
    }
  })
  return result
}

const validateUserID = () => {
  /** VALIDACION PARA VERIFICAR SI EXISTE EN LA BASE DE DATOS DE CEDULAS */
  const formData = saveTempDataEmpleados()
  const url = '/api/dataentities/UP/search?_where=%28document=%2A' + formData.document + '%2A%29&_fields=company'
  $.ajax({
    headers: {
      Accept: 'application/vnd.vtex.ds.v10+json',
      'Content-Type': 'application/json'
    },
    cache: false,
    crossDomain: true,
    async: true,
    type: 'GET',
    url: url,
    success: function (data) {
      /** VALIDACION PARA VERIFICAR SI EXISTE EN LA BASE DE DATOS DE CEDULAS */
      if (data.length >= 1) {
        const validarUsuario = validateExitsClient()
        console.log(validarUsuario)
        if (validarUsuario[0].emailLogin != null) {
          /** USUARIO EXISTE Y MOSTRAR EL MAIL REGISTRADO */
          $('.content-loading-button').hide()
          $('.btn-default').show()
          $('.mensajes').text('')
          $('.mensajes').html('Ya existe un usuario asociado a este documento. <br> Intenta iniciar sesión por favor con este email:  <strong>' + validarUsuario[0].emailLogin + '</strong>')
          $('#myModal').modal('show')
        } else {
          let formData = saveTempDataEmpleados()
          if (validarUsuario[0].document === formData.document) {
            const response = insertEmpleado()
            if (response != null) {
              const idUser = getInfoUP()
              formData = saveTempDataEmpleados()
              const ret = {}

              ret.document = formData.document
              ret.emailLogin = formData.email

              $.ajax({
                headers: {
                  Accept: 'application/vnd.vtex.ds.v10+json',
                  'Content-Type': 'application/json'
                },
                url: '/api/dataentities/UP/documents/' + idUser[0].id,
                type: 'PATCH',
                dataType: 'json',
                data: JSON.stringify(ret),
                cache: false,
                success: function (response2) {
                  $('.content-loading-button').hide()
                  $('.btn-default').show()
                  $('.formu-b2c').hide()
                  $('.contact-qd-v1-form-success').show()
                }
              })
            } else {
              $('.content-loading-button').hide()
              $('.btn-default').show()
              $('.mensajes').text('')
              $('.mensajes').text('El usuario con el email que se intenta registrar ya existe, por favor intente iniciando sesion.')
              $('#myModal').modal('show')
            }
          }
        }
      } else {
        $('.content-loading-button').hide()
        $('.btn-default').show()
        $('.mensajes').text('')
        $('.mensajes').text('Este número de documento no se encuentra registrado en la base de datos de la Boutique empleados, por favor contáctate con el área de recursos humanos.')
        $('#myModal').modal('show')
      }
    },
    error: function (data) {
      console.log('Error al consumir el crÃ©dito de Cliente', data)
      return false
    }
  })
}

const insertEmpleado = () => {
  const formData = saveTempDataEmpleados()
  let result
  $.ajax({
    crossDomain: true,
    async: false,
    url: 'https://media.autecomobility.com/recursos/web/empleados/empleados.php',
    type: 'POST',
    dataType: 'json',
    cache: false,
    data: JSON.stringify(formData),
    success: function (response) {
      result = response
    }
  })

  return result
}

const validateExitsClient = () => {
  const formDatas = saveTempDataEmpleados()
  const url = '/api/dataentities/UP/search?_where=%28document=%2A' + formDatas.document + '%2A%29&_fields=emailLogin,document'
  let result
  $.ajax({
    headers: {
      Accept: 'application/vnd.vtex.ds.v10+json',
      'Content-Type': 'application/json'
    },
    crossDomain: true,
    async: false,
    cache: false,
    type: 'GET',
    url: url,
    success: function (data) {
      result = data
    }
  })

  return result
}

const removeH2Slick = () => {
  $('.repuestos-carousel h2').parent().parent().hide()
}

const hoverMenu = () => {
  /* hover menu */
  let timer
  const delay = 3
  $('.header-qd-v1-amazing-menu>ul>li').hover(function () {
    const $this = $(this)
    timer = setTimeout(function () {
      $this.find('.active-menu').css({
        opacity: 1,
        visibility: 'visible'
      })
    }, delay)
  }, function () {
    const $this = $(this)
    clearTimeout(timer)
    $this.find('.active-menu').css({
      opacity: 0,
      visibility: 'hidden'
    })
  })
  /* hover menu */
}
/** EMPLEADOS */


const messageOutStock = () => {
  const numberProduct = $('#___rc-p-id').val()
  dataProd(numberProduct).then((res) => {
    const unavailableStock = $('body').hasClass('qd-product-unavailable')
    $('.product-qd-v1-price span').attr('style', 'display: block !important')
    // alert(validateStock)
    // alert(res.available)
    if (res.available === false) {
      if ($('.content-aviso-outstock').length < 2) {
        $('.qd-price-formated').attr('style', 'display: block;')
        $('.product-qd-v1-act-tab').attr('style', 'display: block !important')
        $('.product-qd-v1-price').append('<div class="content-aviso-outstock"><label>No disponible online. </label><a href="/puntos-de-atencion">Valida el punto de venta más cercano</a></div>')
      }
    }

    if (unavailableStock === true) {
      if ($(document.body).is('.product-brand') && $('.product-qd-v1-test-drive').is(':visible') !== true) {
        $('.product-qd-v1-contact').attr('style', 'width : 100%!important; display : inline')
        // $('.product-qd-v1-price span').attr('style', 'display: block!important')
      }
    }
  })
}

const listenChangueSKU = () => {
  $('body').on('change', function () {
    if ($(this).hasClass('qd-product-unavailable')) {
      $('.content-aviso-outstock').remove()
      $('.qd-price-formated').attr('style', 'display: block;')
      $('.qd-price-formated').attr('style', 'font-size: 38px;')
      $('.qd-price-formated').attr('style', 'color: #000;')
      $('.product-qd-v1-price').append('<div class="content-aviso-outstock"><label><sttrong>No disponible online.</sttrong> </label><a href="/puntos-de-atencion">Valida el punto de venta más cercano</a></div>')
      setTimeout(() => {
        $('.product-qd-v1-price span').attr('style', 'display: block !important')
        $('.product-brand .product-qd-v1-act-tab').attr('style', 'display: block !important')
        $('.price-title').attr('style', 'display: block !important')

        if ($('.addi').is(':visible') === true) {
          $('.addi').attr('style', 'display: none !important')
        }
      }, 150)
    } else {
      const unavailableStock = $('body').hasClass('qd-product-unavailable')
      if (unavailableStock === true) {
        if ($(document.body).is('.product-brand') && $('.product-qd-v1-test-drive').is(':visible') !== true) {
          $('.product-qd-v1-contact').attr('style', 'width : 100%!important; display : inline')
          // $('.product-qd-v1-price span').attr('style', 'display: block!important')
        }
      }
      // $('.product-qd-v1-contact').attr('style', 'width : 100%!important; display : inline')
      $('.qd-product-unavailable .que-hacer').attr('style', 'display: block !important')
      $('.content-aviso-outstock').remove()
      // $('.qd-price-formated').attr('style', 'display: none !important');
      if (!$('.addi').is(':visible') === true) {
        $('.addi').attr('style', 'display: block !important')
      }
    }
  })
}

document.addEventListener('DOMContentLoaded', (e) => {
  removeH2Slick()
  hoverMenu()
  clickSendEmpleados()
  messageOutStock()
  listenChangueSKU()
})
