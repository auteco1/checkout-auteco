const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCSSExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
    mode: 'development',
    entry:{
      autecoHome: './src/modules/auteco-home.js',
      autecoMain: './src/modules/auteco-main.js',
      autecoMainEmpleado: './src/modules/auteco-main-empleados.js',
      autecoProduct : './src/modules/auteco-product.js',
      autecoHotsite : './src/modules/auteco-hotsites.js',
      autecoInstitucionales : './src/modules/auteco-institucionales.js',
      //calculadoraBusiness : './hotsites/calculadora-business/scripts.js',
      autecoVitrina : './src/modules/auteco-vitrina.js',
      checkout : './src/modules/checkout.js',
      checkoutConfirmationCustom : './src/modules/checkout-custom.js',
      autecoMainRedesign : './src/modules/auteco-redesign.js',
      autecoMainRedesignHome : './src/modules/home.js',
      autecoMainRedesignDepartament: './src/modules/departament.js',
      autecoMainRedesignCategory: './src/modules/category.js'
    },
    output:{
        filename:'[name].bundle.js',
        path: path.resolve(__dirname, 'dist')
    },
    watch:true,
    watchOptions: {
        ignored: ['node_modules/**']
    },
    plugins: [
      new HtmlWebpackPlugin({
      filename: 'index.html',
      template:'src/index.html',
      title:'index'
      }),
      new MiniCSSExtractPlugin({
        filename: "[name].css"
      })
    ],
    module: {
        rules: [
          {
            test: /\.js$/,
            exclude: /(node_modules|bower_components)/,
            loaders: ["babel-loader"],
          },
          { 
            test: /\.scss$/, 
            loader: [
              MiniCSSExtractPlugin.loader,
              "css-loader",
              'sass-loader'
            ]
          },
          {
            test: /\.(png|svg|jpg|gif)$/,
            use: [
              'file-loader',
          ],
          
         }
        ],
    },
    devServer: {
        port: 9000
    }
    
}